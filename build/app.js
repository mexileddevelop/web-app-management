'use strict';



var appConfig = window.appConfig || {};

appConfig.menu_speed = 200;

appConfig.smartSkin = "smart-style-0";

appConfig.skins = [
    {name: "smart-style-0",
        logo: "styles/img/logo.png",
        class: "btn btn-block btn-xs txt-color-white margin-right-5",
        style: "background-color:#4E463F;",
        label: "Smart Default"},

    {name: "smart-style-1",
        logo: "styles/img/logo-white.png",
        class: "btn btn-block btn-xs txt-color-white",
        style: "background:#3A4558;",
        label: "Dark Elegance"},

    {name: "smart-style-2",
        logo: "styles/img/logo-blue.png",
        class: "btn btn-xs btn-block txt-color-darken margin-top-5",
        style: "background:#fff;",
        label: "Ultra Light"},

    {name: "smart-style-3",
        logo: "styles/img/logo-pale.png",
        class: "btn btn-xs btn-block txt-color-white margin-top-5",
        style: "background:#f78c40",
        label: "Google Skin"},

    {name: "smart-style-4",
        logo: "styles/img/logo-pale.png",
        class: "btn btn-xs btn-block txt-color-white margin-top-5",
        style: "background: #bbc0cf; border: 1px solid #59779E; color: #17273D !important;",
        label: "PixelSmash"},

    {name: "smart-style-5",
        logo: "styles/img/logo-pale.png",
        class: "btn btn-xs btn-block txt-color-white margin-top-5",
        style: "background: rgba(153, 179, 204, 0.2); border: 1px solid rgba(121, 161, 221, 0.8); color: #17273D !important;",
        label: "Glass"},

    {name: "smart-style-6",
        logo: "styles/img/logo-pale.png",
        class: "btn btn-xs btn-block txt-color-white margin-top-5",
        style: "background: #2196F3; border: 1px solid rgba(121, 161, 221, 0.8); color: #FFF !important;",
        beta: true,
        label: "MaterialDesign"
    }


];



appConfig.sound_path = "sound/";
appConfig.sound_on = true; 


/*
* DEBUGGING MODE
* debugState = true; will spit all debuging message inside browser console.
* The colors are best displayed in chrome browser.
*/


appConfig.debugState = false;	
appConfig.debugStyle = 'font-weight: bold; color: #00f;';
appConfig.debugStyle_green = 'font-weight: bold; font-style:italic; color: #46C246;';
appConfig.debugStyle_red = 'font-weight: bold; color: #ed1c24;';
appConfig.debugStyle_warning = 'background-color:yellow';
appConfig.debugStyle_success = 'background-color:green; font-weight:bold; color:#fff;';
appConfig.debugStyle_error = 'background-color:#ed1c24; font-weight:bold; color:#fff;';


appConfig.voice_command = true;
appConfig.voice_command_auto = false;

/*
 *  Sets the language to the default 'en-US'. (supports over 50 languages 
 *  by google)
 * 
 *  Afrikaans         ['af-ZA']
 *  Bahasa Indonesia  ['id-ID']
 *  Bahasa Melayu     ['ms-MY']
 *  CatalГ            ['ca-ES']
 *  ДЊeЕЎtina         ['cs-CZ']
 *  Deutsch           ['de-DE']
 *  English           ['en-AU', 'Australia']
 *                    ['en-CA', 'Canada']
 *                    ['en-IN', 'India']
 *                    ['en-NZ', 'New Zealand']
 *                    ['en-ZA', 'South Africa']
 *                    ['en-GB', 'United Kingdom']
 *                    ['en-US', 'United States']
 *  EspaГ±ol          ['es-AR', 'Argentina']
 *                    ['es-BO', 'Bolivia']
 *                    ['es-CL', 'Chile']
 *                    ['es-CO', 'Colombia']
 *                    ['es-CR', 'Costa Rica']
 *                    ['es-EC', 'Ecuador']
 *                    ['es-SV', 'El Salvador']
 *                    ['es-ES', 'EspaГ±a']
 *                    ['es-US', 'Estados Unidos']
 *                    ['es-GT', 'Guatemala']
 *                    ['es-HN', 'Honduras']
 *                    ['es-MX', 'MГ©xico']
 *                    ['es-NI', 'Nicaragua']
 *                    ['es-PA', 'PanamГЎ']
 *                    ['es-PY', 'Paraguay']
 *                    ['es-PE', 'PerГє']
 *                    ['es-PR', 'Puerto Rico']
 *                    ['es-DO', 'RepГєblica Dominicana']
 *                    ['es-UY', 'Uruguay']
 *                    ['es-VE', 'Venezuela']
 *  Euskara           ['eu-ES']
 *  FranГ§ais         ['fr-FR']
 *  Galego            ['gl-ES']
 *  Hrvatski          ['hr_HR']
 *  IsiZulu           ['zu-ZA']
 *  ГЌslenska         ['is-IS']
 *  Italiano          ['it-IT', 'Italia']
 *                    ['it-CH', 'Svizzera']
 *  Magyar            ['hu-HU']
 *  Nederlands        ['nl-NL']
 *  Norsk bokmГҐl     ['nb-NO']
 *  Polski            ['pl-PL']
 *  PortuguГЄs        ['pt-BR', 'Brasil']
 *                    ['pt-PT', 'Portugal']
 *  RomГўnДѓ          ['ro-RO']
 *  SlovenДЌina       ['sk-SK']
 *  Suomi             ['fi-FI']
 *  Svenska           ['sv-SE']
 *  TГјrkГ§e          ['tr-TR']
 *  Р±СЉР»РіР°СЂСЃРєРё['bg-BG']
 *  PСѓСЃСЃРєРёР№     ['ru-RU']
 *  РЎСЂРїСЃРєРё      ['sr-RS']
 *  н•њкµ­м–ґ         ['ko-KR']
 *  дё­ж–‡            ['cmn-Hans-CN', 'ж™®йЂљиЇќ (дё­е›Ѕе¤§й™†)']
 *                    ['cmn-Hans-HK', 'ж™®йЂљиЇќ (й¦™жёЇ)']
 *                    ['cmn-Hant-TW', 'дё­ж–‡ (еЏ°зЃЈ)']
 *                    ['yue-Hant-HK', 'зІµиЄћ (й¦™жёЇ)']
 *  ж—Ґжњ¬иЄћ         ['ja-JP']
 *  Lingua latД«na    ['la']
 */
appConfig.voice_command_lang = 'en-US';
/*
 *  Use localstorage to remember on/off (best used with HTML Version)
 */ 
appConfig.voice_localStorage = false;
/*
 * Voice Commands
 * Defines all voice command variables and functions
 */ 
if (appConfig.voice_command) {
        
     	appConfig.commands = {
                
        'show dashboard' : function() { window.location.hash = "dashboard" },
        'show inbox' : function() {  window.location.hash = "inbox/" },
        'show graphs' : function() {  window.location.hash = "graphs/flot" },
        'show flotchart' : function() { window.location.hash = "graphs/flot" },
        'show morris chart' : function() { window.location.hash = "graphs/morris" },
        'show inline chart' : function() { window.location.hash = "graphs/inline-charts" },
        'show dygraphs' : function() { window.location.hash = "graphs/dygraphs" },
        'show tables' : function() { window.location.hash = "tables/table" },
        'show data table' : function() { window.location.hash = "tables/datatable" },
        'show jquery grid' : function() { window.location.hash = "tables/jqgrid" },
        'show form' : function() { window.location.hash = "forms/form-elements" },
        'show form layouts' : function() { window.location.hash = "forms/form-templates" },
        'show form validation' : function() { window.location.hash = "forms/validation" },
        'show form elements' : function() { window.location.hash = "forms/bootstrap-forms" },
        'show form plugins' : function() { window.location.hash = "forms/plugins" },
        'show form wizards' : function() { window.location.hash = "forms/wizards" },
        'show bootstrap editor' : function() { window.location.hash = "forms/other-editors" },
        'show dropzone' : function() { window.location.hash = "forms/dropzone" },
        'show image cropping' : function() { window.location.hash = "forms/image-editor" },
        'show general elements' : function() { window.location.hash = "ui/general-elements" },
        'show buttons' : function() { window.location.hash = "ui/buttons" },
        'show fontawesome' : function() { window.location.hash = "ui/icons/fa" },
        'show glyph icons' : function() { window.location.hash = "ui/icons/glyph" },
        'show flags' : function() { window.location.hash = "ui/icons/flags" },
        'show grid' : function() { window.location.hash = "ui/grid" },
        'show tree view' : function() { window.location.hash = "ui/treeview" },
        'show nestable lists' : function() { window.location.hash = "ui/nestable-list" },
        'show jquery U I' : function() { window.location.hash = "ui/jqui" },
        'show typography' : function() { window.location.hash = "ui/typography" },
        'show calendar' : function() { window.location.hash = "calendar" },
        'show widgets' : function() { window.location.hash = "widgets" },
        'show gallery' : function() { window.location.hash = "gallery" },
        'show maps' : function() { window.location.hash = "gmap-xml" },
        'go back' :  function() { history.back(1); }, 
        'scroll up' : function () { $('html, body').animate({ scrollTop: 0 }, 100); },
        'scroll down' : function () { $('html, body').animate({ scrollTop: $(document).height() }, 100);},
        'hide navigation' : function() { 
            if ($( ":root" ).hasClass("container") && !$( ":root" ).hasClass("menu-on-top")){
                $('span.minifyme').trigger("click");
            } else {
                $('#hide-menu > span > a').trigger("click"); 
            }
        },
        'show navigation' : function() { 
            if ($( ":root" ).hasClass("container") && !$( ":root" ).hasClass("menu-on-top")){
                $('span.minifyme').trigger("click");
            } else {
                $('#hide-menu > span > a').trigger("click"); 
            }
        },
        'mute' : function() {
            appConfig.sound_on = false;
            $.smallBox({
                title : "MUTE",
                content : "All sounds have been muted!",
                color : "#a90329",
                timeout: 4000,
                icon : "fa fa-volume-off"
            });
        },
        'sound on' : function() {
            appConfig.sound_on = true;
            $.speechApp.playConfirmation();
            $.smallBox({
                title : "UNMUTE",
                content : "All sounds have been turned on!",
                color : "#40ac2b",
                sound_file: 'voice_alert',
                timeout: 5000,
                icon : "fa fa-volume-up"
            });
        },
        'stop' : function() {
            smartSpeechRecognition.abort();
            $( ":root" ).removeClass("voice-command-active");
            $.smallBox({
                title : "VOICE COMMAND OFF",
                content : "Your voice commands has been successfully turned off. Click on the <i class='fa fa-microphone fa-lg fa-fw'></i> icon to turn it back on.",
                color : "#40ac2b",
                sound_file: 'voice_off',
                timeout: 8000,
                icon : "fa fa-microphone-slash"
            });
            if ($('#speech-btn .popover').is(':visible')) {
                $('#speech-btn .popover').fadeOut(250);
            }
        },
        'help' : function() {

            $('#voiceModal').removeData('modal').modal( { remote: "app/layout/partials/voice-commands.tpl.html", show: true } );
            if ($('#speech-btn .popover').is(':visible')) {
                $('#speech-btn .popover').fadeOut(250);
            }

        },      
        'got it' : function() {
            $('#voiceModal').modal('hide');
        },  
        'logout' : function() {
            $.speechApp.stop();
            window.location = $('#logout > span > a').attr("href");
        }
    };
}

appConfig.apiRootUrl = 'api';

window.appConfig = appConfig;

/*
* END APP.appConfig
*/
'use strict';

$.sound_path = appConfig.sound_path;
$.sound_on = appConfig.sound_on;


$(function () {

    // moment.js default language
    moment.locale('en')

    angular.bootstrap(document, ['app']);
 
});

'use strict';

/**
 * @ngdoc overview
 * @name app [smartadminApp]
 * @description
 * # app [smartadminApp]
 *
 * Main module of the application.
 */

angular.module('app', [
    'ngSanitize',
    'ngAnimate',
    'restangular',
    'ui.router',
    'ui.bootstrap',
    'angular-md5',

    // Smartadmin Angular Common Module
    'SmartAdmin',

    // App
    'app.auth',
    'app.layout',
    //'app.chat',
    //'app.dashboard',
    //'app.calendar',
    //'app.inbox',
    //'app.graphs',
    //'app.tables',
    //'app.forms',
    //'app.ui',
    //'app.widgets',
    //'app.appViews',
    //'app.misc',
    //'app.smartAdmin',
    //'app.eCommerce'
    'app.monitor',
    'app.operativo',
    'app.proyectos',
    'app.admin'
])
    .config(['$provide', '$httpProvider', 'RestangularProvider', '$urlRouterProvider',
        function ($provide, $httpProvider, RestangularProvider, $urlRouterProvider) {

            // Intercept http calls.
            $provide.factory('ErrorHttpInterceptor', ['$q', '$injector', 'sessionService', function ($q, $injector, sessionService) {
                var errorCounter = 0;

                function notifyError(rejection) {
                    if (rejection.status === -1)
                        rejection.data = "Servicio Web no Disponible";

                    if (rejection.status !== 401) {
                        $.bigBox({
                            title: rejection.status + ' ' + rejection.statusText,
                            content: rejection.data,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            number: ++errorCounter,
                            timeout: 6000
                        });
                    }
                }

                return {
                    // On request failure
                    requestError: function (rejection) {
                        // show notification
                        notifyError(rejection);

                        // Return the promise rejection.
                        return $q.reject(rejection);
                    },

                    // On response failure
                    responseError: function (rejection) {
                        // show notification
                        notifyError(rejection);
                        // Return the promise rejection.
                        return $q.reject(rejection);
                    }
                };
            }]);

            $urlRouterProvider.otherwise(function ($injector) {
                if ($injector.get("$state") === false)
                    return "/login";
                else
                    return "/monitoreo";
            });

            // Add the interceptor to the $httpProvider.
            $httpProvider.interceptors.push('ErrorHttpInterceptor');

            RestangularProvider.setBaseUrl(location.pathname.replace(/[^\/]+?$/, ''));

        }
    ])
    .constant('APP_CONFIG', window.appConfig)

    .run(['$rootScope', '$state', '$stateParams', 'sessionService', 'permissionsService', 'DTDefaultOptions',
        function ($rootScope, $state, $stateParams, sessionService, permissionsService, DTDefaultOptions) {
            //DTDefaultOptions es para las opcion por defecto de todas las tablas
            //Lo usamos para asignar un template del mensaje "Loading..." traducido
            DTDefaultOptions.setLoadingTemplate('<h3>Espere un momento...</h3>');
            DTDefaultOptions.setLanguageSource('api/tables/Spanish.json');
            //~
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            // editableOptions.theme = 'bs3';

            //check if user has permissions on Routes
            $rootScope.$on('$stateChangeStart', function (event, toState) {
                $rootScope.globalShowContent = false;
                // if been login is required
                if (toState.data.login === true && !sessionService.getData()) {
                    event.preventDefault();
                    $state.go('login').then(function () {
                        $state.reload();
                    });
                } else {
                    if (!!toState.permission) {
                        if ((typeof toState.permission) === 'string') {
                            if (!permissionsService.canAccess(toState.permission)) {
                                event.preventDefault();
                                $state.go('app.monitor', {}, {reload: true});
                            }
                            $rootScope.globalShowContent = true;
                        } else
                            $state.go('app.monitor', {}, {reload: true});
                    } else
                        $rootScope.globalShowContent = true;
                }

            });

        }
    ]);
(function(){
    "use strict";

    angular.module('SmartAdmin', [
        "SmartAdmin.Forms",
        "SmartAdmin.Layout",
        "SmartAdmin.UI",
    ]);
})();
"use strict";

angular.module('app.admin', ['ui.router', 'datatables', 'datatables.bootstrap'])
    .config(['$stateProvider', function ($stateProvider) {

        $stateProvider
            .state('app.admin', {
                abstract: true,
                data: {
                    login: true,
                    title: 'Administración'
                }
            })

            .state('app.admin.usuarios', {
                cache: false,
                url: '/admin/usuarios',
                permission: 'user.view, user.add, user.edit, user.delete',
                data: {
                    login: true,
                    title: 'Usuarios'
                },
                views: {
                    "content@app": {
                        controller: 'UsuariosCtrl',
                        templateUrl: "app/admin/usuarios/views/index.html"
                    }
                }
            })
            .state('app.admin.usuarios.accion', {
                cache: false,
                url: '/:accion/:id',
                permission: 'user.view, user.add, user.edit',
                data: {
                    login: true,
                    title: 'Usuario'
                },
                views: {
                    "content@app": {
                        controller: 'UsuariosAccionCtrl',
                        templateUrl: "app/admin/usuarios/views/accion.html"
                    }
                }
            })

            .state('app.admin.perfiles', {
                cache: false,
                url: '/admin/perfiles',
                permission: 'profile.view, profile.add, profile.edit, profile.delete',
                data: {
                    login: true,
                    title: 'Perfiles'
                },
                views: {
                    "content@app": {
                        controller: 'PerfilesCtrl',
                        templateUrl: "app/admin/perfiles/views/index.html"
                    }
                }
            })
            .state('app.admin.perfiles.accion', {
                cache: false,
                url: '/:accion/:id',
                permission: 'profile.view, profile.add, profile.edit',
                data: {
                    login: true,
                    title: 'Perfil'
                },
                views: {
                    "content@app": {
                        controller: 'PerfilesAccionCtrl',
                        templateUrl: "app/admin/perfiles/views/accion.html"
                    }
                }
            })

            .state('app.admin.modulos', {
                cache: false,
                url: '/admin/modulos',
                permission: 'module.view, module.add, module.edit, module.delete',
                data: {
                    login: true,
                    title: 'Modulos'
                },
                views: {
                    "content@app": {
                        controller: 'ModulosCtrl',
                        templateUrl: "app/admin/modulos/views/index.html"
                    }
                }
            })
            .state('app.admin.modulos.accion', {
                cache: false,
                url: '/:accion/:id',
                permission: 'module.view, module.add, module.edit',
                data: {
                    login: true,
                    title: 'Modulo'
                },
                views: {
                    "content@app": {
                        controller: 'ModulosAccionCtrl',
                        templateUrl: "app/admin/modulos/views/accion.html"
                    }
                }
            })

    }]);
"use strict";
angular.module('app.auth', ['ui.router'])
    .config(['$stateProvider', function ($stateProvider) {

        $stateProvider
            .state('login', {
                url: '/login',
                views: {
                    root: {
                        templateUrl: 'app/auth/views/login.html',
                        controller: 'loginCtrl'
                    }
                },
                data: {
                    title: 'Ingreso',
                    htmlId: 'extr-page'
                },
                resolve: {
                    srcipts: function (lazyScript) {
                        return lazyScript.register([
                            'build/vendor.ui.js'
                        ])

                    }
                }
            })
            .state('register', {
                url: '/registro',
                views: {
                    root: {
                        templateUrl: 'app/auth/views/register.html',
                        controller: 'registerCtrl'
                    }
                },
                data: {
                    title: 'Registro',
                    htmlId: 'extr-page'
                }
            })
            .state('forgotPassword', {
                url: '/forgot-password',
                views: {
                    root: {
                        templateUrl: 'app/auth/views/forgot-password.html'
                    }
                },
                data: {
                    title: 'Forgot Password',
                    htmlId: 'extr-page'
                }
            })
            .state('lock', {
                url: '/lock',
                views: {
                    root: {
                        templateUrl: 'app/auth/views/lock.html'
                    }
                },
                data: {
                    title: 'Locked Screen',
                    htmlId: 'lock-page'
                }
            })

    }]);
'use strict';

angular.module('app.dashboard', [
    'ui.router',
    'ngResource'
])

.config(function ($stateProvider) {
    $stateProvider
        .state('app.dashboard', {
            url: '/dashboard',
            views: {
                "content@app": {
                    controller: 'DashboardCtrl',
                    templateUrl: 'app/dashboard/dashboard.html'
                }
            },
            data:{
                title: 'Dashboard'
            }
        })
        .state('app.dashboard-social', {
            url: '/dashboard-social',
            views: {
                "content@app": {
                    templateUrl: 'app/dashboard/social-wall.html'
                }
            },
            data:{
                title: 'Dashboard Social'
            }
        });
});

"use strict";


angular.module('app.layout', ['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('app', {
                abstract: true,
                views: {
                    root: {
                        templateUrl: 'app/layout/layout.tpl.html'
                    }
                }
            });
        $urlRouterProvider.otherwise('/');

    }]);
"use strict";


angular.module('app.monitor', ['ui.router',
    'uiGmapgoogle-maps'
])
//.config(function(uiGmapGoogleMapApiProvider) {
//    uiGmapGoogleMapApiProvider.configure({
//        //    key: 'your api key',
//        v: '3.20', //defaults to latest 3.X anyhow
//        libraries: 'weather,geometry,visualization'
//    });
//})


angular.module('app.monitor')
    .config(['$stateProvider', 'uiGmapGoogleMapApiProvider', function ($stateProvider, uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyDk7Kg1cEqiRUEyYCXN6KWRkejGRMwHkzc',
            v: '3.20', //defaults to latest 3.X anyhow
            libraries: 'weather,geometry,visualization'
        });

        $stateProvider
            .state('app.monitor', {
                url: '/monitoreo',
                data: {
                    login: true,
                    title: 'Estatus de Infraestructura'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/monitoreo/views/index.html',
                        controller: 'MonitoreoCtrl'
                    }
                },
                resolve: {
                    scripts: function (lazyScript) {
                        return lazyScript.register([
                            'build/vendor.graphs.js'
                        ]);
                    }
                }
            })
    }]);
"use strict";


angular.module('app.operativo', ['ui.router', 'datatables', 'datatables.bootstrap'])
    .config(['$stateProvider', function ($stateProvider) {

        $stateProvider
            .state('app.operativo', {
                url: '/operativo',
                data: {
                    login: true,
                    title: 'Operativo'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/operativo/views/index.html',
                        controller: 'OperativoCtrl'
                    }
                }
            })
    }]);
"use strict";

angular.module('app.proyectos', ['ui.router', 'datatables', 'datatables.bootstrap', 'uiGmapgoogle-maps'])
    .config(['$stateProvider', 'uiGmapGoogleMapApiProvider', function ($stateProvider, uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyDk7Kg1cEqiRUEyYCXN6KWRkejGRMwHkzc',
            v: '3.20', //defaults to latest 3.X anyhow
            libraries: 'weather,geometry,visualization'
        });

        $stateProvider
            .state('app.proyecto', {
                cache: false,
                url: '/proyectos',
                permission: 'project.view, project.add, project.edit, project.delete',
                data: {
                    login: true,
                    title: 'Proyectos'
                },
                views: {
                    "content@app": {
                        controller: 'ProyectoCtrl',
                        templateUrl: 'app/proyecto/views/index.html'
                    }
                }
            })
            .state('app.proyecto.accion', {
                cache: false,
                url: '/:accion?:id',
                permission: 'project.view, project.add, project.edit',
                data: {
                    login: true,
                    title: 'Proyecto'
                },
                views: {
                    "content@app": {
                        controller: 'ProyectoAccionCtrl',
                        templateUrl: 'app/proyecto/views/accion.html'
                    }
                }
            })

            .state('app.proyecto.seccion', {
                cache: false,
                url: '^/secciones/:proyecto',
                permission: 'project.view, project.add, project.edit, project.delete',
                data: {
                    login: true,
                    title: 'Secciones'
                },
                views: {
                    "content@app": {
                        controller: 'SeccionCtrl',
                        templateUrl: 'app/proyecto/seccion/views/index.html'
                    }
                }
            })
            .state('app.proyecto.seccion.accion', {
                cache: false,
                url: '/:accion?:id',
                permission: 'project.view, project.add, project.edit',
                data: {
                    login: true,
                    title: 'Seccion'
                },
                views: {
                    "content@app": {
                        controller: 'SeccionAccionCtrl',
                        templateUrl: 'app/proyecto/seccion/views/accion.html'
                    }
                }
            })

            .state('app.proyecto.seccion.poligono', {
                cache: false,
                url: '^/poligonos/:seccion',
                permission: 'project.view, project.add, project.edit, project.delete',
                data: {
                    login: true,
                    title: 'Poligonos'
                },
                views: {
                    "content@app": {
                        controller: 'PoligonoCtrl',
                        templateUrl: 'app/proyecto/seccion/poligono/views/index.html'
                    }
                }
            })
            .state('app.proyecto.seccion.poligono.accion', {
                cache: false,
                url: '/:accion?:id',
                permission: 'project.view, project.add, project.edit',
                data: {
                    login: true,
                    title: 'Polígono'
                },
                views: {
                    "content@app": {
                        controller: 'PoligonoAccionCtrl',
                        templateUrl: 'app/proyecto/seccion/poligono/views/accion.html'
                    }
                }
            })

            .state('app.proyecto.seccion.poligono.circuito', {
                cache: false,
                url: '^/circuitos/:poligono',
                permission: 'project.view, project.add, project.edit, project.delete',
                data: {
                    login: true,
                    title: 'Circuitos'
                },
                views: {
                    "content@app": {
                        controller: 'CircuitoCtrl',
                        templateUrl: 'app/proyecto/seccion/poligono/circuito/views/index.html'
                    }
                }
            })
            .state('app.proyecto.seccion.poligono.circuito.accion', {
                cache: false,
                url: '/:accion?:id',
                permission: 'project.view, project.add, project.edit',
                data: {
                    login: true,
                    title: 'Circuito'
                },
                views: {
                    "content@app": {
                        controller: 'CircuitoAccionCtrl',
                        templateUrl: 'app/proyecto/seccion/poligono/circuito/views/accion.html'
                    }
                },
                resolve: {
                    srcipts: function(lazyScript){
                        return lazyScript.register([
                            "build/vendor.ui.js"
                        ])

                    }
                }
            })

    }]);
(function(){
    "use strict";

    angular.module('SmartAdmin.Forms', []);
})();
(function(){
    "use strict";

    angular.module('SmartAdmin.Layout', []);
})();
(function(){
    "use strict";

    angular.module('SmartAdmin.UI', []);
})();
    "use strict";


angular.module('app.chat', ['ngSanitize'])
.run(function ($templateCache) {

    $templateCache.put("template/popover/popover.html",
        "<div class=\"popover {{placement}}\" ng-class=\"{ in: isOpen(), fade: animation() }\">\n" +
        "  <div class=\"arrow\"></div>\n" +
        "\n" +
        "  <div class=\"popover-inner\">\n" +
        "      <h3 class=\"popover-title\" ng-bind-html=\"title | unsafe\" ng-show=\"title\"></h3>\n" +
        "      <div class=\"popover-content\"ng-bind-html=\"content | unsafe\"></div>\n" +
        "  </div>\n" +
        "</div>\n" +
        "");

}).filter('unsafe', ['$sce', function ($sce) {
    return function (val) {
        return $sce.trustAsHtml(val);
    };
}]);
'use strict';

angular.module('app.dashboard').controller('DashboardCtrl', function ($scope, $interval, CalendarEvent) {

    // Live Feeds Widget Data And Display Controls
    // Live Stats Tab


    function getFakeItem(index, prevValue){
        var limitUp = Math.min(100, prevValue + 5),
            limitDown = Math.abs(prevValue - 5);
        return [
            index,
            _.random(limitDown, limitUp, true)
        ]
    }

    function getFakeData() {
        return _(_.range(199)).reduce(function (out, number) {

            out.push(getFakeItem(number+1, _.last(out)[1]));
            return out;
        }, [
            [0, 50] // starting point
        ])
    }

    $scope.autoUpdate = false;

    var updateInterval;
    $scope.$watch('autoUpdate', function(autoUpdate){

        if(autoUpdate){
            updateInterval = $interval(function(){
                var stats = _.rest($scope.liveStats[0]).map(function(elem, i){
                    elem[0] = i;
                    return elem;
                });
                stats.push([199, _.last(stats)[1]]);
                $scope.liveStats = [stats];
            }, 1500)
        } else {
            $interval.cancel(updateInterval);
        }
    });


    $scope.liveStats = [getFakeData()];



    $scope.liveStatsOptions = {
        yaxis: {
            min: 0,
            max: 100
        },
        xaxis: {
            min: 0,
            max: 100
        },
        colors: ['rgb(87, 136, 156)'],
        series: {
            lines: {
                lineWidth: 1,
                fill: true,
                fillColor: {
                    colors: [
                        {
                            opacity: 0.4
                        },
                        {
                            opacity: 0
                        }
                    ]
                },
                steps: false

            }
        }
    };


    // Stats Display With Flot Chart

    var twitter = [
        [1, 27],
        [2, 34],
        [3, 51],
        [4, 48],
        [5, 55],
        [6, 65],
        [7, 61],
        [8, 70],
        [9, 65],
        [10, 75],
        [11, 57],
        [12, 59],
        [13, 62]
    ];
    var facebook = [
        [1, 25],
        [2, 31],
        [3, 45],
        [4, 37],
        [5, 38],
        [6, 40],
        [7, 47],
        [8, 55],
        [9, 43],
        [10, 50],
        [11, 47],
        [12, 39],
        [13, 47]
    ];
    $scope.statsData = [
        {
            label: "Twitter",
            data: twitter,
            lines: {
                show: true,
                lineWidth: 1,
                fill: true,
                fillColor: {
                    colors: [
                        {
                            opacity: 0.1
                        },
                        {
                            opacity: 0.13
                        }
                    ]
                }
            },
            points: {
                show: true
            }
        },
        {
            label: "Facebook",
            data: facebook,
            lines: {
                show: true,
                lineWidth: 1,
                fill: true,
                fillColor: {
                    colors: [
                        {
                            opacity: 0.1
                        },
                        {
                            opacity: 0.13
                        }
                    ]
                }
            },
            points: {
                show: true
            }
        }
    ];

    $scope.statsDisplayOptions = {
        grid: {
            hoverable: true
        },
        colors: ["#568A89", "#3276B1"],
        tooltip: true,
        tooltipOpts: {
            //content : "Value <b>$x</b> Value <span>$y</span>",
            defaultTheme: false
        },
        xaxis: {
            ticks: [
                [1, "JAN"],
                [2, "FEB"],
                [3, "MAR"],
                [4, "APR"],
                [5, "MAY"],
                [6, "JUN"],
                [7, "JUL"],
                [8, "AUG"],
                [9, "SEP"],
                [10, "OCT"],
                [11, "NOV"],
                [12, "DEC"],
                [13, "JAN+1"]
            ]
        },
        yaxes: {

        }
    };


    /* Live stats TAB 3: Revenew  */

    var trgt = [[1354586000000, 153], [1364587000000, 658], [1374588000000, 198], [1384589000000, 663], [1394590000000, 801], [1404591000000, 1080], [1414592000000, 353], [1424593000000, 749], [1434594000000, 523], [1444595000000, 258], [1454596000000, 688], [1464597000000, 364]],
        prft = [[1354586000000, 53], [1364587000000, 65], [1374588000000, 98], [1384589000000, 83], [1394590000000, 980], [1404591000000, 808], [1414592000000, 720], [1424593000000, 674], [1434594000000, 23], [1444595000000, 79], [1454596000000, 88], [1464597000000, 36]],
        sgnups = [[1354586000000, 647], [1364587000000, 435], [1374588000000, 784], [1384589000000, 346], [1394590000000, 487], [1404591000000, 463], [1414592000000, 479], [1424593000000, 236], [1434594000000, 843], [1444595000000, 657], [1454596000000, 241], [1464597000000, 341]];

    var targets = {
        label : "Target Profit",
        data : trgt,
        bars : {
            show : true,
            align : "center",
            barWidth : 30 * 30 * 60 * 1000 * 80
        }
    };
    $scope.targetsShow = true;

    $scope.$watch('targetsShow', function(toggle){
        reveniewElementToggle(targets, toggle);
    });


    var actuals = {
        label : "Actual Profit",
        data : prft,
        color : '#3276B1',
        lines : {
            show : true,
            lineWidth : 3
        },
        points : {
            show : true
        }
    };

    $scope.actualsShow = true;

    $scope.$watch('actualsShow', function(toggle){
        reveniewElementToggle(actuals, toggle);
    });

    var signups = {
        label : "Actual Signups",
        data : sgnups,
        color : '#71843F',
        lines : {
            show : true,
            lineWidth : 1
        },
        points : {
            show : true
        }
    };
    $scope.signupsShow = true;

    $scope.$watch('signupsShow', function(toggle){
        reveniewElementToggle(signups, toggle);
    });

    $scope.revenewData = [targets, actuals, signups];

    function reveniewElementToggle(element, toggle){
        if(toggle){
            if($scope.revenewData.indexOf(element) == -1)
                $scope.revenewData.push(element)
        } else {
            $scope.revenewData = _.without($scope.revenewData, element);
        }
    }

    $scope.revenewDisplayOptions = {
        grid : {
            hoverable : true
        },
        tooltip : true,
        tooltipOpts : {
            //content: '%x - %y',
            //dateFormat: '%b %y',
            defaultTheme : false
        },
        xaxis : {
            mode : "time"
        },
        yaxes : {
            tickFormatter : function(val, axis) {
                return "$" + val;
            },
            max : 1200
        }

    };

    // bird eye widget data
    $scope.countriesVisitsData = {
        "US": 4977,
        "AU": 4873,
        "IN": 3671,
        "BR": 2476,
        "TR": 1476,
        "CN": 146,
        "CA": 134,
        "BD": 100
    };

    $scope.events = [];

    // Queriing our events from CalendarEvent resource...
    // Scope update will automatically update the calendar
    CalendarEvent.query().$promise.then(function (events) {
        $scope.events = events;
    });


});
angular.module('app').directive('hasPermission', ['permissionsService', function (permissionsService) {
    return {
        restrict: 'A',
        compile: function (element) {
            element.removeAttr('has-permission').removeAttr('data-has-permission');
            return {
                post: function (scope, element, attributes) {
                    if (!((typeof attributes.hasPermission) === 'string'))
                        throw 'hasPermission value must be a string'

                    var modules = attributes.hasPermission.trim();

                    function toggleVisibilityBasedOnPermission() {
                        if (!permissionsService.hasPermission(modules))
                            element.remove();
                    }

                    toggleVisibilityBasedOnPermission();
                    scope.$on('tokenChanged', toggleVisibilityBasedOnPermission);
                }
            };
        }
    };
}]);
angular.module('app').factory('permissionsService', ['sessionService', function (sessionService) {
    return {
        /**
         *
         * @returns {[string,string]}
         *      string array with user permissions
         */
        getPermissions: function () {
            return sessionService.getData().permissions;
        },

        /**
         *
         * @param Modules
         *      string with requested comma separated modules
         *      @format module_submodule
         * @returns {boolean}
         *      true if the user has permission of some requested modules
         */
        hasPermission: function (modules) {
            if (!!modules) {
                modules = modules.replace(/\./g, '_').split(','); //Replace dots (.) by underscore (_) and akes an array of modules

                return modules.some(this.evaluateModulesPermission, this.getPermissions());
            }
        },

        evaluateModulesPermission: function(module) {
            var searchPattern, hasPermission; //Variables to save the search pattern and a boolean if the permission was found

            module = module.trim().toUpperCase(); //Clear white spaces and makes string uppercase

            var notPermissionFlag = (module[0] === '!'); //Check if there is a not operator in the module (true/false)
            if (notPermissionFlag)
                module = module.slice(1).trim();

            searchPattern = new RegExp('\\b' + module + '\\b');

            // this: sended by some() function in hasPermission as second parameter
            hasPermission = !!this.find(function (permission) {
                return (permission.search(searchPattern) != -1 );
            });

            return hasPermission!==notPermissionFlag;
        },

        /**
         *
         * @param permissionRequest
         *      string with requested comma separated modules
         *      @format module.submodule
         * @returns {boolean}
         *      true if the user has some requested permission
         */
        canAccess: function (permissionRequest) {
            if (!!permissionRequest) {
                permissionRequest = permissionRequest.trim();

                return this.hasPermission(permissionRequest);
            }
        }
   };
}]);
angular.module('app')
    .factory('restService', ['$http', '$q', 'tunnelRestSessionService',
        function ($http, $q, tunnelRestSessionService) {
            var endpoint = "http://209.126.99.20:8085";

            /**
             * aqui se definen todos los request que se pueden hacer en el servidor, son las pagina de las cuales van a extraer los datos
             * se define un arreglo en formato json, y a cada posicion del arreglo se le define una estructura
             * uri que es el path de la url,  type tipo de metodo, content, y si es asyncrono o no.
             */
            var configurations = {
                // Log in, log out y control de password
                "logIn": {
                    "uri": "/oauth/token",
                    "type": "POST",
                    "content": "application/x-www-form-urlencoded",
                    "async": false
                },
                "logOut": {
                    "uri": "/oauth/logout",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "chgPass": {
                    "uri": "/secure/passwordUpdate",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "frgtPass": {
                    "uri": "/users/recover_password",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "rcvrPass": {
                    "uri": "/users/recover_password",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                }
                // Usuarios
                ,
                "userGET": {
                    "uri": "/users",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "userPOST": {
                    "uri": "/users",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "userPUT": {
                    "uri": "/users",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "userDELETE": {
                    "uri": "/users",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Perfiles
                ,
                "profileGET": {
                    "uri": "/profiles",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "profileTREE": {
                    "uri": "/profiles/permissions/tree",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "profilePOST": {
                    "uri": "/profiles",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "profilePUT": {
                    "uri": "/profiles",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "profileDELETE": {
                    "uri": "/api/profiles",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Modulos
                ,
                "moduleGET": {
                    "uri": "/modules",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "modulePOST": {
                    "uri": "/modules",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "modulePUT": {
                    "uri": "/modules",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "moduleDELETE": {
                    "uri": "/modules",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Proyectos
                ,
                "projectGET": {
                    "uri": "/projects",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "projectPOST": {
                    "uri": "/projects",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "projectPUT": {
                    "uri": "/projects",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "projectDELETE": {
                    "uri": "/projects",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Secciones
                ,
                "sectionGET": {
                    "uri": "/sections",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "sectionPOST": {
                    "uri": "/sections",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "sectionPUT": {
                    "uri": "/sections",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "sectionDELETE": {
                    "uri": "/sections",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Poligonos
                ,
                "polygonGET": {
                    "uri": "/polygons",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "polygonPOST": {
                    "uri": "/polygons",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "polygonPUT": {
                    "uri": "/polygons",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "polygonDELETE": {
                    "uri": "/polygons",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Circuitos (devices)
                ,
                "deviceGET": {
                    "uri": "/devices",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "devicePOST": {
                    "uri": "/devices",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "devicePUT": {
                    "uri": "/devices",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "deviceDELETE": {
                    "uri": "/devices",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                },
                "deviceRecordsGET": {
                    "uri": "/records",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                }
            };

            return {
                sendRequest: function ($params, $code, $uriParams, $tokenRefreshed) {
                    var promiseRequest = $q.defer();

                    var service = this;
                    $tokenRefreshed = $tokenRefreshed || false;
                    var authHeader = null;
                    if (!!tunnelRestSessionService.sessionGetData()) {
                        authHeader = {"Authorization": tunnelRestSessionService.sessionGetData().tokenType + ' ' + tunnelRestSessionService.sessionGetData().token};
                    }
                    $http({
                        method: configurations[$code].type,
                        url: endpoint + configurations[$code].uri + ($uriParams || ''),
                        data: $params,
                        //content: configurations[$code].content,
                        headers: angular.extend({}, {"Content-Type": configurations[$code].content || "application/json"}, authHeader)
                    }).then(function (data) {
                        promiseRequest.resolve(data);
                    }, function (error) {
                        if (error.status === 401 || error.status === 403) {
                            if (!$tokenRefreshed) {
                                if (!!tunnelRestSessionService.sessionGetData()) {
                                    tunnelRestSessionService.sessionRefreshToken().then(function () {
                                        service.sendRequest($params, $code, $uriParams, true).then(function (data) {
                                            promiseRequest.resolve(data);
                                        }, function (error) {
                                            tunnelRestSessionService.sessionLogOut();
                                            promiseRequest.reject(error);
                                        });
                                        tunnelRestSessionService.sessionCleaningRefresh();
                                    }, function (error) {
                                        tunnelRestSessionService.sessionLogOut();
                                        tunnelRestSessionService.sessionCleaningRefresh();
                                        promiseRequest.reject(error);
                                    });
                                } else {
                                    tunnelRestSessionService.sessionLogOut();
                                    tunnelRestSessionService.sessionCleaningRefresh();
                                    promiseRequest.reject(error);
                                }
                            } else {
                                tunnelRestSessionService.sessionLogOut();
                                tunnelRestSessionService.sessionCleaningRefresh();
                                promiseRequest.reject();
                            }
                        } else
                            promiseRequest.reject(error);
                    });

                    return promiseRequest.promise;
                }
            };
        }
    ]);
angular.module('app')
    .factory('sessionService', ['$q', '$rootScope', '$injector', 'md5', 'tunnelRestSessionService',
        function ($q, $rootScope, $injector, md5, tunnelRestSessionService) {
            var cleanData = {
                name: '',
                username: '',
                token: '',
                tokenType: '',
                tokenRefresh: '',
                permissions: []
            };
            var sessionData = null;
            var inProcess = {
                loggingOut: false,
                refreshingToken: {
                    active: false,
                    promise: null
                }
            };
            return {
                getData: function () {
                    if (!sessionData && !!window.localStorage['mexiledSessionData']) {
                        sessionData = angular.fromJson(window.localStorage['mexiledSessionData']);
                        $rootScope.$broadcast('tokenChanged');
                    }
                    return sessionData;
                },
                cleanLocalData: function () {
                    window.localStorage.removeItem('mexiledSessionData');
                },
                refreshToken: function () {
                    if (!inProcess.refreshingToken.active) {
                        inProcess.refreshingToken.active = true;
                        inProcess.refreshingToken.promise = null;

                        var refreshData = "grant_type=refresh_token&client_id=web-application-access&client_secret=secret&refresh_token=" + this.getData().tokenRefresh;
                        inProcess.refreshingToken.promise = $q.defer();

                        tunnelRestSessionService.restRequest(refreshData, 'logIn', '', true).then(function (data) {
                            data = data.data;

                            sessionData = cleanData;
                            sessionData.name = data.name;
                            sessionData.username = data.username;
                            sessionData.token = data.access_token;
                            sessionData.tokenType = data.token_type;
                            sessionData.tokenRefresh = data.refresh_token;
                            sessionData.permissions = data.roles;

                            window.localStorage['mexiledSessionData'] = JSON.stringify(sessionData);

                            inProcess.refreshingToken.promise.resolve();
                        }, function (error) {
                            inProcess.refreshingToken.promise.reject(error);
                        });
                    }

                    return inProcess.refreshingToken.promise.promise;
                },
                cleanRefreshingToken: function () {
                    inProcess.refreshingToken.active = false;
                },
                login: function (user, pass) {
                    var loginData = "grant_type=password&client_id=web-application-access&client_secret=secret&username=" + user + "&password=" + md5.createHash(pass + '{' + user + '}');
                    var promiseLogIn = $q.defer();

                    tunnelRestSessionService.restRequest(loginData, 'logIn').then(function (data) {
                        data = data.data;

                        sessionData = cleanData;
                        sessionData.name = data.name;
                        sessionData.username = data.username;
                        sessionData.token = data.access_token;
                        sessionData.tokenType = data.token_type;
                        sessionData.tokenRefresh = data.refresh_token;
                        sessionData.permissions = data.roles;

                        window.localStorage['mexiledSessionData'] = JSON.stringify(sessionData);

                        inProcess = {
                            loggingOut: false,
                            refreshingToken: {
                                active: false,
                                promise: null
                            }
                        };

                        promiseLogIn.resolve();
                    }, function (error) {
                        promiseLogIn.reject(error);
                    });

                    return promiseLogIn.promise;
                },
                logout: function () {
                    if (!inProcess.loggingOut) {
                        inProcess.loggingOut = true;
                        var service = this;
                        tunnelRestSessionService.restRequest(null, 'logOut', '', true).finally(function () {
                            service.cleanLocalData();
                            sessionData = null;

                            $injector.get('$state').go('login', {}, {reload: true});
                        });
                    }
                }
            };
        }
    ]);
angular.module('app')
    .factory('tunnelRestSessionService', ['$injector', function ($injector) {
        var sessionService;
        var restService;
        return {
            sessionGetData: function () {
                if (!sessionService) { sessionService = $injector.get('sessionService'); }
                return sessionService.getData();
            },
            sessionRefreshToken: function () {
                if (!sessionService) { sessionService = $injector.get('sessionService'); }
                return sessionService.refreshToken();
            },
            sessionCleaningRefresh: function () {
                if (!sessionService) { sessionService = $injector.get('sessionService'); }
                return sessionService.cleanRefreshingToken();
            },
            sessionLogOut: function () {
                if (!sessionService) { sessionService = $injector.get('sessionService'); }
                return sessionService.logout();
            },
            restRequest: function ($params, $code, $uriParams, $tokenRefreshed) {
                if (!restService) { restService = $injector.get('restService'); }
                return restService.sendRequest($params, $code, $uriParams, $tokenRefreshed);
            }
        }
    }]);
angular.module('app.auth')
    .controller('loginCtrl', ['$scope', '$state', 'sessionService',
        function ($scope, $state, sessionService) {
            $scope.user = {
                value: '',
                valid: true
            };
            $scope.pass = {
                value: '',
                valid: true
            };
            $scope.loginError = false;

            sessionService.cleanLocalData();

            $scope.ingresar = function () {
                $scope.user.valid = (!!$scope.user.value && /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($scope.user.value));
                $scope.pass.valid = (!!$scope.pass.value && $scope.pass.value.length >= 6);
                if ($scope.user.valid && $scope.pass.valid) {
                    sessionService.login($scope.user.value, $scope.pass.value).then(function () {
                        $scope.loginError = false;
                        $state.go('app.monitor');
                    }, function (error) {
                        $scope.loginError = true;
                    });
                }
            };
            // Olvido contraseña
            $scope.forgotPass = function () {
                $scope.formForgotPass.submited = true;
                if ($scope.formForgotPass.$valid) {
                    if ($scope.email !== '') {
                        RestRequest.getRequestNoToken($http, null, 'frgtPass', '?username=' + $scope.email).then(
                            function () {
                                $('#modalForgotPass').dialog('close');
                                $.smallBox({
                                    title: "Solicitud enviada",
                                    content: "Se ha enviado a su correo las instrucciones para continuar con su solicitud de cambio de contraseña.",
                                    color: "#739E73",
                                    iconSmall: "fa fa-check fadeInRight animated",
                                    timeout: 4000
                                });
                            }
                        );
                    }
                }
            };

            // Limpia los modal dialog existentes en dom para su correcto funcionamiento al loggear
            $('div.ui-dialog').each(function () {
                this.parentNode.removeChild(this);
            });
        }
    ]);
angular.module('app.auth')
    .controller('registerCtrl', ['$scope', '$state', 'restService', 'md5',
        function ($scope, $state, restService, md5) {
            $scope.user = {
                userId: null,
                profileId: 2,
                name: '',
                username: '',
                email: '',
                validEmail: true,
                password: '',
                passwordConfirm: '',
                active: true
            };

            $scope.registerError = false;

            // Email validator
            $('input[type=email]').on('keyup change input paste cut', function (ev) {
                var element = ev.target;
                $scope.user.validEmail = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($(element).val());
            });

            $scope.registrar = function () {
                $scope.register.submited = true;
                $scope.user.validEmail = (!!$scope.user.email && /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($scope.user.email));
                if (!!$scope.user.name && $scope.user.validEmail && !!$scope.user.password && $scope.user.password === $scope.user.passwordConfirm) {
                    $scope.registerError = false;
                    $scope.user.username = $scope.user.email;

                    var tmpPassword = $scope.user.password;
                    $scope.user.password = md5.createHash($scope.user.password + '{' + $scope.user.email + '}');
                    $scope.user.passwordConfirm = $scope.user.password;

                    restService.sendRequest($scope.user, 'userPOST').then(function () {
                        $.smallBox({
                            title: "Registro completado",
                            content: "Se realizo el registro de su cuenta exitosamente, ahora puede utilizar sus datos para ingresar.",
                            color: "#C46A69",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 5000
                        });

                        $state.go('login');
                    }, function () {
                        $scope.user.password = tmpPassword;
                        $scope.user.passwordConfirm = $scope.user.password;

                        $.smallBox({
                            title: "Error al registrar",
                            content: "Su registro no pudo completarse en estos momentos, vuelva a intentarlo mas tarde.",
                            color: "#C46A69",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });
                    });
                } else
                    $scope.registerError = true;
            };
        }
    ]);
angular.module("app").run(["$templateCache", function($templateCache) {$templateCache.put("app/dashboard/live-feeds.tpl.html","<div jarvis-widget id=\"live-feeds-widget\" data-widget-togglebutton=\"false\" data-widget-editbutton=\"false\"\n     data-widget-fullscreenbutton=\"false\" data-widget-colorbutton=\"false\" data-widget-deletebutton=\"false\">\n<!-- widget options:\nusage: <div class=\"jarviswidget\" id=\"wid-id-0\" data-widget-editbutton=\"false\">\n\ndata-widget-colorbutton=\"false\"\ndata-widget-editbutton=\"false\"\ndata-widget-togglebutton=\"false\"\ndata-widget-deletebutton=\"false\"\ndata-widget-fullscreenbutton=\"false\"\ndata-widget-custombutton=\"false\"\ndata-widget-collapsed=\"true\"\ndata-widget-sortable=\"false\"\n\n-->\n<header>\n    <span class=\"widget-icon\"> <i class=\"glyphicon glyphicon-stats txt-color-darken\"></i> </span>\n\n    <h2>Live Feeds </h2>\n\n    <ul class=\"nav nav-tabs pull-right in\" id=\"myTab\">\n        <li class=\"active\">\n            <a data-toggle=\"tab\" href=\"#s1\"><i class=\"fa fa-clock-o\"></i> <span class=\"hidden-mobile hidden-tablet\">Live Stats</span></a>\n        </li>\n\n        <li>\n            <a data-toggle=\"tab\" href=\"#s2\"><i class=\"fa fa-facebook\"></i> <span class=\"hidden-mobile hidden-tablet\">Social Network</span></a>\n        </li>\n\n        <li>\n            <a data-toggle=\"tab\" href=\"#s3\"><i class=\"fa fa-dollar\"></i> <span class=\"hidden-mobile hidden-tablet\">Revenue</span></a>\n        </li>\n    </ul>\n\n</header>\n\n<!-- widget div-->\n<div class=\"no-padding\">\n\n    <div class=\"widget-body\">\n        <!-- content -->\n        <div id=\"myTabContent\" class=\"tab-content\">\n            <div class=\"tab-pane fade active in padding-10 no-padding-bottom\" id=\"s1\">\n                <div class=\"row no-space\">\n                    <div class=\"col-xs-12 col-sm-12 col-md-8 col-lg-8\">\n														<span class=\"demo-liveupdate-1\"> <span\n                                                                class=\"onoffswitch-title\">Live switch</span> <span\n                                                                class=\"onoffswitch\">\n																<input type=\"checkbox\" name=\"start_interval\" ng-model=\"autoUpdate\"\n                                                                       class=\"onoffswitch-checkbox\" id=\"start_interval\">\n																<label class=\"onoffswitch-label\" for=\"start_interval\">\n                                                                    <span class=\"onoffswitch-inner\"\n                                                                          data-swchon-text=\"ON\"\n                                                                          data-swchoff-text=\"OFF\"></span>\n                                                                    <span class=\"onoffswitch-switch\"></span>\n                                                                </label> </span> </span>\n\n                        <div id=\"updating-chart\" class=\"chart-large txt-color-blue\" flot-basic flot-data=\"liveStats\" flot-options=\"liveStatsOptions\"></div>\n\n                    </div>\n                    <div class=\"col-xs-12 col-sm-12 col-md-4 col-lg-4 show-stats\">\n\n                        <div class=\"row\">\n                            <div class=\"col-xs-6 col-sm-6 col-md-12 col-lg-12\"><span class=\"text\"> My Tasks <span\n                                    class=\"pull-right\">130/200</span> </span>\n\n                                <div class=\"progress\">\n                                    <div class=\"progress-bar bg-color-blueDark\" style=\"width: 65%;\"></div>\n                                </div>\n                            </div>\n                            <div class=\"col-xs-6 col-sm-6 col-md-12 col-lg-12\"><span class=\"text\"> Transfered <span\n                                    class=\"pull-right\">440 GB</span> </span>\n\n                                <div class=\"progress\">\n                                    <div class=\"progress-bar bg-color-blue\" style=\"width: 34%;\"></div>\n                                </div>\n                            </div>\n                            <div class=\"col-xs-6 col-sm-6 col-md-12 col-lg-12\"><span class=\"text\"> Bugs Squashed<span\n                                    class=\"pull-right\">77%</span> </span>\n\n                                <div class=\"progress\">\n                                    <div class=\"progress-bar bg-color-blue\" style=\"width: 77%;\"></div>\n                                </div>\n                            </div>\n                            <div class=\"col-xs-6 col-sm-6 col-md-12 col-lg-12\"><span class=\"text\"> User Testing <span\n                                    class=\"pull-right\">7 Days</span> </span>\n\n                                <div class=\"progress\">\n                                    <div class=\"progress-bar bg-color-greenLight\" style=\"width: 84%;\"></div>\n                                </div>\n                            </div>\n\n                            <span class=\"show-stat-buttons\"> <span class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\"> <a\n                                    href-void class=\"btn btn-default btn-block hidden-xs\">Generate PDF</a> </span> <span\n                                    class=\"col-xs-12 col-sm-6 col-md-6 col-lg-6\"> <a href-void\n                                                                                     class=\"btn btn-default btn-block hidden-xs\">Report\n                                a bug</a> </span> </span>\n\n                        </div>\n\n                    </div>\n                </div>\n\n                <div class=\"show-stat-microcharts\" data-sparkline-container data-easy-pie-chart-container>\n                    <div class=\"col-xs-12 col-sm-3 col-md-3 col-lg-3\">\n\n                        <div class=\"easy-pie-chart txt-color-orangeDark\" data-percent=\"33\" data-pie-size=\"50\">\n                            <span class=\"percent percent-sign\">35</span>\n                        </div>\n                        <span class=\"easy-pie-title\"> Server Load <i class=\"fa fa-caret-up icon-color-bad\"></i> </span>\n                        <ul class=\"smaller-stat hidden-sm pull-right\">\n                            <li>\n                                <span class=\"label bg-color-greenLight\"><i class=\"fa fa-caret-up\"></i> 97%</span>\n                            </li>\n                            <li>\n                                <span class=\"label bg-color-blueLight\"><i class=\"fa fa-caret-down\"></i> 44%</span>\n                            </li>\n                        </ul>\n                        <div class=\"sparkline txt-color-greenLight hidden-sm hidden-md pull-right\"\n                             data-sparkline-type=\"line\" data-sparkline-height=\"33px\" data-sparkline-width=\"70px\"\n                             data-fill-color=\"transparent\">\n                            130, 187, 250, 257, 200, 210, 300, 270, 363, 247, 270, 363, 247\n                        </div>\n                    </div>\n                    <div class=\"col-xs-12 col-sm-3 col-md-3 col-lg-3\">\n                        <div class=\"easy-pie-chart txt-color-greenLight\" data-percent=\"78.9\" data-pie-size=\"50\">\n                            <span class=\"percent percent-sign\">78.9 </span>\n                        </div>\n                        <span class=\"easy-pie-title\"> Disk Space <i class=\"fa fa-caret-down icon-color-good\"></i></span>\n                        <ul class=\"smaller-stat hidden-sm pull-right\">\n                            <li>\n                                <span class=\"label bg-color-blueDark\"><i class=\"fa fa-caret-up\"></i> 76%</span>\n                            </li>\n                            <li>\n                                <span class=\"label bg-color-blue\"><i class=\"fa fa-caret-down\"></i> 3%</span>\n                            </li>\n                        </ul>\n                        <div class=\"sparkline txt-color-blue hidden-sm hidden-md pull-right\" data-sparkline-type=\"line\"\n                             data-sparkline-height=\"33px\" data-sparkline-width=\"70px\" data-fill-color=\"transparent\">\n                            257, 200, 210, 300, 270, 363, 130, 187, 250, 247, 270, 363, 247\n                        </div>\n                    </div>\n                    <div class=\"col-xs-12 col-sm-3 col-md-3 col-lg-3\">\n                        <div class=\"easy-pie-chart txt-color-blue\" data-percent=\"23\" data-pie-size=\"50\">\n                            <span class=\"percent percent-sign\">23 </span>\n                        </div>\n                        <span class=\"easy-pie-title\"> Transfered <i class=\"fa fa-caret-up icon-color-good\"></i></span>\n                        <ul class=\"smaller-stat hidden-sm pull-right\">\n                            <li>\n                                <span class=\"label bg-color-darken\">10GB</span>\n                            </li>\n                            <li>\n                                <span class=\"label bg-color-blueDark\"><i class=\"fa fa-caret-up\"></i> 10%</span>\n                            </li>\n                        </ul>\n                        <div class=\"sparkline txt-color-darken hidden-sm hidden-md pull-right\"\n                             data-sparkline-type=\"line\" data-sparkline-height=\"33px\" data-sparkline-width=\"70px\"\n                             data-fill-color=\"transparent\">\n                            200, 210, 363, 247, 300, 270, 130, 187, 250, 257, 363, 247, 270\n                        </div>\n                    </div>\n                    <div class=\"col-xs-12 col-sm-3 col-md-3 col-lg-3\">\n                        <div class=\"easy-pie-chart txt-color-darken\" data-percent=\"36\" data-pie-size=\"50\">\n                            <span class=\"percent degree-sign\">36 <i class=\"fa fa-caret-up\"></i></span>\n                        </div>\n                        <span class=\"easy-pie-title\"> Temperature <i\n                                class=\"fa fa-caret-down icon-color-good\"></i></span>\n                        <ul class=\"smaller-stat hidden-sm pull-right\">\n                            <li>\n                                <span class=\"label bg-color-red\"><i class=\"fa fa-caret-up\"></i> 124</span>\n                            </li>\n                            <li>\n                                <span class=\"label bg-color-blue\"><i class=\"fa fa-caret-down\"></i> 40 F</span>\n                            </li>\n                        </ul>\n                        <div class=\"sparkline txt-color-red hidden-sm hidden-md pull-right\" data-sparkline-type=\"line\"\n                             data-sparkline-height=\"33px\" data-sparkline-width=\"70px\" data-fill-color=\"transparent\">\n                            2700, 3631, 2471, 2700, 3631, 2471, 1300, 1877, 2500, 2577, 2000, 2100, 3000\n                        </div>\n                    </div>\n                </div>\n\n            </div>\n            <!-- end s1 tab pane -->\n\n            <div class=\"tab-pane fade\" id=\"s2\">\n                <div class=\"widget-body-toolbar bg-color-white\">\n\n                    <form class=\"form-inline\" role=\"form\">\n\n                        <div class=\"form-group\">\n                            <label class=\"sr-only\" for=\"s123\">Show From</label>\n                            <input type=\"email\" class=\"form-control input-sm\" id=\"s123\" placeholder=\"Show From\">\n                        </div>\n                        <div class=\"form-group\">\n                            <input type=\"email\" class=\"form-control input-sm\" id=\"s124\" placeholder=\"To\">\n                        </div>\n\n                        <div class=\"btn-group hidden-phone pull-right\">\n                            <a class=\"btn dropdown-toggle btn-xs btn-default\" data-toggle=\"dropdown\"><i\n                                    class=\"fa fa-cog\"></i> More <span class=\"caret\"> </span> </a>\n                            <ul class=\"dropdown-menu pull-right\">\n                                <li>\n                                    <a href-void><i class=\"fa fa-file-text-alt\"></i> Export to PDF</a>\n                                </li>\n                                <li>\n                                    <a href-void><i class=\"fa fa-question-sign\"></i> Help</a>\n                                </li>\n                            </ul>\n                        </div>\n\n                    </form>\n\n                </div>\n                <div class=\"padding-10\">\n                    <div id=\"statsChart\" class=\"chart-large has-legend-unique\" flot-basic flot-data=\"statsData\" flot-options=\"statsDisplayOptions\"></div>\n                </div>\n\n            </div>\n            <!-- end s2 tab pane -->\n\n            <div class=\"tab-pane fade\" id=\"s3\">\n\n                <div class=\"widget-body-toolbar bg-color-white smart-form\" id=\"rev-toggles\">\n\n                    <div class=\"inline-group\">\n\n                        <label for=\"gra-0\" class=\"checkbox\">\n                            <input type=\"checkbox\" id=\"gra-0\" ng-model=\"targetsShow\">\n                            <i></i> Target </label>\n                        <label for=\"gra-1\" class=\"checkbox\">\n                            <input type=\"checkbox\" id=\"gra-1\" ng-model=\"actualsShow\">\n                            <i></i> Actual </label>\n                        <label for=\"gra-2\" class=\"checkbox\">\n                            <input type=\"checkbox\" id=\"gra-2\" ng-model=\"signupsShow\">\n                            <i></i> Signups </label>\n                    </div>\n\n                    <div class=\"btn-group hidden-phone pull-right\">\n                        <a class=\"btn dropdown-toggle btn-xs btn-default\" data-toggle=\"dropdown\"><i\n                                class=\"fa fa-cog\"></i> More <span class=\"caret\"> </span> </a>\n                        <ul class=\"dropdown-menu pull-right\">\n                            <li>\n                                <a href-void><i class=\"fa fa-file-text-alt\"></i> Export to PDF</a>\n                            </li>\n                            <li>\n                                <a href-void><i class=\"fa fa-question-sign\"></i> Help</a>\n                            </li>\n                        </ul>\n                    </div>\n\n                </div>\n\n                <div class=\"padding-10\">\n                    <div id=\"flotcontainer\" class=\"chart-large has-legend-unique\" flot-basic flot-data=\"revenewData\" flot-options=\"revenewDisplayOptions\" ></div>\n                </div>\n            </div>\n            <!-- end s3 tab pane -->\n        </div>\n\n        <!-- end content -->\n    </div>\n\n</div>\n<!-- end widget div -->\n</div>\n");
$templateCache.put("app/layout/layout.tpl.html","<!-- HEADER -->\n<div data-smart-include=\"app/layout/partials/header.tpl.html\" class=\"placeholder-header\"></div>\n<!-- END HEADER -->\n\n\n<!-- Left panel : Navigation area -->\n<!-- Note: This width of the aside area can be adjusted through LESS variables -->\n<div data-smart-include=\"app/layout/partials/navigation.tpl.html\" class=\"placeholder-left-panel\"></div>\n\n<!-- END NAVIGATION -->\n\n<!-- MAIN PANEL -->\n<div id=\"main\" role=\"main\" ng-show=\"globalShowContent\" class=\"ng-hide\">\n\n    <!-- RIBBON -->\n    <div id=\"ribbon\">\n\n        <!-- breadcrumb -->\n        <state-breadcrumbs></state-breadcrumbs>\n        <!-- end breadcrumb -->\n\n\n    </div>\n    <!-- END RIBBON -->\n\n\n    <div data-smart-router-animation-wrap=\"content content@app\" data-wrap-for=\"#content\">\n        <div data-ui-view=\"content\" data-autoscroll=\"false\"></div>\n    </div>\n\n</div>\n<!-- END MAIN PANEL -->\n\n<!-- PAGE FOOTER -->\n<div data-smart-include=\"app/layout/partials/footer.tpl.html\"></div>\n\n<div data-smart-include=\"app/layout/shortcut/shortcut.tpl.html\"></div>\n\n<!-- END PAGE FOOTER -->\n\n\n");
$templateCache.put("app/dashboard/projects/recent-projects.tpl.html","<div class=\"project-context hidden-xs dropdown\" dropdown>\n\n    <span class=\"label\">{{getWord(\'Projects\')}}:</span>\n    <span class=\"project-selector dropdown-toggle\" data-toggle=\"dropdown\">{{getWord(\'Recent projects\')}} <i ng-if=\"projects.length\"\n            class=\"fa fa-angle-down\"></i></span>\n\n    <ul class=\"dropdown-menu\" ng-if=\"projects.length\">\n        <li ng-repeat=\"project in projects\">\n            <a href=\"{{project.href}}\">{{project.title}}</a>\n        </li>\n        <li class=\"divider\"></li>\n        <li>\n            <a ng-click=\"clearProjects()\"><i class=\"fa fa-power-off\"></i> Clear</a>\n        </li>\n    </ul>\n\n</div>");
$templateCache.put("app/dashboard/todo/todo-widget.tpl.html","<div id=\"todo-widget\" jarvis-widget data-widget-editbutton=\"false\" data-widget-color=\"blue\"\n     ng-controller=\"TodoCtrl\">\n    <header>\n        <span class=\"widget-icon\"> <i class=\"fa fa-check txt-color-white\"></i> </span>\n\n        <h2> ToDo\'s </h2>\n\n        <div class=\"widget-toolbar\">\n            <!-- add: non-hidden - to disable auto hide -->\n            <button class=\"btn btn-xs btn-default\" ng-class=\"{active: newTodo}\" ng-click=\"toggleAdd()\"><i ng-class=\"{ \'fa fa-plus\': !newTodo, \'fa fa-times\': newTodo}\"></i> Add</button>\n\n        </div>\n    </header>\n    <!-- widget div-->\n    <div>\n        <div class=\"widget-body no-padding smart-form\">\n            <!-- content goes here -->\n            <div ng-show=\"newTodo\">\n                <h5 class=\"todo-group-title\"><i class=\"fa fa-plus-circle\"></i> New Todo</h5>\n\n                <form name=\"newTodoForm\" class=\"smart-form\">\n                    <fieldset>\n                        <section>\n                            <label class=\"input\">\n                                <input type=\"text\" required class=\"input-lg\" ng-model=\"newTodo.title\"\n                                       placeholder=\"What needs to be done?\">\n                            </label>\n                        </section>\n                        <section>\n                            <div class=\"col-xs-6\">\n                                <label class=\"select\">\n                                    <select class=\"input-sm\" ng-model=\"newTodo.state\"\n                                            ng-options=\"state as state for state in states\"></select> <i></i> </label>\n                            </div>\n                        </section>\n                    </fieldset>\n                    <footer>\n                        <button ng-disabled=\"newTodoForm.$invalid\" type=\"button\" class=\"btn btn-primary\"\n                                ng-click=\"createTodo()\">\n                            Add\n                        </button>\n                        <button type=\"button\" class=\"btn btn-default\" ng-click=\"toggleAdd()\">\n                            Cancel\n                        </button>\n                    </footer>\n                </form>\n            </div>\n\n            <todo-list state=\"Critical\"  title=\"Critical Tasks\" icon=\"warning\" todos=\"todos\"></todo-list>\n\n            <todo-list state=\"Important\" title=\"Important Tasks\" icon=\"exclamation\" todos=\"todos\"></todo-list>\n\n            <todo-list state=\"Completed\" title=\"Completed Tasks\" icon=\"check\" todos=\"todos\"></todo-list>\n\n            <!-- end content -->\n        </div>\n\n    </div>\n    <!-- end widget div -->\n</div>");
$templateCache.put("app/layout/language/language-selector.tpl.html","<ul class=\"header-dropdown-list hidden-xs ng-cloak\" ng-controller=\"LanguagesCtrl\">\n    <li class=\"dropdown\" dropdown>\n        <a class=\"dropdown-toggle\"  data-toggle=\"dropdown\" href> <img src=\"styles/img/blank.gif\" class=\"flag flag-{{currentLanguage.key}}\" alt=\"{{currentLanguage.alt}}\"> <span> {{currentLanguage.title}} </span>\n            <i class=\"fa fa-angle-down\"></i> </a>\n        <ul class=\"dropdown-menu pull-right\">\n            <li ng-class=\"{active: language==currentLanguage}\" ng-repeat=\"language in languages\">\n                <a ng-click=\"selectLanguage(language)\" ><img src=\"styles/img/blank.gif\" class=\"flag flag-{{language.key}}\"\n                                                   alt=\"{{language.alt}}\"> {{language.title}}</a>\n            </li>\n        </ul>\n    </li>\n</ul>");
$templateCache.put("app/layout/partials/footer.tpl.html","<div class=\"page-footer\">\n    <div class=\"row\">\n        <div class=\"col-xs-6 col-sm-6 col-md-offset-6 text-right hidden-xs\">\n            <div class=\"txt-color-white inline-block\">\n                <i class=\"txt-color-blueLight hidden-mobile\">Last account activity <i class=\"fa fa-clock-o\"></i>\n                    <strong>52 mins ago &nbsp;</strong> </i>\n\n                <div class=\"btn-group dropup\">\n                    <button class=\"btn btn-xs dropdown-toggle bg-color-blue txt-color-white\" data-toggle=\"dropdown\">\n                        <i class=\"fa fa-link\"></i> <span class=\"caret\"></span>\n                    </button>\n                    <ul class=\"dropdown-menu pull-right text-left\">\n                        <li>\n                            <div class=\"padding-5\">\n                                <p class=\"txt-color-darken font-sm no-margin\">Download Progress</p>\n\n                                <div class=\"progress progress-micro no-margin\">\n                                    <div class=\"progress-bar progress-bar-success\" style=\"width: 50%;\"></div>\n                                </div>\n                            </div>\n                        </li>\n                        <li class=\"divider\"></li>\n                        <li>\n                            <div class=\"padding-5\">\n                                <p class=\"txt-color-darken font-sm no-margin\">Server Load</p>\n\n                                <div class=\"progress progress-micro no-margin\">\n                                    <div class=\"progress-bar progress-bar-success\" style=\"width: 20%;\"></div>\n                                </div>\n                            </div>\n                        </li>\n                        <li class=\"divider\"></li>\n                        <li>\n                            <div class=\"padding-5\">\n                                <p class=\"txt-color-darken font-sm no-margin\">Memory Load <span class=\"text-danger\">*critical*</span>\n                                </p>\n\n                                <div class=\"progress progress-micro no-margin\">\n                                    <div class=\"progress-bar progress-bar-danger\" style=\"width: 70%;\"></div>\n                                </div>\n                            </div>\n                        </li>\n                        <li class=\"divider\"></li>\n                        <li>\n                            <div class=\"padding-5\">\n                                <button class=\"btn btn-block btn-default\">refresh</button>\n                            </div>\n                        </li>\n                    </ul>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>");
$templateCache.put("app/layout/partials/header.tpl.html","<header id=\"header\">\n<div id=\"logo-group\">\n    <!-- collapse menu button -->\n    <div id=\"hide-menu\" class=\"btn-header pull-left\">\n        <span> <a toggle-menu title=\"Collapse Menu\" style=\"box-sizing: border-box;\"><i\n                class=\"fa fa-reorder\"></i></a> </span>\n    </div>\n    <!-- end collapse menu -->\n    <!-- Note: The activity badge color changes when clicked and resets the number to 0\n    Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->\n    <span id=\"activity\" class=\"activity-dropdown\" activities-dropdown-toggle> \n        <i class=\"fa fa-user\"></i> \n        <b class=\"badge bg-color-red\">21</b> \n    </span>\n    <div smart-include=\"app/dashboard/activities/activities.html\"></div>\n\n    <!-- fullscreen button -->\n    <div id=\"fullscreen\" class=\"btn-header transparent pull-left\">\n        <span> <a full-screen title=\"Full Screen\" style=\"box-sizing: border-box;\"><i\n                class=\"fa fa-arrows-alt\"></i></a> </span>\n    </div>\n    <!-- end fullscreen button -->\n</div>\n\n<!-- pulled right: nav area -->\n<div class=\"pull-right\">\n\n    <!-- #MOBILE -->\n    <!-- logout button -->\n    <div id=\"logout\" class=\"btn-header transparent no-style pull-right\">\n        <span>\n            <a user-logout style=\"width: auto !important;\">\n                <i class=\"fa fa-sign-out\"></i>\n                <span style=\"font-size: 70%;\"> Cerrar Sesión </span>\n            </a>\n        </span>\n    </div>\n    <!-- end logout button -->\n\n    <!-- search mobile button (this is hidden till mobile view port) -->\n    <div ng-if=\"false\" id=\"search-mobile\" class=\"btn-header transparent pull-right\" data-search-mobile>\n        <span> <a href=\"#\" title=\"Search\"><i class=\"fa fa-search\"></i></a> </span>\n    </div>\n    <!-- end search mobile button -->\n\n    <!-- input: search field -->\n    <form ng-if=\"false\" action=\"#/search\" class=\"header-search pull-right\">\n        <input id=\"search-fld\" type=\"text\" name=\"param\" placeholder=\"Find reports and more\" data-autocomplete=\'[\n					\"ActionScript\",\n					\"AppleScript\",\n					\"Asp\",\n					\"BASIC\",\n					\"C\",\n					\"C++\",\n					\"Clojure\",\n					\"COBOL\",\n					\"ColdFusion\",\n					\"Erlang\",\n					\"Fortran\",\n					\"Groovy\",\n					\"Haskell\",\n					\"Java\",\n					\"JavaScript\",\n					\"Lisp\",\n					\"Perl\",\n					\"PHP\",\n					\"Python\",\n					\"Ruby\",\n					\"Scala\",\n					\"Scheme\"]\'>\n        <button type=\"submit\">\n            <i class=\"fa fa-search\"></i>\n        </button>\n        <a href=\"$\" id=\"cancel-search-js\" title=\"Cancel Search\"><i class=\"fa fa-times\"></i></a>\n    </form>\n    <!-- end input: search field -->\n</div>\n<!-- end pulled right: nav area -->\n\n</header>");
$templateCache.put("app/layout/partials/navigation.tpl.html","<aside id=\"left-panel\">\n\n    <div class=\"navigation-logo\"></div>\n\n    <nav>\n        <!-- NOTE: Notice the gaps after each icon usage <i></i>..\n        Please note that these links work a bit different than\n        traditional href=\"\" links. See documentation for details.\n        -->\n\n        <ul data-smart-menu>\n\n            <li data-ui-sref-active=\"active\">\n                <a data-ui-sref=\"app.monitor\" title=\"Estatus de Infraestructura\">\n                    <i class=\"fa fa-lg fa-fw fa-gears\"></i> <span class=\"menu-item-parent\">Estatus de Infraestructura</span>\n                </a>\n            </li>\n            <li data-ui-sref-active=\"active\">\n                <a data-ui-sref=\"app.operativo\" title=\"Operativo\">\n                    <i class=\"fa fa-lg fa-fw fa-wrench\"></i> <span class=\"menu-item-parent\">Operativo</span>\n                </a>\n            </li>\n            <li data-menu-collapse\n                >\n                <a>\n                    <i class=\"fa fa-lg fa-fw fa-th-large\"></i> <span class=\"menu-item-parent\">Gestion Proyectos</span>\n                </a>\n                <ul>\n                    <li data-ui-sref-active=\"active\"\n                        >\n                        <a data-ui-sref=\"app.proyecto\">\n                            <i class=\"fa fa-fw fa-suitcase\"></i> <span>Proyectos</span>\n                        </a>\n                    </li>\n                    <li data-ui-sref-active=\"active\"\n                        has-permission=\"project.view, project.add, project.edit, project.delete\">\n                        <a data-ui-sref=\"app.proyecto.seccion\">\n                            <i class=\"fa fa-fw fa-th-list\"></i> <span>Secciones</span>\n                        </a>\n                    </li>\n                    <li data-ui-sref-active=\"active\"\n                        has-permission=\"project.view, project.add, project.edit, project.delete\">\n                        <a data-ui-sref=\"app.proyecto.seccion.poligono\">\n                            <i class=\"fa fa-fw fa-cube\"></i> <span>Poligonos</span>\n                        </a>\n                    </li>\n                    <li data-ui-sref-active=\"active\"\n                        has-permission=\"project.view, project.add, project.edit, project.delete\">\n                        <a data-ui-sref=\"app.proyecto.seccion.poligono.circuito\">\n                            <i class=\"fa fa-fw fa-microchip\"></i> <span>Circuitos</span>\n                        </a>\n                    </li>\n                </ul>\n            </li>\n            <li data-menu-collapse\n                has-permission=\"user.view, user.add, user.edit, user.delete,\n                                profile.view, profile.add, profile.edit, profile.delete,\n                                module.view, module.add, module.edit, module.delete\">\n                <a>\n                    <i class=\"fa fa-lg fa-fw fa-user-circle-o\"></i> <span class=\"menu-item-parent\">Administración</span>\n                </a>\n                <ul>\n                    <li data-ui-sref-active=\"active\"\n                        has-permission=\"user.view, user.add, user.edit, user.delete\">\n                        <a data-ui-sref=\"app.admin.usuarios\">\n                            <i class=\"fa fa-fw fa-user\"></i> <span>Usuarios</span>\n                        </a>\n                    </li>\n                    <li data-ui-sref-active=\"active\"\n                        has-permission=\"profile.view, profile.add, profile.edit, profile.delete\">\n                        <a data-ui-sref=\"app.admin.perfiles\">\n                            <i class=\"fa fa-fw fa-id-card\"></i> <span>Perfiles</span>\n                        </a>\n                    </li>\n                    <li data-ui-sref-active=\"active\"\n                        has-permission=\"module.view, module.add, module.edit, module.delete\">\n                        <a data-ui-sref=\"app.admin.modulos\">\n                            <i class=\"fa fa-fw fa-shield\"></i> <span>Modulos</span>\n                        </a>\n                    </li>\n                </ul>\n            </li>\n\n        </ul>\n\n        <!-- NOTE: This allows you to pull menu items from server -->\n        <!-- <ul data-smart-menu-items=\"/api/menu-items.json\"></ul> -->\n    </nav>\n\n  <span class=\"minifyme\" data-action=\"minifyMenu\" minify-menu>\n    <i class=\"fa fa-arrow-circle-left hit\"></i>\n  </span>\n\n</aside>");
$templateCache.put("app/layout/partials/sub-header.tpl.html","<div class=\"col-xs-12 col-sm-5 col-md-5 col-lg-8\" data-sparkline-container>\n    <ul id=\"sparks\" class=\"\">\n        <li class=\"sparks-info\">\n            <h5> My Income <span class=\"txt-color-blue\">$47,171</span></h5>\n            <div class=\"sparkline txt-color-blue hidden-mobile hidden-md hidden-sm\">\n                1300, 1877, 2500, 2577, 2000, 2100, 3000, 2700, 3631, 2471, 2700, 3631, 2471\n            </div>\n        </li>\n        <li class=\"sparks-info\">\n            <h5> Site Traffic <span class=\"txt-color-purple\"><i class=\"fa fa-arrow-circle-up\"></i>&nbsp;45%</span></h5>\n            <div class=\"sparkline txt-color-purple hidden-mobile hidden-md hidden-sm\">\n                110,150,300,130,400,240,220,310,220,300, 270, 210\n            </div>\n        </li>\n        <li class=\"sparks-info\">\n            <h5> Site Orders <span class=\"txt-color-greenDark\"><i class=\"fa fa-shopping-cart\"></i>&nbsp;2447</span></h5>\n            <div class=\"sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm\">\n                110,150,300,130,400,240,220,310,220,300, 270, 210\n            </div>\n        </li>\n    </ul>\n</div>\n			");
$templateCache.put("app/layout/partials/voice-commands.tpl.html","<!-- TRIGGER BUTTON:\n<a href=\"/my-ajax-page.html\" data-toggle=\"modal\" data-target=\"#remoteModal\" class=\"btn btn-default\">Open Modal</a>  -->\n\n<!-- MODAL PLACE HOLDER\n<div class=\"modal fade\" id=\"remoteModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"remoteModalLabel\" aria-hidden=\"true\">\n<div class=\"modal-dialog\">\n<div class=\"modal-content\"></div>\n</div>\n</div>   -->\n<!--////////////////////////////////////-->\n\n<!--<div class=\"modal-header\">\n<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">\n&times;\n</button>\n<h4 class=\"modal-title\" id=\"myModalLabel\">Command List</h4>\n</div>-->\n<div class=\"modal-body\">\n\n	<h1><i class=\"fa fa-microphone text-muted\"></i>&nbsp;&nbsp; SmartAdmin Voice Command</h1>\n	<hr class=\"simple\">\n	<h5>Instruction</h5>\n\n	Click <span class=\"text-success\">\"Allow\"</span> to access your microphone and activate Voice Command.\n	You will notice a <span class=\"text-primary\"><strong>BLUE</strong> Flash</span> on the microphone icon indicating activation.\n	The icon will appear <span class=\"text-danger\"><strong>RED</strong></span> <span class=\"label label-danger\"><i class=\"fa fa-microphone fa-lg\"></i></span> if you <span class=\"text-danger\">\"Deny\"</span> access or don\'t have any microphone installed.\n	<br>\n	<br>\n	As a security precaution, your browser will disconnect the microphone every 60 to 120 seconds (sooner if not being used). In which case Voice Command will prompt you again to <span class=\"text-success\">\"Allow\"</span> or <span class=\"text-danger\">\"Deny\"</span> access to your microphone.\n	<br>\n	<br>\n	If you host your page over <strong>http<span class=\"text-success\">s</span></strong> (secure socket layer) protocol you can wave this security measure and have an unintrupted Voice Command.\n	<br>\n	<br>\n	<h5>Commands</h5>\n	<ul>\n		<li>\n			<strong>\'show\' </strong> then say the <strong>*page*</strong> you want to go to. For example <strong>\"show inbox\"</strong> or <strong>\"show calendar\"</strong>\n		</li>\n		<li>\n			<strong>\'mute\' </strong> - mutes all sound effects for the theme.\n		</li>\n		<li>\n			<strong>\'sound on\'</strong> - unmutes all sound effects for the theme.\n		</li>\n		<li>\n			<span class=\"text-danger\"><strong>\'stop\'</strong></span> - deactivates voice command.\n		</li>\n		<li>\n			<span class=\"text-primary\"><strong>\'help\'</strong></span> - brings up the command list\n		</li>\n		<li>\n			<span class=\"text-danger\"><strong>\'got it\'</strong></span> - closes help modal\n		</li>\n		<li>\n			<strong>\'hide navigation\'</strong> - toggle navigation collapse\n		</li>\n		<li>\n			<strong>\'show navigation\'</strong> - toggle navigation to open (can be used again to close)\n		</li>\n		<li>\n			<strong>\'scroll up\'</strong> - scrolls to the top of the page\n		</li>\n		<li>\n			<strong>\'scroll down\'</strong> - scrollts to the bottom of the page\n		</li>\n		<li>\n			<strong>\'go back\' </strong> - goes back in history (history -1 click)\n		</li>\n		<li>\n			<strong>\'logout\'</strong> - logs you out\n		</li>\n	</ul>\n	<br>\n	<h5>Adding your own commands</h5>\n	Voice Command supports up to 80 languages. Adding your own commands is extreamly easy. All commands are stored inside <strong>app.config.js</strong> file under the <code>var commands = {...}</code>. \n\n	<hr class=\"simple\">\n	<div class=\"text-right\">\n		<button type=\"button\" class=\"btn btn-success btn-lg\" data-dismiss=\"modal\">\n			Got it!\n		</button>\n	</div>\n\n</div>\n<!--<div class=\"modal-footer\">\n<button type=\"button\" class=\"btn btn-primary\" data-dismiss=\"modal\">Got it!</button>\n</div> -->");
$templateCache.put("app/layout/shortcut/shortcut.tpl.html","<div id=\"shortcut\">\n	<ul>\n		<li>\n			<a href=\"#/inbox/\" class=\"jarvismetro-tile big-cubes bg-color-blue\"> <span class=\"iconbox\"> <i class=\"fa fa-envelope fa-4x\"></i> <span>Mail <span class=\"label pull-right bg-color-darken\">14</span></span> </span> </a>\n		</li>\n		<li>\n			<a href=\"#/calendar\" class=\"jarvismetro-tile big-cubes bg-color-orangeDark\"> <span class=\"iconbox\"> <i class=\"fa fa-calendar fa-4x\"></i> <span>Calendar</span> </span> </a>\n		</li>\n		<li>\n			<a href=\"#/maps\" class=\"jarvismetro-tile big-cubes bg-color-purple\"> <span class=\"iconbox\"> <i class=\"fa fa-map-marker fa-4x\"></i> <span>Maps</span> </span> </a>\n		</li>\n		<li>\n			<a href=\"#/invoice\" class=\"jarvismetro-tile big-cubes bg-color-blueDark\"> <span class=\"iconbox\"> <i class=\"fa fa-book fa-4x\"></i> <span>Invoice <span class=\"label pull-right bg-color-darken\">99</span></span> </span> </a>\n		</li>\n		<li>\n			<a href=\"#/gallery\" class=\"jarvismetro-tile big-cubes bg-color-greenLight\"> <span class=\"iconbox\"> <i class=\"fa fa-picture-o fa-4x\"></i> <span>Gallery </span> </span> </a>\n		</li>\n		<li>\n			<a href=\"#/profile\" class=\"jarvismetro-tile big-cubes selected bg-color-pinkDark\"> <span class=\"iconbox\"> <i class=\"fa fa-user fa-4x\"></i> <span>My Profile </span> </span> </a>\n		</li>\n	</ul>\n</div>");
$templateCache.put("app/dashboard/chat/directives/aside-chat-widget.tpl.html","<ul>\n    <li>\n        <div class=\"display-users\">\n            <input class=\"form-control chat-user-filter\" placeholder=\"Filter\" type=\"text\">\n            <dl>\n                <dt>\n                    <a href=\"#\" class=\"usr\"\n                       data-chat-id=\"cha1\"\n                       data-chat-fname=\"Sadi\"\n                       data-chat-lname=\"Orlaf\"\n                       data-chat-status=\"busy\"\n                       data-chat-alertmsg=\"Sadi Orlaf is in a meeting. Please do not disturb!\"\n                       data-chat-alertshow=\"true\"\n                       popover-trigger=\"hover\"\n                       popover-placement=\"right\"\n                       smart-popover-html=\"\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/5.png\' alt=\'Sadi Orlaf\'>\n											<div class=\'usr-card-content\'>\n												<h3>Sadi Orlaf</h3>\n												<p>Marketing Executive</p>\n											</div>\n										</div>\n									\">\n                        <i></i>Sadi Orlaf\n                    </a>\n                </dt>\n                <dt>\n                    <a href=\"#\" class=\"usr\"\n                       data-chat-id=\"cha2\"\n                       data-chat-fname=\"Jessica\"\n                       data-chat-lname=\"Dolof\"\n                       data-chat-status=\"online\"\n                       data-chat-alertmsg=\"\"\n                       data-chat-alertshow=\"false\"\n                       popover-trigger=\"hover\"\n                       popover-placement=\"right\"\n                       smart-popover-html=\"\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/1.png\' alt=\'Jessica Dolof\'>\n											<div class=\'usr-card-content\'>\n												<h3>Jessica Dolof</h3>\n												<p>Sales Administrator</p>\n											</div>\n										</div>\n									\">\n                        <i></i>Jessica Dolof\n                    </a>\n                </dt>\n                <dt>\n                    <a href=\"#\" class=\"usr\"\n                       data-chat-id=\"cha3\"\n                       data-chat-fname=\"Zekarburg\"\n                       data-chat-lname=\"Almandalie\"\n                       data-chat-status=\"online\"\n                       popover-trigger=\"hover\"\n                       popover-placement=\"right\"\n                       smart-popover-html=\"\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/3.png\' alt=\'Zekarburg Almandalie\'>\n											<div class=\'usr-card-content\'>\n												<h3>Zekarburg Almandalie</h3>\n												<p>Sales Admin</p>\n											</div>\n										</div>\n									\">\n                        <i></i>Zekarburg Almandalie\n                    </a>\n                </dt>\n                <dt>\n                    <a href=\"#\" class=\"usr\"\n                       data-chat-id=\"cha4\"\n                       data-chat-fname=\"Barley\"\n                       data-chat-lname=\"Krazurkth\"\n                       data-chat-status=\"away\"\n                       popover-trigger=\"hover\"\n                       popover-placement=\"right\"\n                       smart-popover-html=\"\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/4.png\' alt=\'Barley Krazurkth\'>\n											<div class=\'usr-card-content\'>\n												<h3>Barley Krazurkth</h3>\n												<p>Sales Director</p>\n											</div>\n										</div>\n									\">\n                        <i></i>Barley Krazurkth\n                    </a>\n                </dt>\n                <dt>\n                    <a href=\"#\" class=\"usr offline\"\n                       data-chat-id=\"cha5\"\n                       data-chat-fname=\"Farhana\"\n                       data-chat-lname=\"Amrin\"\n                       data-chat-status=\"incognito\"\n                       popover-trigger=\"hover\"\n                       popover-placement=\"right\"\n                       smart-popover-html=\"\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/female.png\' alt=\'Farhana Amrin\'>\n											<div class=\'usr-card-content\'>\n												<h3>Farhana Amrin</h3>\n												<p>Support Admin <small><i class=\'fa fa-music\'></i> Playing Beethoven Classics</small></p>\n											</div>\n										</div>\n									\">\n                        <i></i>Farhana Amrin (offline)\n                    </a>\n                </dt>\n                <dt>\n                    <a href=\"#\" class=\"usr offline\"\n                       data-chat-id=\"cha6\"\n                       data-chat-fname=\"Lezley\"\n                       data-chat-lname=\"Jacob\"\n                       data-chat-status=\"incognito\"\n                       popover-trigger=\"hover\"\n                       popover-placement=\"right\"\n                       smart-popover-html=\"\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/male.png\' alt=\'Lezley Jacob\'>\n											<div class=\'usr-card-content\'>\n												<h3>Lezley Jacob</h3>\n												<p>Sales Director</p>\n											</div>\n										</div>\n									\">\n                        <i></i>Lezley Jacob (offline)\n                    </a>\n                </dt>\n            </dl>\n\n\n            <!--<a href=\"chat.html\" class=\"btn btn-xs btn-default btn-block sa-chat-learnmore-btn\">About the API</a>-->\n        </div>\n    </li>\n</ul>");
$templateCache.put("app/dashboard/chat/directives/chat-users.tpl.html","<div id=\"chat-container\" ng-class=\"{open: open}\">\n    <span class=\"chat-list-open-close\" ng-click=\"openToggle()\"><i class=\"fa fa-user\"></i><b>!</b></span>\n\n    <div class=\"chat-list-body custom-scroll\">\n        <ul id=\"chat-users\">\n            <li ng-repeat=\"chatUser in chatUsers | filter: chatUserFilter\">\n                <a ng-click=\"messageTo(chatUser)\"><img ng-src=\"{{chatUser.picture}}\">{{chatUser.username}} <span\n                        class=\"badge badge-inverse\">{{chatUser.username.length}}</span><span class=\"state\"><i\n                        class=\"fa fa-circle txt-color-green pull-right\"></i></span></a>\n            </li>\n        </ul>\n    </div>\n    <div class=\"chat-list-footer\">\n        <div class=\"control-group\">\n            <form class=\"smart-form\">\n                <section>\n                    <label class=\"input\" >\n                        <input type=\"text\" ng-model=\"chatUserFilter\" id=\"filter-chat-list\" placeholder=\"Filter\">\n                    </label>\n                </section>\n            </form>\n        </div>\n    </div>\n</div>");
$templateCache.put("app/dashboard/chat/directives/chat-widget.tpl.html","<div id=\"chat-widget\" jarvis-widget data-widget-color=\"blueDark\" data-widget-editbutton=\"false\"\n     data-widget-fullscreenbutton=\"false\">\n\n\n    <header>\n        <span class=\"widget-icon\"> <i class=\"fa fa-comments txt-color-white\"></i> </span>\n\n        <h2> SmartMessage </h2>\n\n        <div class=\"widget-toolbar\">\n            <!-- add: non-hidden - to disable auto hide -->\n\n            <div class=\"btn-group\" data-dropdown>\n                <button class=\"btn dropdown-toggle btn-xs btn-success\" data-toggle=\"dropdown\">\n                    Status <i class=\"fa fa-caret-down\"></i>\n                </button>\n                <ul class=\"dropdown-menu pull-right js-status-update\">\n                    <li>\n                        <a href-void><i class=\"fa fa-circle txt-color-green\"></i> Online</a>\n                    </li>\n                    <li>\n                        <a href-void><i class=\"fa fa-circle txt-color-red\"></i> Busy</a>\n                    </li>\n                    <li>\n                        <a href-void><i class=\"fa fa-circle txt-color-orange\"></i> Away</a>\n                    </li>\n                    <li class=\"divider\"></li>\n                    <li>\n                        <a href-void><i class=\"fa fa-power-off\"></i> Log Off</a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </header>\n\n    <!-- widget div-->\n    <div>\n        <div class=\"widget-body widget-hide-overflow no-padding\">\n            <!-- content goes here -->\n\n            <chat-users></chat-users>\n\n            <!-- CHAT BODY -->\n            <div id=\"chat-body\" class=\"chat-body custom-scroll\">\n                <ul>\n                    <li class=\"message\" ng-repeat=\"message in chatMessages\">\n                        <img class=\"message-picture online\" ng-src=\"{{message.user.picture}}\">\n\n                        <div class=\"message-text\">\n                            <time>\n                                {{message.date | date }}\n                            </time>\n                            <a ng-click=\"messageTo(message.user)\" class=\"username\">{{message.user.username}}</a>\n                            <div ng-bind-html=\"message.body\"></div>\n\n                        </div>\n                    </li>\n                </ul>\n            </div>\n\n            <!-- CHAT FOOTER -->\n            <div class=\"chat-footer\">\n\n                <!-- CHAT TEXTAREA -->\n                <div class=\"textarea-div\">\n\n                    <div class=\"typearea\">\n                        <textarea placeholder=\"Write a reply...\" id=\"textarea-expand\"\n                                  class=\"custom-scroll\" ng-model=\"newMessage\"></textarea>\n                    </div>\n\n                </div>\n\n                <!-- CHAT REPLY/SEND -->\n											<span class=\"textarea-controls\">\n												<button class=\"btn btn-sm btn-primary pull-right\" ng-click=\"sendMessage()\">\n                                                    Reply\n                                                </button> <span class=\"pull-right smart-form\"\n                                                                style=\"margin-top: 3px; margin-right: 10px;\"> <label\n                                                    class=\"checkbox pull-right\">\n                                                <input type=\"checkbox\" name=\"subscription\" id=\"subscription\">\n                                                <i></i>Press <strong> ENTER </strong> to send </label> </span> <a\n                                                    href-void class=\"pull-left\"><i\n                                                    class=\"fa fa-camera fa-fw fa-lg\"></i></a> </span>\n\n            </div>\n\n            <!-- end content -->\n        </div>\n\n    </div>\n    <!-- end widget div -->\n</div>");
$templateCache.put("app/dashboard/todo/directives/todo-list.tpl.html","<div>\n    <h5 class=\"todo-group-title\"><i class=\"fa fa-{{icon}}\"></i> {{title}} (\n        <small class=\"num-of-tasks\">{{scopeItems.length}}</small>\n        )\n    </h5>\n    <ul class=\"todo\">\n        <li ng-class=\"{complete: todo.completedAt}\" ng-repeat=\"todo in todos | orderBy: todo._id | filter: filter  track by todo._id\" >\n    	<span class=\"handle\"> <label class=\"checkbox\">\n            <input type=\"checkbox\" ng-click=\"todo.toggle()\" ng-checked=\"todo.completedAt\"\n                   name=\"checkbox-inline\">\n            <i></i> </label> </span>\n\n            <p>\n                <strong>Ticket #{{$index + 1}}</strong> - {{todo.title}}\n                <span class=\"text-muted\" ng-if=\"todo.description\">{{todo.description}}</span>\n                <span class=\"date\">{{todo.createdAt | date}} &dash; <a ng-click=\"deleteTodo(todo)\" class=\"text-muted\"><i\n                        class=\"fa fa-trash\"></i></a></span>\n\n            </p>\n        </li>\n    </ul>\n</div>");
$templateCache.put("app/_common/forms/directives/bootstrap-validation/bootstrap-attribute-form.tpl.html","<form id=\"attributeForm\" class=\"form-horizontal\"\n      data-bv-message=\"This value is not valid\"\n      data-bv-feedbackicons-valid=\"glyphicon glyphicon-ok\"\n      data-bv-feedbackicons-invalid=\"glyphicon glyphicon-remove\"\n      data-bv-feedbackicons-validating=\"glyphicon glyphicon-refresh\">\n\n    <fieldset>\n        <legend>\n            Set validator options via HTML attributes\n        </legend>\n\n        <div class=\"alert alert-warning\">\n            <code>&lt; input\n                data-bv-validatorname\n                data-bv-validatorname-validatoroption=\"...\" / &gt;</code>\n\n            <br>\n            <br>\n            More validator options can be found here:\n            <a href=\"http://bootstrapvalidator.com/validators/\" target=\"_blank\">http://bootstrapvalidator.com/validators/</a>\n        </div>\n\n        <div class=\"form-group\">\n            <label class=\"col-lg-3 control-label\">Full name</label>\n            <div class=\"col-lg-4\">\n                <input type=\"text\" class=\"form-control\" name=\"firstName\" placeholder=\"First name\"\n                       data-bv-notempty=\"true\"\n                       data-bv-notempty-message=\"The first name is required and cannot be empty\" />\n            </div>\n            <div class=\"col-lg-4\">\n                <input type=\"text\" class=\"form-control\" name=\"lastName\" placeholder=\"Last name\"\n                       data-bv-notempty=\"true\"\n                       data-bv-notempty-message=\"The last name is required and cannot be empty\" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-lg-3 control-label\">Username</label>\n            <div class=\"col-lg-5\">\n                <input type=\"text\" class=\"form-control\" name=\"username\"\n                       data-bv-message=\"The username is not valid\"\n\n                       data-bv-notempty=\"true\"\n                       data-bv-notempty-message=\"The username is required and cannot be empty\"\n\n                       data-bv-regexp=\"true\"\n                       data-bv-regexp-regexp=\"^[a-zA-Z0-9_\\.]+$\"\n                       data-bv-regexp-message=\"The username can only consist of alphabetical, number, dot and underscore\"\n\n                       data-bv-stringlength=\"true\"\n                       data-bv-stringlength-min=\"6\"\n                       data-bv-stringlength-max=\"30\"\n                       data-bv-stringlength-message=\"The username must be more than 6 and less than 30 characters long\"\n\n                       data-bv-different=\"true\"\n                       data-bv-different-field=\"password\"\n                       data-bv-different-message=\"The username and password cannot be the same as each other\" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-lg-3 control-label\">Email address</label>\n            <div class=\"col-lg-5\">\n                <input class=\"form-control\" name=\"email\" type=\"email\"\n                       data-bv-emailaddress=\"true\"\n                       data-bv-emailaddress-message=\"The input is not a valid email address\" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-lg-3 control-label\">Password</label>\n            <div class=\"col-lg-5\">\n                <input type=\"password\" class=\"form-control\" name=\"password\"\n                       data-bv-notempty=\"true\"\n                       data-bv-notempty-message=\"The password is required and cannot be empty\"\n\n                       data-bv-identical=\"true\"\n                       data-bv-identical-field=\"confirmPassword\"\n                       data-bv-identical-message=\"The password and its confirm are not the same\"\n\n                       data-bv-different=\"true\"\n                       data-bv-different-field=\"username\"\n                       data-bv-different-message=\"The password cannot be the same as username\" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-lg-3 control-label\">Retype password</label>\n            <div class=\"col-lg-5\">\n                <input type=\"password\" class=\"form-control\" name=\"confirmPassword\"\n                       data-bv-notempty=\"true\"\n                       data-bv-notempty-message=\"The confirm password is required and cannot be empty\"\n\n                       data-bv-identical=\"true\"\n                       data-bv-identical-field=\"password\"\n                       data-bv-identical-message=\"The password and its confirm are not the same\"\n\n                       data-bv-different=\"true\"\n                       data-bv-different-field=\"username\"\n                       data-bv-different-message=\"The password cannot be the same as username\" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-lg-3 control-label\">Languages</label>\n            <div class=\"col-lg-5\">\n                <div class=\"checkbox\">\n                    <label>\n                        <input type=\"checkbox\" name=\"languages[]\" value=\"english\"\n                               data-bv-message=\"Please specify at least one language you can speak\"\n                               data-bv-notempty=\"true\" />\n                        English </label>\n                </div>\n                <div class=\"checkbox\">\n                    <label>\n                        <input type=\"checkbox\" name=\"languages[]\" value=\"french\" />\n                        French </label>\n                </div>\n                <div class=\"checkbox\">\n                    <label>\n                        <input type=\"checkbox\" name=\"languages[]\" value=\"german\" />\n                        German </label>\n                </div>\n                <div class=\"checkbox\">\n                    <label>\n                        <input type=\"checkbox\" name=\"languages[]\" value=\"russian\" />\n                        Russian </label>\n                </div>\n                <div class=\"checkbox\">\n                    <label>\n                        <input type=\"checkbox\" name=\"languages[]\" value=\"other\" />\n                        Other </label>\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <div class=\"form-actions\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <button class=\"btn btn-default\" type=\"submit\">\n                    <i class=\"fa fa-eye\"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n\n</form>\n     ");
$templateCache.put("app/_common/forms/directives/bootstrap-validation/bootstrap-button-group-form.tpl.html","<form id=\"buttonGroupForm\" method=\"post\" class=\"form-horizontal\">\n\n    <fieldset>\n        <legend>\n            Default Form Elements\n        </legend>\n        <div class=\"form-group\">\n            <label class=\"col-lg-3 control-label\">Gender</label>\n            <div class=\"col-lg-9\">\n                <div class=\"btn-group\" data-toggle=\"buttons\">\n                    <label class=\"btn btn-default\">\n                        <input type=\"radio\" name=\"gender\" value=\"male\" />\n                        Male </label>\n                    <label class=\"btn btn-default\">\n                        <input type=\"radio\" name=\"gender\" value=\"female\" />\n                        Female </label>\n                    <label class=\"btn btn-default\">\n                        <input type=\"radio\" name=\"gender\" value=\"other\" />\n                        Other </label>\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-lg-3 control-label\">Languages</label>\n            <div class=\"col-lg-9\">\n                <div class=\"btn-group\" data-toggle=\"buttons\">\n                    <label class=\"btn btn-default\">\n                        <input type=\"checkbox\" name=\"languages[]\" value=\"english\" />\n                        English </label>\n                    <label class=\"btn btn-default\">\n                        <input type=\"checkbox\" name=\"languages[]\" value=\"german\" />\n                        German </label>\n                    <label class=\"btn btn-default\">\n                        <input type=\"checkbox\" name=\"languages[]\" value=\"french\" />\n                        French </label>\n                    <label class=\"btn btn-default\">\n                        <input type=\"checkbox\" name=\"languages[]\" value=\"russian\" />\n                        Russian </label>\n                    <label class=\"btn btn-default\">\n                        <input type=\"checkbox\" name=\"languages[]\" value=\"italian\">\n                        Italian </label>\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <div class=\"form-actions\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <button class=\"btn btn-default\" type=\"submit\">\n                    <i class=\"fa fa-eye\"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n\n</form>\n");
$templateCache.put("app/_common/forms/directives/bootstrap-validation/bootstrap-contact-form.tpl.html","<form id=\"contactForm\" method=\"post\" class=\"form-horizontal\">\n\n    <fieldset>\n        <legend>Showing messages in custom area</legend>\n        <div class=\"form-group\">\n            <label class=\"col-md-3 control-label\">Full name</label>\n            <div class=\"col-md-6\">\n                <input type=\"text\" class=\"form-control\" name=\"fullName\" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-md-3 control-label\">Email</label>\n            <div class=\"col-md-6\">\n                <input type=\"text\" class=\"form-control\" name=\"email\" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-md-3 control-label\">Title</label>\n            <div class=\"col-md-6\">\n                <input type=\"text\" class=\"form-control\" name=\"title\" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-md-3 control-label\">Content</label>\n            <div class=\"col-md-6\">\n                <textarea class=\"form-control\" name=\"content\" rows=\"5\"></textarea>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <!-- #messages is where the messages are placed inside -->\n        <div class=\"form-group\">\n            <div class=\"col-md-9 col-md-offset-3\">\n                <div id=\"messages\"></div>\n            </div>\n        </div>\n    </fieldset>\n\n    <div class=\"form-actions\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <button class=\"btn btn-default\" type=\"submit\">\n                    <i class=\"fa fa-eye\"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n\n</form>\n");
$templateCache.put("app/_common/forms/directives/bootstrap-validation/bootstrap-movie-form.tpl.html","\n<form id=\"movieForm\" method=\"post\">\n\n    <fieldset>\n        <legend>\n            Default Form Elements\n        </legend>\n        <div class=\"form-group\">\n            <div class=\"row\">\n                <div class=\"col-md-8\">\n                    <label class=\"control-label\">Movie title</label>\n                    <input type=\"text\" class=\"form-control\" name=\"title\" />\n                </div>\n\n                <div class=\"col-md-4 selectContainer\">\n                    <label class=\"control-label\">Genre</label>\n                    <select class=\"form-control\" name=\"genre\">\n                        <option value=\"\">Choose a genre</option>\n                        <option value=\"action\">Action</option>\n                        <option value=\"comedy\">Comedy</option>\n                        <option value=\"horror\">Horror</option>\n                        <option value=\"romance\">Romance</option>\n                    </select>\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <div class=\"row\">\n                <div class=\"col-sm-12 col-md-4\">\n                    <label class=\"control-label\">Director</label>\n                    <input type=\"text\" class=\"form-control\" name=\"director\" />\n                </div>\n\n                <div class=\"col-sm-12 col-md-4\">\n                    <label class=\"control-label\">Writer</label>\n                    <input type=\"text\" class=\"form-control\" name=\"writer\" />\n                </div>\n\n                <div class=\"col-sm-12 col-md-4\">\n                    <label class=\"control-label\">Producer</label>\n                    <input type=\"text\" class=\"form-control\" name=\"producer\" />\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <div class=\"row\">\n                <div class=\"col-sm-12 col-md-6\">\n                    <label class=\"control-label\">Website</label>\n                    <input type=\"text\" class=\"form-control\" name=\"website\" />\n                </div>\n\n                <div class=\"col-sm-12 col-md-6\">\n                    <label class=\"control-label\">Youtube trailer</label>\n                    <input type=\"text\" class=\"form-control\" name=\"trailer\" />\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"control-label\">Review</label>\n            <textarea class=\"form-control\" name=\"review\" rows=\"8\"></textarea>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n\n            <div class=\"row\">\n                <div class=\"col-sm-12 col-md-12\">\n                    <label class=\"control-label\">Rating</label>\n                </div>\n\n                <div class=\"col-sm-12 col-md-10\">\n\n                    <label class=\"radio radio-inline no-margin\">\n                        <input type=\"radio\" name=\"rating\" value=\"terrible\" class=\"radiobox style-2\" />\n                        <span>Terrible</span> </label>\n\n                    <label class=\"radio radio-inline\">\n                        <input type=\"radio\" name=\"rating\" value=\"watchable\" class=\"radiobox style-2\" />\n                        <span>Watchable</span> </label>\n                    <label class=\"radio radio-inline\">\n                        <input type=\"radio\" name=\"rating\" value=\"best\" class=\"radiobox style-2\" />\n                        <span>Best ever</span> </label>\n\n                </div>\n\n            </div>\n\n        </div>\n    </fieldset>\n\n    <div class=\"form-actions\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <button class=\"btn btn-default\" type=\"submit\">\n                    <i class=\"fa fa-eye\"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n\n</form>\n\n ");
$templateCache.put("app/_common/forms/directives/bootstrap-validation/bootstrap-product-form.tpl.html","<form id=\"productForm\" class=\"form-horizontal\">\n\n    <fieldset>\n        <legend>\n            Default Form Elements\n        </legend>\n        <div class=\"form-group\">\n            <label class=\"col-xs-2 col-lg-3 control-label\">Price</label>\n            <div class=\"col-xs-9 col-lg-6 inputGroupContainer\">\n                <div class=\"input-group\">\n                    <input type=\"text\" class=\"form-control\" name=\"price\" />\n                    <span class=\"input-group-addon\">$</span>\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-xs-2 col-lg-3 control-label\">Amount</label>\n            <div class=\"col-xs-9 col-lg-6 inputGroupContainer\">\n                <div class=\"input-group\">\n                    <span class=\"input-group-addon\">&#8364;</span>\n                    <input type=\"text\" class=\"form-control\" name=\"amount\" />\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-xs-2 col-lg-3 control-label\">Color</label>\n            <div class=\"col-xs-9 col-lg-6 selectContainer\">\n                <select class=\"form-control\" name=\"color\">\n                    <option value=\"\">Choose a color</option>\n                    <option value=\"blue\">Blue</option>\n                    <option value=\"green\">Green</option>\n                    <option value=\"red\">Red</option>\n                    <option value=\"yellow\">Yellow</option>\n                    <option value=\"white\">White</option>\n                </select>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-xs-2 col-lg-3 control-label\">Size</label>\n            <div class=\"col-xs-9 col-lg-6 selectContainer\">\n                <select class=\"form-control\" name=\"size\">\n                    <option value=\"\">Choose a size</option>\n                    <option value=\"S\">S</option>\n                    <option value=\"M\">M</option>\n                    <option value=\"L\">L</option>\n                    <option value=\"XL\">XL</option>\n                </select>\n            </div>\n        </div>\n    </fieldset>\n\n    <div class=\"form-actions\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <button class=\"btn btn-default\" type=\"submit\">\n                    <i class=\"fa fa-eye\"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n</form>\n\n");
$templateCache.put("app/_common/forms/directives/bootstrap-validation/bootstrap-profile-form.tpl.html","<form id=\"profileForm\">\n\n    <fieldset>\n        <legend>\n            Default Form Elements\n        </legend>\n        <div class=\"form-group\">\n            <label>Email address</label>\n            <input type=\"text\" class=\"form-control\" name=\"email\" />\n        </div>\n    </fieldset>\n    <fieldset>\n        <div class=\"form-group\">\n            <label>Password</label>\n            <input type=\"password\" class=\"form-control\" name=\"password\" />\n        </div>\n    </fieldset>\n\n    <div class=\"form-actions\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <button class=\"btn btn-default\" type=\"submit\">\n                    <i class=\"fa fa-eye\"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n</form>\n");
$templateCache.put("app/_common/forms/directives/bootstrap-validation/bootstrap-toggling-form.tpl.html","<form id=\"togglingForm\" method=\"post\" class=\"form-horizontal\">\n\n    <fieldset>\n        <legend>\n            Default Form Elements\n        </legend>\n        <div class=\"form-group\">\n            <label class=\"col-lg-3 control-label\">Full name <sup>*</sup></label>\n            <div class=\"col-lg-4\">\n                <input type=\"text\" class=\"form-control\" name=\"firstName\" placeholder=\"First name\" />\n            </div>\n            <div class=\"col-lg-4\">\n                <input type=\"text\" class=\"form-control\" name=\"lastName\" placeholder=\"Last name\" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-lg-3 control-label\">Company <sup>*</sup></label>\n            <div class=\"col-lg-5\">\n                <input type=\"text\" class=\"form-control\" name=\"company\"\n                       required data-bv-notempty-message=\"The company name is required\" />\n            </div>\n            <div class=\"col-lg-2\">\n                <button type=\"button\" class=\"btn btn-info btn-sm\" data-toggle=\"#jobInfo\">\n                    Add more info\n                </button>\n            </div>\n        </div>\n    </fieldset>\n\n    <!-- These fields will not be validated as long as they are not visible -->\n    <div id=\"jobInfo\" style=\"display: none;\">\n        <fieldset>\n            <div class=\"form-group\">\n                <label class=\"col-lg-3 control-label\">Job title <sup>*</sup></label>\n                <div class=\"col-lg-5\">\n                    <input type=\"text\" class=\"form-control\" name=\"job\" />\n                </div>\n            </div>\n        </fieldset>\n\n        <fieldset>\n            <div class=\"form-group\">\n                <label class=\"col-lg-3 control-label\">Department <sup>*</sup></label>\n                <div class=\"col-lg-5\">\n                    <input type=\"text\" class=\"form-control\" name=\"department\" />\n                </div>\n            </div>\n        </fieldset>\n    </div>\n\n    <fieldset>\n        <div class=\"form-group\">\n            <label class=\"col-lg-3 control-label\">Mobile phone <sup>*</sup></label>\n            <div class=\"col-lg-5\">\n                <input type=\"text\" class=\"form-control\" name=\"mobilePhone\" />\n            </div>\n            <div class=\"col-lg-2\">\n                <button type=\"button\" class=\"btn btn-info btn-sm\" data-toggle=\"#phoneInfo\">\n                    Add more phone numbers\n                </button>\n            </div>\n        </div>\n    </fieldset>\n    <!-- These fields will not be validated as long as they are not visible -->\n    <div id=\"phoneInfo\" style=\"display: none;\">\n\n        <fieldset>\n            <div class=\"form-group\">\n                <label class=\"col-lg-3 control-label\">Home phone</label>\n                <div class=\"col-lg-5\">\n                    <input type=\"text\" class=\"form-control\" name=\"homePhone\" />\n                </div>\n            </div>\n        </fieldset>\n        <fieldset>\n            <div class=\"form-group\">\n                <label class=\"col-lg-3 control-label\">Office phone</label>\n                <div class=\"col-lg-5\">\n                    <input type=\"text\" class=\"form-control\" name=\"officePhone\" />\n                </div>\n            </div>\n        </fieldset>\n    </div>\n\n    <div class=\"form-actions\">\n        <div class=\"row\">\n            <div class=\"col-md-12\">\n                <button class=\"btn btn-default\" type=\"submit\">\n                    <i class=\"fa fa-eye\"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n</form>");
$templateCache.put("app/_common/layout/directives/demo/demo-states.tpl.html","<div class=\"demo\"><span id=\"demo-setting\"><i class=\"fa fa-cog txt-color-blueDark\"></i></span>\n\n    <form>\n        <legend class=\"no-padding margin-bottom-10\">Layout Options</legend>\n        <section>\n            <label><input type=\"checkbox\" ng-model=\"fixedHeader\"\n                          class=\"checkbox style-0\"><span>Fixed Header</span></label>\n            <label><input type=\"checkbox\"\n                          ng-model=\"fixedNavigation\"\n                          class=\"checkbox style-0\"><span>Fixed Navigation</span></label>\n            <label><input type=\"checkbox\"\n                          ng-model=\"fixedRibbon\"\n                          class=\"checkbox style-0\"><span>Fixed Ribbon</span></label>\n            <label><input type=\"checkbox\"\n                          ng-model=\"fixedPageFooter\"\n                          class=\"checkbox style-0\"><span>Fixed Footer</span></label>\n            <label><input type=\"checkbox\"\n                          ng-model=\"insideContainer\"\n                          class=\"checkbox style-0\"><span>Inside <b>.container</b></span></label>\n            <label><input type=\"checkbox\"\n                          ng-model=\"rtl\"\n                          class=\"checkbox style-0\"><span>RTL</span></label>\n            <label><input type=\"checkbox\"\n                          ng-model=\"menuOnTop\"\n                          class=\"checkbox style-0\"><span>Menu on <b>top</b></span></label>\n            <label><input type=\"checkbox\"\n                          ng-model=\"colorblindFriendly\"\n                          class=\"checkbox style-0\"><span>For Colorblind <div\n                    class=\"font-xs text-right\">(experimental)\n            </div></span>\n            </label><span id=\"smart-bgimages\"></span></section>\n        <section><h6 class=\"margin-top-10 semi-bold margin-bottom-5\">Clear Localstorage</h6><a\n                ng-click=\"factoryReset()\" class=\"btn btn-xs btn-block btn-primary\" id=\"reset-smart-widget\"><i\n                class=\"fa fa-refresh\"></i> Factory Reset</a></section>\n\n        <h6 class=\"margin-top-10 semi-bold margin-bottom-5\">SmartAdmin Skins</h6>\n\n\n        <section id=\"smart-styles\">\n            <a ng-repeat=\"skin in skins\" ng-click=\"setSkin(skin)\" class=\"{{skin.class}}\" style=\"{{skin.style}}\"><i ng-if=\"skin.name == $parent.smartSkin\" class=\"fa fa-check fa-fw\"></i> {{skin.label}} <sup ng-if=\"skin.beta\">beta</sup></a>\n        </section>\n    </form>\n</div>");}]);
"use strict";

angular.module('app').directive('userLogout', ['sessionService', function (sessionService) {
    return {
        restrict: "EA",
        replace: false,
        link: function (scope, element) {
            element.bind('click', function () {
                sessionService.logout();
            });
        }
    }
}]);
"use strict";	

angular.module('app').controller("ActivitiesCtrl", function ActivitiesCtrl($scope, $log, activityService){

	$scope.activeTab = 'default';
	$scope.currentActivityItems = [];
	
	// Getting different type of activites
	activityService.get(function(data){

		$scope.activities = data.activities;
		
	});


	$scope.isActive = function(tab){
		return $scope.activeTab === tab;
	};

	$scope.setTab = function(activityType){
		$scope.activeTab = activityType;

		activityService.getbytype(activityType, function(data) {

			$scope.currentActivityItems = data.data;

		});

	};

});
"use strict";

angular.module('app').directive('activitiesDropdownToggle', function($log) {

	var link = function($scope,$element, attrs){
		var ajax_dropdown = null;

		$element.on('click',function(){
			var badge = $(this).find('.badge');

			if (badge.hasClass('bg-color-red')) {

				badge.removeClass('bg-color-red').text(0);

			}

			ajax_dropdown = $(this).next('.ajax-dropdown');

			if (!ajax_dropdown.is(':visible')) {

				ajax_dropdown.fadeIn(150);

				$(this).addClass('active');

			}
			 else {
				
				ajax_dropdown.fadeOut(150);
				
				$(this).removeClass('active');

			}

		})

		$(document).mouseup(function(e) {
			if (ajax_dropdown && !ajax_dropdown.is(e.target) && ajax_dropdown.has(e.target).length === 0) {
				ajax_dropdown.fadeOut(150);
				$element.removeClass('active');
			}
		});
	}
	
	return{
		restrict:'EA',
		link:link
	}
});
"use strict";

angular.module('app').factory('activityService', function($http, $log, APP_CONFIG) {

	function getActivities(callback){

		$http.get(APP_CONFIG.apiRootUrl + '/activities/activity.json').success(function(data){

			callback(data);
				
		}).error(function(){

			$log.log('Error');
			callback([]);

		});

	}

	function getActivitiesByType(type, callback){

		$http.get(APP_CONFIG.apiRootUrl + '/activities/activity-' + type + '.json').success(function(data){

			callback(data);
				
		}).error(function(){

			$log.log('Error');
			callback([]);

		});

	}
	
	return{
		get:function(callback){
			getActivities(callback);
		},
		getbytype:function(type,callback){
			getActivitiesByType(type, callback);
		}
	}
});
"use strict";

angular.module('app').factory('Project', function($http, APP_CONFIG){
    return {
        list: $http.get(APP_CONFIG.apiRootUrl + '/projects.json')
    }
});
"use strict";

angular.module('app').directive('recentProjects', function(Project){
    return {
        restrict: "EA",
        replace: true,
        templateUrl: "app/dashboard/projects/recent-projects.tpl.html",
        scope: true,
        link: function(scope, element){

            Project.list.then(function(response){
                scope.projects = response.data;
            });
            scope.clearProjects = function(){
                scope.projects = [];
            }
        }
    }
});
"use strict";

angular.module('app').controller('TodoCtrl', function ($scope, $timeout, Todo) {
    $scope.newTodo = undefined;

    $scope.states = ['Critical', 'Important', 'Completed'];

    $scope.todos = Todo.getList().$object;

    // $scope.$watch('todos', function(){ }, true)

    $scope.toggleAdd = function () {
        if (!$scope.newTodo) {
            $scope.newTodo = {
                state: 'Important'
            };
        } else {
            $scope.newTodo = undefined;
        }
    };

    $scope.createTodo = function () {
        $scope.todos.push(
           Todo.normalize($scope.newTodo)
        );
        $scope.newTodo = undefined;

    };

    $scope.deleteTodo = function (todo) {
        todo.remove().then(function () {
            $scope.todos.splice($scope.todos.indexOf(todo), 1);
        });

    };

});
"use strict";

angular.module('app').factory('Language', function($http, APP_CONFIG){

	function getLanguage(key, callback) {

		$http.get(APP_CONFIG.apiRootUrl + '/langs/' + key + '.json').success(function(data){

			callback(data);
			
		}).error(function(){

			$log.log('Error');
			callback([]);

		});

	}

	function getLanguages(callback) {

		$http.get(APP_CONFIG.apiRootUrl + '/languages.json').success(function(data){

			callback(data);
			
		}).error(function(){

			$log.log('Error');
			callback([]);

		});

	}

	return {
		getLang: function(type, callback) {
			getLanguage(type, callback);
		},
		getLanguages:function(callback){
			getLanguages(callback);
		}
	}

});
"use strict";

angular.module('app').controller("LanguagesCtrl",  function LanguagesCtrl($scope, $rootScope, $log, Language){

    $rootScope.lang = {};
    
    Language.getLanguages(function(data){

        $rootScope.currentLanguage = data[0];

        $rootScope.languages = data;

        Language.getLang(data[0].key,function(data){

            $rootScope.lang = data;
        });

    });

    $scope.selectLanguage = function(language){
        $rootScope.currentLanguage = language;
        
        Language.getLang(language.key,function(data){

            $rootScope.lang = data;
            
        });
    }

    $rootScope.getWord = function(key){
        if(angular.isDefined($rootScope.lang[key])){
            return $rootScope.lang[key];
        } 
        else {
            return key;
        }
    }

});
"use strict";

angular.module('app').directive('languageSelector', function(Language){
    return {
        restrict: "EA",
        replace: true,
        templateUrl: "app/layout/language/language-selector.tpl.html",
        scope: true
    }
});
"use strict";

angular.module('app').directive('toggleShortcut', function($log,$timeout) {

	var initDomEvents = function($element){

		var shortcut_dropdown = $('#shortcut');

		$element.on('click',function(){
		
			if (shortcut_dropdown.is(":visible")) {
				shortcut_buttons_hide();
			} else {
				shortcut_buttons_show();
			}

		})

		shortcut_dropdown.find('a').click(function(e) {
			e.preventDefault();
			window.location = $(this).attr('href');
			setTimeout(shortcut_buttons_hide, 300);
		});

		

		// SHORTCUT buttons goes away if mouse is clicked outside of the area
		$(document).mouseup(function(e) {
			if (shortcut_dropdown && !shortcut_dropdown.is(e.target) && shortcut_dropdown.has(e.target).length === 0) {
				shortcut_buttons_hide();
			}
		});

		// SHORTCUT ANIMATE HIDE
		function shortcut_buttons_hide() {
			shortcut_dropdown.animate({
				height : "hide"
			}, 300, "easeOutCirc");
			$('body').removeClass('shortcut-on');

		}

		// SHORTCUT ANIMATE SHOW
		function shortcut_buttons_show() {
			shortcut_dropdown.animate({
				height : "show"
			}, 200, "easeOutCirc");
			$('body').addClass('shortcut-on');
		}
	}

	var link = function($scope,$element){
		$timeout(function(){
			initDomEvents($element);
		});
	}

	return{
		restrict:'EA',
		link:link
	}
})
'use strict';

angular.module('app.monitor')
    .controller('MonitoreoCtrl', ['$scope', '$filter', '$compile', 'uiGmapGoogleMapApi', 'restService',
        function ($scope, $filter, $compile, uiGmapGoogleMapApi, restService) {
            $scope.markersCircuits = []; // Map Markers
            var centerCoords = null; // Device to center the map
            $scope.graphOperacionCircuitos = [
                {
                    name: "Proyectados",
                    data: [0],
                    color: "#9F9F9F"
                },
                {
                    name: "Desconectado",
                    data: [0],
                    color: "#E90808"
                },
                {
                    name: "Apagado",
                    data: [0],
                    color: "#49ADE8"
                },
                {
                    name: "Encendido",
                    data: [0],
                    color: "#83B925"
                }
            ]; // Graph
            $scope.graphAlarmasSistema = [
                {
                    name: "Con alerta",
                    data: [0],
                    color: "#EA0E24"
                },
                {
                    name: "Con aviso",
                    data: [0],
                    color: "#F9EA24"
                },
                {
                    name: "Sin alerta",
                    data: [0],
                    color: "#BFBFBF"
                }
            ]; // Graph

            /*
             defaultColors = {
             "OK": "#F99C25",
             "Disconnected": "#25B1C4",
             "Warning": "#F9EA24",
             "Alert": "#EA0E24",
             "StanBy": "#8E8E8E"
             };
             */
            function byLabelName(el) {
                if (el.hasOwnProperty('name'))
                    return el.name.toLowerCase() == this;
                if (el.hasOwnProperty('label'))
                    return el.label.toLowerCase() == this;
            }
            
            restService.sendRequest(null, 'deviceGET').then(function (data) {
                data = data.data;
                for (var i = 0; i < data.length; i++) {
                    if (!centerCoords && data[i].installationStatus.toLowerCase() != 'proyectado')
                        centerCoords = data[i];

                    var icon;

                    switch (data[i].installationStatus.toLowerCase()) {
                        case 'encendido':
                            icon = 'styles/img/mexiled/mapMarkerEncendido.png';
                            break;
                        case 'apagado':
                            icon = 'styles/img/mexiled/mapMarkerApagado.png';
                            break;
                        case 'proyectado':
                            icon = 'styles/img/mexiled/mapMarkerProyectado.png';
                            break;
                        default:
                            icon = 'styles/img/mexiled/mapMarkerVarios.png';
                            break;
                    }

                    /* Graficas */
                    // Operacion de circuitos
                    var index = -1;
                    index = $scope.graphOperacionCircuitos.findIndex(byLabelName, data[i].installationStatus.toLowerCase());
                    if (index >= 0) {
                        if ($scope.graphOperacionCircuitos[index].hasOwnProperty('value'))
                            $scope.graphOperacionCircuitos[index].value++;
                        if ($scope.graphOperacionCircuitos[index].hasOwnProperty('data'))
                            $scope.graphOperacionCircuitos[index].data[0]++;
                    }
                    // Alarmas de sistema
                    index = -1;
                    if (!!data[i].alert)
                        index = $scope.graphAlarmasSistema.findIndex(byLabelName, 'con alerta');
                    else if (!!data[i].warning)
                        index = $scope.graphAlarmasSistema.findIndex(byLabelName, 'con aviso');
                    else
                        index = $scope.graphAlarmasSistema.findIndex(byLabelName, 'sin alerta');
                    if (index >= 0) {
                        if ($scope.graphAlarmasSistema[index].hasOwnProperty('value'))
                            $scope.graphAlarmasSistema[index].value++;
                        if ($scope.graphAlarmasSistema[index].hasOwnProperty('data'))
                            $scope.graphAlarmasSistema[index].data[0]++;
                    }
                    /* ~ */

                    $scope.markersCircuits.push({
                        id: i + 1,
                        coords: {
                            latitude: data[i].latitude,
                            longitude: data[i].longitude
                        },
                        options: {
                            icon: {
                                url: icon,
                                scaledSize: {
                                    height: 9,
                                    width: 9
                                }
                            }
                        },
                        iwData: '<div class="info-box-wrap">' +
                            '<h3 class="title"><a>' + data[i].code + '</a></h3>' +
                            '<span><b>Número de lamparas:</b> ' + data[i].lamps + '</span><br>' +
                            '<span><b>Ultimo registro:</b> ' + $filter('date')(data[i].lastActivity, "dd-MM-yyyy HH:mm:ss") + '</span><br>' +
                            '<span><b>Estatus:</b> ' + data[i].installationStatus + '</span>' +
                            '</div>'
                    });
                }

                var graphs = angular.element(document).find('div[stackbar-chart]');
                for (var i = 0; i < graphs.length; i++) {
                    $compile(graphs[i])($scope);
                }
            });
            /* Mapa Data */
            if (!!centerCoords)
                centerCoords = {latitude: centerCoords.latitude, longitude: centerCoords.longitude};
            else
                centerCoords = {latitude: 20.6322137, longitude: -103.2776288};

            $scope.map = {
                center: centerCoords,
                zoom: 12,
                control: {},
                options: {
                    scrollwheel: false
                },
                markersEvents: {
                    click: function (marker, eventName, model) {
                        $scope.map.infoWindow.model = model;
                        $scope.map.infoWindow.show = true;
                    }
                },
                infoWindow: {
                    model: null,
                    show: false,
                    closeClock: function () {
                        this.show = false;
                    }
                }
            };

            uiGmapGoogleMapApi.then(function (maps) {});
        }
    ]);
'use strict';

angular.module('app.monitor').directive('stackbarChart', function () {
    return {
        restrict: 'A',
        scope: {
            data: '='
        },
        link: function (scope, element) {
            var defaultColors = [
                "#F99C25", // Encendido
                "#25B1C4", // Apagado
                "#EA0E24", // Desconectado
                "#8E8E8E"  // Proyectados
            ];

            for (var i = 0; i < scope.data.length; i++) {
                if (!scope.data[i].color)
                    scope.data[i].color = defaultColors[i % defaultColors.length];
            }

            Highcharts.chart(element[0], {
                chart: {
                    type: 'bar'
                },
                credits: {
                    enabled: false
                },
                legend: {
                    reversed: true,
                    enabled: false
                },
                title: {
                    text: ''
                },
                xAxis: {
                    visible: false,
                },
                yAxis: {
                    visible: false
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<span style="color:' + this.point.color + '">\u25CF</span> ' + this.series.name + ': <b>' + this.percentage + '%</b> (' + this.point.y + ')<br/>'
                    }
                },
                series: scope.data
            });
        }
    }
});
'use strict';

angular.module('app.operativo')
    .controller('OperativoCtrl', ['$scope', '$compile', '$filter', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', 'restService',
        function ($scope, $compile, $filter, $timeout, DTOptionsBuilder, DTColumnBuilder, restService) {
            var promiseListDevices = restService.sendRequest(null, 'deviceGET');
            var resolvedListDevices = [];
            var indexSelectedDevice = -1;
            var timeoutToRefreshCurrentMeasure;

            $scope.graphData = [];
            $scope.currentRecord = {
                date: null,
                fase1: null,
                fase2: null
            };

                promiseListDevices.then(function (data) {
                resolvedListDevices = data.data;
                if (!!resolvedListDevices.length) {
                    indexSelectedDevice = 0;
                    $scope.filter();
                }
            });
            /*
             Grafica
             */
            /*function getRandomData(period) {
                var currentDate = Date.now();
                var tickRate; // Minutes
                var timeToDisplay = new Date();
                switch (period) {
                    case 'D':
                    default:
                        $scope.graphFilter = period;
                        timeToDisplay.setDate(timeToDisplay.getDate() - 1);
                        tickRate = 5;
                        break;
                    case 'S':
                        $scope.graphFilter = period;
                        timeToDisplay.setDate(timeToDisplay.getDate() - 7);
                        tickRate = 60;
                        break;
                    case 'M':
                        $scope.graphFilter = period;
                        timeToDisplay.setDate(timeToDisplay.getDate() - 28);
                        tickRate = 60;
                        break;
                }
                timeToDisplay.setHours(0, 2, 0, 0);

                // zip the generated y values with the x values
                var res = [];
                var ampere;
                while (timeToDisplay.getTime() < currentDate) {
                    ampere = (timeToDisplay.getHours() < 8 || timeToDisplay.getHours() >= 20) ? 15 : 0;
                    // X & Y axis data
                    res.push([timeToDisplay.getTime(), Math.round((ampere + Math.random() * 0.05) * 100) / 100]);
                    timeToDisplay.setMinutes(timeToDisplay.getMinutes() + tickRate);
                }
                return res;
            }*/

            function getRecordsData(Records) {
                var recordsForGraph = [[], []];

                for (var i = 0; i < Records.length; i++) {
                    recordsForGraph[0].push([Records[i].createdDate, Records[i].fase1.potencia]);
                    recordsForGraph[1].push([Records[i].createdDate, Records[i].fase2.potencia]);
                }

                return recordsForGraph;
            }

            function getCurrentMeasure(lastRecord) {
                var currentMeasure = {
                    date: null,
                    fase1: null,
                    fase2: null
                };

                var timeToNotifyFromNow = new Date().getTime() - resolvedListDevices[indexSelectedDevice].timeBeforeNotify * 60000;
                if (lastRecord.createdDate >= timeToNotifyFromNow) {
                    currentMeasure.date = lastRecord.createdDate;
                    currentMeasure.fase1 = lastRecord.fase1;
                    currentMeasure.fase2 = lastRecord.fase2;
                }

                /* set timer to get a new measure every timeBeforeNotify on device (parameter was in minutes) */
                if (!!timeoutToRefreshCurrentMeasure)
                    $timeout.cancel(timeoutToRefreshCurrentMeasure);
                timeoutToRefreshCurrentMeasure = $timeout(function () {
                    restService.sendRequest(null, 'deviceRecordsGET', '?id=' + resolvedListDevices[indexSelectedDevice].deviceId).then(function (data) {
                        $scope.currentRecord = getCurrentMeasure(data.data.lastRecords);
                    });
                }, resolvedListDevices[indexSelectedDevice].timeBeforeNotify * 60000);
                /* ~ */

                return currentMeasure;
            }

            $scope.filter = function (timePeriod) {
                timePeriod = timePeriod || 'D';
                if (timePeriod != $scope.graphFilter) {
                    $scope.graphFilter = timePeriod;
                    if (indexSelectedDevice >= 0) {
                        restService.sendRequest(null, 'deviceRecordsGET', '?id=' + resolvedListDevices[indexSelectedDevice].deviceId + '&type=' + $scope.graphFilter).then(function (data) {
                            $scope.graphData = getRecordsData(data.data.records);
                            $scope.currentRecord = getCurrentMeasure(data.data.lastRecords);
                        });
                    }
                }
            };

            $scope.loadData = function (deviceId) {
                function indexInListById(device) {
                    return device.deviceId = deviceId;
                }

                if (deviceId != resolvedListDevices[indexSelectedDevice].deviceId) {
                    indexSelectedDevice = resolvedListDevices.findIndex(indexInListById);
                    $scope.filter();
                }
            };

            /*
             Tabla
             */
            $scope.dtOptions = DTOptionsBuilder
                .fromFnPromise(promiseListDevices)
                .withDataProp('data')
                //Add Bootstrap compatibility
                .withOption('responsive', true)
                .withOption('order', [[2, "desc"]])
                .withOption('createdRow', createdRow)
                .withDOM("<'dt-toolbar'<' col-sm-6 col-xs-12 hidden-xs'l><'col-sm-6 col-xs-12'f>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap();
            $scope.dtColumns = [
                DTColumnBuilder.newColumn('code').renderWith(function (data, type, full) {
                    return '<button class="btn btn-xs btn-warning" ng-click="loadData(\'' + full.deviceId + '\')"><i class="fa fa-line-chart"></i></button> ' + data;
                }),
                DTColumnBuilder.newColumn('installationStatus').renderWith(function (data) {
                    var classToAdd = 'label-default';
                    if (data.toLowerCase() == 'sin conexión' || data.toLowerCase() == 'desconectado')
                        classToAdd = 'label-danger'
                    if (data.toLowerCase() == 'apagado')
                        classToAdd = 'label-info';
                    if (data.toLowerCase() == 'en linea' && data.toLowerCase() == 'encendido')
                        classToAdd = 'label-success';

                    return '<span class="label ' + classToAdd + '">' + data + '</span>';
                }),
                DTColumnBuilder.newColumn('lastActivity').renderWith(function (data) {
                    return $filter('date')(data, "dd-MM-yyyy HH:mm:ss");
                }),
                DTColumnBuilder.newColumn('polygon.section.name'),
                DTColumnBuilder.newColumn('installationStatus').renderWith(function (data) {
                    var classToAdd = 'label-default';
                    if (data.toLowerCase() == 'sin conexión' || data.toLowerCase() == 'desconectado')
                        classToAdd = 'label-danger'
                    if (data.toLowerCase() == 'apagado')
                        classToAdd = 'label-info';
                    if (data.toLowerCase() == 'en linea' && data.toLowerCase() == 'encendido')
                        classToAdd = 'label-success';

                    return '<span class="label ' + classToAdd + '">' + data + '</span>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }
        }
    ]);
"use strict";
angular.module('app.operativo').value('FlotConfig', {
    "chartBorderColor": "#efefef",
    "chartGridColor": "#DDD",
    //"chartOperativo": "#7e9d3a",
    "chartOperativo": "#f99c25",
    "chartMono": "#000"

});

angular.module('app.operativo').directive('operativoGraph', ['$timeout', 'FlotConfig', function($timeout, FlotConfig) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="chart"></div>',
        scope: {
            data: '='
        },
        link: function (scope, element) {
// For the demo we use generated data, but normally it would be coming from the server
            var dataOptions = {
                lines: {
                    lineWidth: 1,
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 0.2
                        }, {
                            opacity: 0.4
                        }]
                    }
                },
                splines: {
                    show: false,
                    tension: 0.6,
                    lineWidth: 1,
                    fill: 0.1
                }
            };
            // setup plot
            var options = {
                xaxis: {
                    mode: "time",
                    tickLength: 20,
                    timezone: "browser"
                },
                yaxis: {
                    min: 0
                },
                colors: [FlotConfig.chartOperativo],
                grid: {
                    color: "#999999",
                    clickable: true,
                    tickColor: "#D4D4D4",
                    borderWidth: 0,
                    hoverable: true //IMPORTANT! this is needed for tooltip to work,
                },
                legend: {
                    position: 'sw',
                    margin: 5,
                    backgroundColor: "#FFFFFF"
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s <br> %x <br> Potencia: %y",
                    dateFormat: "%y-%m-%d %H:%M:%S"
                },
                series: {
                    points: {
                        show: true,
                        lineWidth: 1,
                    }
                }
            };
            var plot = $.plot(
                element,
                [
                    $.extend({}, dataOptions, {data: scope.data[0], label: "Potencia Fase 1"}),
                    $.extend({}, dataOptions, {data: scope.data[1], label: "Potencia Fase 2"})
                ],
                options
            );

            scope.$watch('data', function (newValue) {
                plot.setData([
                    $.extend({}, dataOptions, {data: newValue[0], label: "Potencia Fase 1"}),
                    $.extend({}, dataOptions, {data: newValue[1], label: "Potencia Fase 2"})
                ]);
                plot.setupGrid();
                plot.draw();
            });
        }
    }
}]);
angular.module('app.proyectos')
    .controller('ProyectoAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.project = {
                projectId: null,
                name: '',
                code: '',
                active: ''
            };

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('project.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    $scope.project.active = 'true';
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('project.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('project.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'projectGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;

                        $scope.project.projectId = data.projectId;
                        $scope.project.name = data.name;
                        $scope.project.code = data.code;
                        $scope.project.active = '' + data.active;

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            $scope.cancel = function () {
                $state.go('app.proyecto');
                return false;
            };

            // Guardar informacion, crea o edita un usuario con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                } else if (!!$scope.project.name && !!$scope.project.code) {
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('project.add')) {
                                $scope.project.active = ($scope.project.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.project, 'projectPOST').then(function () {
                                    $.bigBox({
                                        title: "Nuevo proyecto agregado",
                                        content: "Se guardo la información del proyecto: <b>" + $scope.project.name + "</b> - <i>" + $scope.project.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del proyecto';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('project.edit')) {
                                $scope.project.active = ($scope.project.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.project, 'projectPUT', '/' + $scope.project.projectId).then(function () {
                                    $.bigBox({
                                        title: "Proyecto actualizado",
                                        content: "Se actualizo la información del proyecto: <b>" + $scope.project.name + "</b> - <i>" + $scope.project.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del proyecto';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);

angular.module('app.proyectos')
    .controller('ProyectoCtrl', ['$scope', '$compile', '$state', 'DTOptionsBuilder', 'DTColumnBuilder', 'permissionsService', 'restService',
        function ($scope, $compile, $state, DTOptionsBuilder, DTColumnBuilder, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.statusFilter = {
                'true': 'Activo',
                'false': 'Inactivo'
            };

            /* Datatable (DT), creacion y asignacion de eventos */
            var dataArrayResolved = [];
            $scope.standardOptions = DTOptionsBuilder
                .fromFnPromise(restService.sendRequest(null, 'projectGET'))
                .withDataProp('data')
                .withDOM("t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap()
                .withOption('createdRow', createdRow);
            $scope.standardColumns = [
                DTColumnBuilder.newColumn('name').notSortable().renderWith(function (row, type, data) {
                    return '<a ui-sref="app.proyecto.seccion({proyecto: ' + data.projectId + '})">' + row + '</a>';
                }),
                DTColumnBuilder.newColumn('code').notSortable(),
                DTColumnBuilder.newColumn('active').notSortable().renderWith(function (data) {
                    return $scope.statusFilter[data];
                }),
                DTColumnBuilder.newColumn(null).notSortable().withClass('table-actions-buttons').renderWith(function (data) {
                    dataArrayResolved[data.projectId] = data;
                    return '<a has-permission="PROJECT_VIEW" title="Ver" ui-sref="app.proyecto.accion({accion: \'ver\', id: ' + data.projectId + '})" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>' +
                        '<a has-permission="PROJECT_EDIT" title="Editar" ui-sref="app.proyecto.accion({accion: \'editar\', id: ' + data.projectId + '})" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>' +
                        '<a has-permission="PROJECT_EDIT" title="' + (data.active ? 'Desactivar' : 'Activar') + '" ng-click="accion(\'' + (data.active ? 'deactivate' : 'activate') + '\', ' + data.projectId + ')" class="btn btn-default btn-xs"><i class="fa ' + (data.active ? 'fa-ban' : 'fa-check-circle-o') + '"></i></a>' +
                        '<a has-permission="PROJECT_DELETE" title="Eliminar" ng-click="accion(\'delete\', ' + data.projectId + ')" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }

            // Callback para funcionalidad de filtros
            $scope.instanceCallback = function (dtInstance) {
                $scope.dtInstance = dtInstance;
                var table = dtInstance.DataTable;
                table.columns().eq(0).each(function (colIdx) {
                    $('input, select', table.column(colIdx).header()).on('keyup change', function () {
                        if (this.getAttribute('id') == 'table-lenght-ctrl') {
                            table
                                .page.len(this.value)
                                .draw();
                        }
                        else {
                            var isSelect = (this.nodeName == 'SELECT');
                            var searchValue = (isSelect && this.value !== '') ? '\\b' + this.value + '\\b' : this.value;
                            table
                                .column(colIdx)
                                .search(searchValue, isSelect, !isSelect)
                                .draw();
                        }
                    });
                });
            };

            $scope.accion = function (accion, id) {
                switch (accion) {
                    case 'delete':
                    case 'eliminar':
                        if (permissionsService.canAccess('project.delete')) {
                            $.SmartMessageBox({
                                title: "Borrar Proyecto",
                                content: "Esta a punto de borrar la información del proyecto <b>" + dataArrayResolved[id].name + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'projectDELETE', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se borro la información del proyecto <b>" + dataArrayResolved[id].name + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'projectGET'));
                                        else
                                            $state.reload();
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo borrar el proyecto solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                    case 'activate':
                    case 'deactivate':
                        if (permissionsService.canAccess('project.edit')) {
                            var active = (accion.toLowerCase() === 'activate');
                            $.SmartMessageBox({
                                title: (active ? 'Activar' : 'Desactivar') + " Proyecto",
                                content: "Esta a punto de " + (active ? 'activar' : 'desactivar') + " el proyecto <b>" + dataArrayResolved[id].name + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    dataArrayResolved[id].active = active;
                                    restService.sendRequest(dataArrayResolved[id], 'projectPUT', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se " + (active ? 'activo' : 'desactivo') + " el proyecto <b>" + dataArrayResolved[id].name + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'projectGET'));
                                        else
                                            $state.reload();
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo " + (active ? 'activar' : 'desactivar') + " el proyecto solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                }
            };
        }
    ]);
"use strict";

angular.module('SmartAdmin.Layout').directive('fullScreen', function(){
    return {
        restrict: 'A',
        link: function(scope, element){
            var $body = $('body');
            var toggleFullSceen = function(e){
                if (!$body.hasClass("full-screen")) {
                    $body.addClass("full-screen");
                    if (document.documentElement.requestFullscreen) {
                        document.documentElement.requestFullscreen();
                    } else if (document.documentElement.mozRequestFullScreen) {
                        document.documentElement.mozRequestFullScreen();
                    } else if (document.documentElement.webkitRequestFullscreen) {
                        document.documentElement.webkitRequestFullscreen();
                    } else if (document.documentElement.msRequestFullscreen) {
                        document.documentElement.msRequestFullscreen();
                    }
                } else {
                    $body.removeClass("full-screen");
                    if (document.exitFullscreen) {
                        document.exitFullscreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitExitFullscreen) {
                        document.webkitExitFullscreen();
                    }
                }
            };

            element.on('click', toggleFullSceen);

        }
    }
});
"use strict";

angular.module('SmartAdmin.Layout').directive('minifyMenu', function(){
    return {
        restrict: 'A',
        link: function(scope, element){
                var $body = $('body');
            var minifyMenu = function() {
                if (!$body.hasClass("menu-on-top")) {
                    $body.toggleClass("minified");
                    $body.removeClass("hidden-menu");
                    $('html').removeClass("hidden-menu-mobile-lock");
                }
            };

            element.on('click', minifyMenu);
        }
    }
})
'use strict';

angular.module('SmartAdmin.Layout').directive('reloadState', function ($rootScope) {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('reload-state data-reload-state');
            tElement.on('click', function (e) {
                $rootScope.$state.transitionTo($rootScope.$state.current, $rootScope.$stateParams, {
                    reload: true,
                    inherit: false,
                    notify: true
                });
                e.preventDefault();
            })
        }
    }
});

"use strict";

angular.module('SmartAdmin.Layout').directive('resetWidgets', function($state){

    return {
        restrict: 'A',
        link: function(scope, element){
            element.on('click', function(){
                $.SmartMessageBox({
                    title : "<i class='fa fa-refresh' style='color:green'></i> Clear Local Storage",
                    content : "Would you like to RESET all your saved widgets and clear LocalStorage?1",
                    buttons : '[No][Yes]'
                }, function(ButtonPressed) {
                    if (ButtonPressed == "Yes" && localStorage) {
                        localStorage.clear();
                        location.reload()
                    }
                });

            });
        }
    }

});

'use strict';

angular.module('SmartAdmin.Layout').directive('searchMobile', function () {
    return {
        restrict: 'A',
        compile: function (element, attributes) {
            element.removeAttr('search-mobile data-search-mobile');

            element.on('click', function (e) {
                $('body').addClass('search-mobile');
                e.preventDefault();
            });

            $('#cancel-search-js').on('click', function (e) {
                $('body').removeClass('search-mobile');
                e.preventDefault();
            });
        }
    }
});
"use strict";

angular.module('SmartAdmin.Layout').directive('toggleMenu', function(){
    return {
        restrict: 'A',
        link: function(scope, element){
            var $body = $('body');

            var toggleMenu = function(){
                if (!$body.hasClass("menu-on-top")){
                    $('html').toggleClass("hidden-menu-mobile-lock");
                    $body.toggleClass("hidden-menu");
                    $body.removeClass("minified");
                } else if ( $body.hasClass("menu-on-top") && $body.hasClass("mobile-view-activated") ) {
                    $('html').toggleClass("hidden-menu-mobile-lock");
                    $body.toggleClass("hidden-menu");
                    $body.removeClass("minified");
                }
            };

            element.on('click', toggleMenu);

            scope.$on('requestToggleMenu', function(){
                toggleMenu();
            });
        }
    }
});
'use strict';

angular.module('SmartAdmin.Layout').directive('bigBreadcrumbs', function () {
    return {
        restrict: 'EA',
        replace: true,
        template: '<div><h1 class="page-title txt-color-blueDark"></h1></div>',
        scope: {
            items: '=',
            icon: '@'
        },
        link: function (scope, element) {
            var first = _.first(scope.items);

            var icon = scope.icon || 'home';
            element.find('h1').append('<i class="fa-fw fa fa-' + icon + '"></i> ' + first);
            _.rest(scope.items).forEach(function (item) {
                element.find('h1').append(' <span>> ' + item + '</span>')
            })
        }
    }
});

'use strict';

angular.module('SmartAdmin.Layout').directive('dismisser', function () {
    return {
        restrict: 'A',
        compile: function (element) {
            element.removeAttr('dismisser data-dissmiser')
            var closer = '<button class="close">&times;</button>';
            element.prepend(closer);
            element.on('click', '>button.close', function(){
                element.fadeOut('fast',function(){ $(this).remove(); });

            })
        }
    }
});
'use strict';

angular.module('SmartAdmin.Layout').directive('hrefVoid', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attributes) {
            element.attr('href','#');
            element.on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
            })
        }
    }
});
'use strict';

/*
* Directive for toggling a ng-model with a button
* Source: https://gist.github.com/aeife/9374784
*/

angular.module('SmartAdmin.Layout').directive('radioToggle', function ($log) {
    return {
        scope: {
            model: "=ngModel",
            value: "@value"
        },
        link: function(scope, element, attrs) {

            element.parent().on('click', function() {
                scope.model = scope.value;
                scope.$apply();
            });
        }
    }
});
/**
 * DETECT MOBILE DEVICES
 * Description: Detects mobile device - if any of the listed device is
 *
 * detected class is inserted to <tElement>.
 *
 *  (so far this is covering most hand held devices)
 */
'use strict';

angular.module('SmartAdmin.Layout').directive('smartDeviceDetect', function () {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-device-detect data-smart-device-detect');

            var isMobile = (/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase()));
            
            tElement.toggleClass('desktop-detected', !isMobile);
            tElement.toggleClass('mobile-detected', isMobile);


        }
    }
});
/**
 *
 * Description: Directive utilizes FastClick library.
 *
 *
 * FastClick is a simple, easy-to-use library for eliminating the
 * 300ms delay between a physical tap and the firing of a click event on mobile browsers.
 * FastClick doesn't attach any listeners on desktop browsers.
 * @link: https://github.com/ftlabs/fastclick
 *
 * On mobile devices 'needsclick' class is attached to <tElement>
 *
 */


'use strict';

angular.module('SmartAdmin.Layout').directive('smartFastClick', function () {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-fast-click data-smart-fast-click');

            FastClick.attach(tElement);

            if(!FastClick.notNeeded())
                tElement.addClass('needsclick')
        }
    }
});

/*'use strict';

angular.module('SmartAdmin.Layout').directive('smartFitAppView', function ($rootScope, SmartCss) {
    return {
        restrict: 'A',
        compile: function (element, attributes) {
            element.removeAttr('smart-fit-app-view data-smart-fit-app-view leading-y data-leading-y');

            var leadingY = attributes.leadingY ? parseInt(attributes.leadingY) : 0;

            var selector = attributes.smartFitAppView;

            if(SmartCss.appViewSize && SmartCss.appViewSize.height){
                var height =  SmartCss.appViewSize.height - leadingY < 252 ? 252 :  SmartCss.appViewSize.height - leadingY;
                SmartCss.add(selector, 'height', height+'px');
            }

            var listenerDestroy = $rootScope.$on('$smartContentResize', function (event, data) {
                var height = data.height - leadingY < 252 ? 252 : data.height - leadingY;
                SmartCss.add(selector, 'height', height+'px');
            });

            element.on('$destroy', function () {
                listenerDestroy();
                SmartCss.remove(selector, 'height');
            });


        }
    }
});*/

"use strict";

angular.module('SmartAdmin.Layout').directive('smartInclude', function () {
        return {
            replace: true,
            restrict: 'A',
            templateUrl: function (element, attr) {
                return attr.smartInclude;
            },
            compile: function(element){
                element[0].className = element[0].className.replace(/placeholder[^\s]+/g, '');
            }
        };
    }
);


'use strict';

angular.module('SmartAdmin.Layout').directive('smartLayout', function ($rootScope, $timeout, $interval, $q, SmartCss, APP_CONFIG) {
    
    var _debug = 0;

    function getDocHeight() {
        var D = document;
        return Math.max(
            D.body.scrollHeight, D.documentElement.scrollHeight,
            D.body.offsetHeight, D.documentElement.offsetHeight,
            D.body.clientHeight, D.documentElement.clientHeight
        );
    }

    var initialized = false, 
           initializedResolver = $q.defer();
    initializedResolver.promise.then(function () {
        initialized = true;
    });

    var $window = $(window),
        $document = $(document),
        $html = $('html'),
        $body = $('body'),
        $navigation ,
        $menu,
        $ribbon,
        $footer,
        $contentAnimContainer;


    (function cacheElements() {
        $navigation = $('#header');
        $menu = $('#left-panel');
        $ribbon = $('#ribbon');
        $footer = $('.page-footer');
        if (_.every([$navigation, $menu, $ribbon, $footer], function ($it) {
            return angular.isNumber($it.height())
        })) {
            initializedResolver.resolve();
        } else {
            $timeout(cacheElements, 100);
        }
    })();

    (function applyConfigSkin(){
        if(APP_CONFIG.smartSkin){
            $body.removeClass(_.pluck(APP_CONFIG.skins, 'name').join(' '));
            $body.addClass(APP_CONFIG.smartSkin);
        }
    })();


    return {
        priority: 2014,
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-layout data-smart-layout');

            var appViewHeight = 0 ,
                appViewWidth = 0,
                calcWidth,
                calcHeight,
                deltaX,
                deltaY;

            var forceResizeTrigger = false;

            function resizeListener() {

//                    full window height appHeight = Math.max($menu.outerHeight() - 10, getDocHeight() - 10);

                var menuHeight = $body.hasClass('menu-on-top') && $menu.is(':visible') ? $menu.height() : 0;
                var menuWidth = !$body.hasClass('menu-on-top') && $menu.is(':visible') ? $menu.width() + $menu.offset().left : 0;

                var $content = $('#content');
                var contentXPad = $content.outerWidth(true) - $content.width();
                var contentYPad = $content.outerHeight(true) - $content.height();


                calcWidth = $window.width() - menuWidth - contentXPad;
                calcHeight = $window.height() - menuHeight - contentYPad - $navigation.height() - $ribbon.height() - $footer.height();

                deltaX = appViewWidth - calcWidth;
                deltaY = appViewHeight - calcHeight;
                if (Math.abs(deltaX) || Math.abs(deltaY) || forceResizeTrigger) {

                    //console.log('exec', calcWidth, calcHeight);
                    $rootScope.$broadcast('$smartContentResize', {
                        width: calcWidth,
                        height: calcHeight,
                        deltaX: deltaX,
                        deltaY: deltaY
                    });
                    appViewWidth = calcWidth;
                    appViewHeight = calcHeight;
                    forceResizeTrigger = false;
                }
            }


            var looping = false;
            $interval(function () {
                if (looping) loop();
            }, 300);

            var debouncedRun = _.debounce(function () {
                run(300)
            }, 300);

            function run(delay) {
                initializedResolver.promise.then(function () {
                    attachOnResize(delay);
                });
            }

            run(10);

            function detachOnResize() {
                looping = false;
            }

            function attachOnResize(delay) {
                $timeout(function () {
                    looping = true;
                }, delay);
            }

            function loop() {
                $body.toggleClass('mobile-view-activated', $window.width() < 979);

                if ($window.width() < 979)
                    $body.removeClass('minified');

                resizeListener();
            }

            function handleHtmlId(toState) {
                if (toState.data && toState.data.htmlId) $html.attr('id', toState.data.htmlId);
                else $html.removeAttr('id');
            }

            $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                //console.log(1, '$stateChangeStart', event, toState, toParams, fromState, fromParams);

                handleHtmlId(toState);
                detachOnResize();
            });

            // initialized with 1 cause we came here with one $viewContentLoading request
            var viewContentLoading = 1;
            $rootScope.$on('$viewContentLoading', function (event, viewConfig) {
                //console.log(2, '$viewContentLoading', event, viewConfig);
                viewContentLoading++;
            });

            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                //console.log(3, '$stateChangeSuccess', event, toState, toParams, fromState, fromParams);
                forceResizeTrigger = true;
            });

            $rootScope.$on('$viewContentLoaded', function (event) {
                //console.log(4, '$viewContentLoaded', event);
                viewContentLoading--;

                if (viewContentLoading == 0 && initialized) {
                    debouncedRun();
                }
            });
        }
    }
});



'use strict';

angular.module('SmartAdmin.Layout').directive('smartPageTitle', function ($rootScope, $timeout) {
    return {
        restrict: 'A',
        compile: function (element, attributes) {
            element.removeAttr('smart-page-title data-smart-page-title');

            var defaultTitle = attributes.smartPageTitle;
            var listener = function(event, toState, toParams, fromState, fromParams) {
                var title = defaultTitle;
                if (toState.data && toState.data.title) title = title + ' | ' + toState.data.title;
                // Set asynchronously so page changes before title does
                $timeout(function() {
                    $('html head title').text(title);
                });
            };

            $rootScope.$on('$stateChangeStart', listener);

        }
    }
});
'use strict';

angular.module('SmartAdmin.Layout').directive('smartRouterAnimationWrap', function ($rootScope,$timeout) {
    return {
        restrict: 'A',
        compile: function (element, attributes) {
            element.removeAttr('smart-router-animation-wrap data-smart-router-animation-wrap wrap-for data-wrap-for');

            element.addClass('router-animation-container');


            var $loader = $('<div class="router-animation-loader"><i class="fa fa-gear fa-4x fa-spin"></i></div>')
                .css({
                    position: 'absolute',
                    top: 50,
                    left: 10
                }).hide().appendTo(element);


            var animateElementSelector = attributes.wrapFor;
            var viewsToMatch = attributes.smartRouterAnimationWrap.split(/\s/);

            var needRunContentViewAnimEnd = false;
            function contentViewAnimStart() {
                needRunContentViewAnimEnd = true;
                element.css({
                    height: element.height() + 'px',
                    overflow: 'hidden'
                }).addClass('active');
                $loader.fadeIn();

                $(animateElementSelector).addClass('animated faster fadeOutDown');
            }

            function contentViewAnimEnd() {
                if(needRunContentViewAnimEnd){
                    element.css({
                        height: 'auto',
                        overflow: 'visible'
                    }).removeClass('active');
                    

                    $(animateElementSelector).addClass('animated faster fadeInUp');

                    needRunContentViewAnimEnd = false;

                    $timeout(function(){
                        $(animateElementSelector).removeClass('animated');
                    },10);
                }
                $loader.fadeOut();
            }


            var destroyForStart = $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
                var isAnimRequired = _.any(viewsToMatch, function(view){
                   return _.has(toState.views, view) || _.has(fromState.views, view);
                });
                if(isAnimRequired){
                    contentViewAnimStart()
                }
            });

            var destroyForEnd = $rootScope.$on('$viewContentLoaded', function (event) {
                contentViewAnimEnd();
            });

            element.on('$destroy', function(){
                destroyForStart();
                destroyForEnd();

            });



        }
    }
});
angular.module('SmartAdmin.Layout').directive('speechRecognition', function ($log) {
	'use strict';

	$.root_ = $('body');
	var root, commands;

    root = window;
    window.appConfig = window.appConfig || {};

	if (appConfig.voice_command) {
		commands = appConfig.commands;
	}


	/*
	 * SMART VOICE
	 * Author: MyOrange | @bootstraphunt
	 * http://www.myorange.ca
	 */

	var SpeechRecognition = root.SpeechRecognition || root.webkitSpeechRecognition || root.mozSpeechRecognition || root.msSpeechRecognition || root.oSpeechRecognition;

// ref: http://updates.html5rocks.com/2013/01/Voice-Driven-Web-Apps-Introduction-to-the-Web-Speech-API


// function
	$.speechApp = (function(speech) {

		speech.start = function() {

			// Add our commands to smartSpeechRecognition
			smartSpeechRecognition.addCommands(commands);

			if (smartSpeechRecognition) {
				// activate plugin
				smartSpeechRecognition.start();
				// add btn class
				$.root_.addClass("voice-command-active");
				// play sound
				$.speechApp.playON();
				// set localStorage when switch is on manually
				if (appConfig.voice_localStorage) {
					localStorage.setItem('sm-setautovoice', 'true');
				}

			} else {
				// if plugin not found
				alert("speech plugin not loaded");
			}

		};
		speech.stop = function() {

			if (smartSpeechRecognition) {
				// deactivate plugin
				smartSpeechRecognition.abort();
				// remove btn class
				$.root_.removeClass("voice-command-active");
				// sound
				$.speechApp.playOFF();
				// del localStorage when switch if off manually
				if (appConfig.voice_localStorage) {
					localStorage.setItem('sm-setautovoice', 'false');
				}
				// remove popover if visible
				if ($('#speech-btn .popover').is(':visible')) {
					$('#speech-btn .popover').fadeOut(250);
				}
			}

		};

		// play sound
		speech.playON = function() {

			var audioElement = document.createElement('audio');

			if (navigator.userAgent.match('Firefox/'))
				audioElement.setAttribute('src', appConfig.sound_path + 'voice_on' + ".ogg");
			else
				audioElement.setAttribute('src', appConfig.sound_path + 'voice_on' + ".mp3");

			//$.get();
			audioElement.addEventListener("load", function() {
				audioElement.play();
			}, true);

			if (appConfig.sound_on) {
				audioElement.pause();
				audioElement.play();
			}
		};

		speech.playOFF = function() {

			var audioElement = document.createElement('audio');

			if (navigator.userAgent.match('Firefox/'))
				audioElement.setAttribute('src', appConfig.sound_path + 'voice_off' + ".ogg");
			else
				audioElement.setAttribute('src', appConfig.sound_path + 'voice_off' + ".mp3");

			$.get();
			audioElement.addEventListener("load", function() {
				audioElement.play();
			}, true);

			if (appConfig.sound_on) {
				audioElement.pause();
				audioElement.play();
			}
		};

		speech.playConfirmation = function() {

			var audioElement = document.createElement('audio');

			if (navigator.userAgent.match('Firefox/'))
				audioElement.setAttribute('src', appConfig.sound_path + 'voice_alert' + ".ogg");
			else
				audioElement.setAttribute('src', appConfig.sound_path + 'voice_alert' + ".mp3");

			$.get();
			audioElement.addEventListener("load", function() {
				audioElement.play();
			}, true);

			if (appConfig.sound_on) {
				audioElement.pause();
				audioElement.play();
			}
		};

		return speech;

	})({});



	/*
	 * SPEECH RECOGNITION ENGINE
	 * Copyright (c) 2013 Tal Ater
	 * Modified by MyOrange
	 * All modifications made are hereby copyright (c) 2014 MyOrange
	 */

	(function(undefined) {"use strict";

		// Check browser support
		// This is done as early as possible, to make it as fast as possible for unsupported browsers
		if (!SpeechRecognition) {
			root.smartSpeechRecognition = null;
			return undefined;
		}

		var commandsList = [], recognition, callbacks = {
				start : [],
				error : [],
				end : [],
				result : [],
				resultMatch : [],
				resultNoMatch : [],
				errorNetwork : [],
				errorPermissionBlocked : [],
				errorPermissionDenied : []
			}, autoRestart, lastStartedAt = 0,
		//debugState = false, // decleared in app.appConfig.js
		//appConfig.debugStyle = 'font-weight: bold; color: #00f;', // decleared in app.appConfig.js

		// The command matching code is a modified version of Backbone.Router by Jeremy Ashkenas, under the MIT license.
			optionalParam = /\s*\((.*?)\)\s*/g, optionalRegex = /(\(\?:[^)]+\))\?/g, namedParam = /(\(\?)?:\w+/g, splatParam = /\*\w+/g, escapeRegExp = /[\-{}\[\]+?.,\\\^$|#]/g, commandToRegExp = function(command) {
				command = command.replace(escapeRegExp, '\\$&').replace(optionalParam, '(?:$1)?').replace(namedParam, function(match, optional) {
					return optional ? match : '([^\\s]+)';
				}).replace(splatParam, '(.*?)').replace(optionalRegex, '\\s*$1?\\s*');
				return new RegExp('^' + command + '$', 'i');
			};

		// This method receives an array of callbacks to iterate over, and invokes each of them
		var invokeCallbacks = function(callbacks) {
			callbacks.forEach(function(callback) {
				callback.callback.apply(callback.context);
			});
		};

		var initIfNeeded = function() {
			if (!isInitialized()) {
				root.smartSpeechRecognition.init({}, false);
			}
		};

		var isInitialized = function() {
			return recognition !== undefined;
		};

		root.smartSpeechRecognition = {
			// Initialize smartSpeechRecognition with a list of commands to recognize.
			// e.g. smartSpeechRecognition.init({'hello :name': helloFunction})
			// smartSpeechRecognition understands commands with named variables, splats, and optional words.
			init : function(commands, resetCommands) {

				// resetCommands defaults to true
				if (resetCommands === undefined) {
					resetCommands = true;
				} else {
					resetCommands = !!resetCommands;
				}

				// Abort previous instances of recognition already running
				if (recognition && recognition.abort) {
					recognition.abort();
				}

				// initiate SpeechRecognition
				recognition = new SpeechRecognition();

				// Set the max number of alternative transcripts to try and match with a command
				recognition.maxAlternatives = 5;
				recognition.continuous = true;
				// Sets the language to the default 'en-US'. This can be changed with smartSpeechRecognition.setLanguage()
				recognition.lang = appConfig.voice_command_lang || 'en-US';

				recognition.onstart = function() {
					invokeCallbacks(callbacks.start);
					//debugState
					if (appConfig.debugState) {
						root.console.log('%c ✔ SUCCESS: User allowed access the microphone service to start ', appConfig.debugStyle_success);
						root.console.log('Language setting is set to: ' + recognition.lang, appConfig.debugStyle);
					}
					$.root_.removeClass("service-not-allowed");
					$.root_.addClass("service-allowed");
				};

				recognition.onerror = function(event) {
					invokeCallbacks(callbacks.error);
					switch (event.error) {
						case 'network':
							invokeCallbacks(callbacks.errorNetwork);
							break;
						case 'not-allowed':
						case 'service-not-allowed':
							// if permission to use the mic is denied, turn off auto-restart
							autoRestart = false;
							$.root_.removeClass("service-allowed");
							$.root_.addClass("service-not-allowed");
							//debugState
							if (appConfig.debugState) {
								root.console.log('%c WARNING: Microphone was not detected (either user denied access or it is not installed properly) ', appConfig.debugStyle_warning);
							}
							// determine if permission was denied by user or automatically.
							if (new Date().getTime() - lastStartedAt < 200) {
								invokeCallbacks(callbacks.errorPermissionBlocked);
							} else {
								invokeCallbacks(callbacks.errorPermissionDenied);
								//console.log("You need your mic to be active")
							}
							break;
					}
				};

				recognition.onend = function() {
					invokeCallbacks(callbacks.end);
					// smartSpeechRecognition will auto restart if it is closed automatically and not by user action.
					if (autoRestart) {
						// play nicely with the browser, and never restart smartSpeechRecognition automatically more than once per second
						var timeSinceLastStart = new Date().getTime() - lastStartedAt;
						if (timeSinceLastStart < 1000) {
							setTimeout(root.smartSpeechRecognition.start, 1000 - timeSinceLastStart);
						} else {
							root.smartSpeechRecognition.start();
						}
					}
				};

				recognition.onresult = function(event) {
					invokeCallbacks(callbacks.result);

					var results = event.results[event.resultIndex], commandText;

					// go over each of the 5 results and alternative results received (we've set maxAlternatives to 5 above)
					for (var i = 0; i < results.length; i++) {
						// the text recognized
						commandText = results[i].transcript.trim();
						if (appConfig.debugState) {
							root.console.log('Speech recognized: %c' + commandText, appConfig.debugStyle);
						}

						// try and match recognized text to one of the commands on the list
						for (var j = 0, l = commandsList.length; j < l; j++) {
							var result = commandsList[j].command.exec(commandText);
							if (result) {
								var parameters = result.slice(1);
								if (appConfig.debugState) {
									root.console.log('command matched: %c' + commandsList[j].originalPhrase, appConfig.debugStyle);
									if (parameters.length) {
										root.console.log('with parameters', parameters);
									}
								}
								// execute the matched command
								commandsList[j].callback.apply(this, parameters);
								invokeCallbacks(callbacks.resultMatch);

								// for commands "sound on", "stop" and "mute" do not play sound or display message
								//var myMatchedCommand = commandsList[j].originalPhrase;

								var ignoreCallsFor = ["sound on", "mute", "stop"];

								if (ignoreCallsFor.indexOf(commandsList[j].originalPhrase) < 0) {
									// play sound when match found
									console.log(2);
									$.smallBox({
										title : (commandsList[j].originalPhrase),
										content : "loading...",
										color : "#333",
										sound_file : 'voice_alert',
										timeout : 2000
									});

									if ($('#speech-btn .popover').is(':visible')) {
										$('#speech-btn .popover').fadeOut(250);
									}
								}// end if

								return true;
							}
						} // end for
					}// end for

					invokeCallbacks(callbacks.resultNoMatch);
					//console.log("no match found for: " + commandText)
					$.smallBox({
						title : "Error: <strong>" + ' " ' + commandText + ' " ' + "</strong> no match found!",
						content : "Please speak clearly into the microphone",
						color : "#a90329",
						timeout : 5000,
						icon : "fa fa-microphone"
					});
					if ($('#speech-btn .popover').is(':visible')) {
						$('#speech-btn .popover').fadeOut(250);
					}
					return false;
				};

				// build commands list
				if (resetCommands) {
					commandsList = [];
				}
				if (commands.length) {
					this.addCommands(commands);
				}
			},

			// Start listening (asking for permission first, if needed).
			// Call this after you've initialized smartSpeechRecognition with commands.
			// Receives an optional options object:
			// { autoRestart: true }
			start : function(options) {
				initIfNeeded();
				options = options || {};
				if (options.autoRestart !== undefined) {
					autoRestart = !!options.autoRestart;
				} else {
					autoRestart = true;
				}
				lastStartedAt = new Date().getTime();
				recognition.start();
			},

			// abort the listening session (aka stop)
			abort : function() {
				autoRestart = false;
				if (isInitialized) {
					recognition.abort();
				}
			},

			// Turn on output of debug messages to the console. Ugly, but super-handy!
			debug : function(newState) {
				if (arguments.length > 0) {
					appConfig.debugState = !!newState;
				} else {
					appConfig.debugState = true;
				}
			},

			// Set the language the user will speak in. If not called, defaults to 'en-US'.
			// e.g. 'fr-FR' (French-France), 'es-CR' (Español-Costa Rica)
			setLanguage : function(language) {
				initIfNeeded();
				recognition.lang = language;
			},

			// Add additional commands that smartSpeechRecognition will respond to. Similar in syntax to smartSpeechRecognition.init()
			addCommands : function(commands) {
				var cb, command;

				initIfNeeded();

				for (var phrase in commands) {
					if (commands.hasOwnProperty(phrase)) {
						cb = root[commands[phrase]] || commands[phrase];
						if ( typeof cb !== 'function') {
							continue;
						}
						//convert command to regex
						command = commandToRegExp(phrase);

						commandsList.push({
							command : command,
							callback : cb,
							originalPhrase : phrase
						});
					}
				}
				if (appConfig.debugState) {
					root.console.log('Commands successfully loaded: %c' + commandsList.length, appConfig.debugStyle);
				}
			},

			// Remove existing commands. Called with a single phrase, array of phrases, or methodically. Pass no params to remove all commands.
			removeCommands : function(commandsToRemove) {
				if (commandsToRemove === undefined) {
					commandsList = [];
					return;
				}
				commandsToRemove = Array.isArray(commandsToRemove) ? commandsToRemove : [commandsToRemove];
				commandsList = commandsList.filter(function(command) {
					for (var i = 0; i < commandsToRemove.length; i++) {
						if (commandsToRemove[i] === command.originalPhrase) {
							return false;
						}
					}
					return true;
				});
			},

			// Lets the user add a callback of one of 9 types:
			// start, error, end, result, resultMatch, resultNoMatch, errorNetwork, errorPermissionBlocked, errorPermissionDenied
			// Can also optionally receive a context for the callback function as the third argument
			addCallback : function(type, callback, context) {
				if (callbacks[type] === undefined) {
					return;
				}
				var cb = root[callback] || callback;
				if ( typeof cb !== 'function') {
					return;
				}
				callbacks[type].push({
					callback : cb,
					context : context || this
				});
			}
		};

	}).call(this);

	var autoStart = function() {

		smartSpeechRecognition.addCommands(commands);

		if (smartSpeechRecognition) {
			// activate plugin
			smartSpeechRecognition.start();
			// add btn class
			$.root_.addClass("voice-command-active");
			// set localStorage when switch is on manually
			if (appConfig.voice_localStorage) {
				localStorage.setItem('sm-setautovoice', 'true');
			}

		} else {
			// if plugin not found
			alert("speech plugin not loaded");
		}
	}
// if already running with localstorage
	if (SpeechRecognition && appConfig.voice_command && localStorage.getItem('sm-setautovoice') == 'true') {
		autoStart();
	}

// auto start
	if (SpeechRecognition && appConfig.voice_command_auto && appConfig.voice_command) {
		autoStart();
	}


	var link = function(scope, element) {


		if (SpeechRecognition && appConfig.voice_command) {

			// create dynamic modal instance
			var modal = $('<div class="modal fade" id="voiceModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"></div></div></div>');
			// attach to body
			modal.appendTo("body");

			element.on("click", function(e) {

            	if ($.root_.hasClass("voice-command-active")) {
					$.speechApp.stop();
					//$('#speech-btn > span > a > i').removeClass().addClass('fa fa-microphone-slash');
				} else {
					$.speechApp.start();
					//add popover
					$('#speech-btn .popover').fadeIn(350);
					//$('#speech-btn > span > a > i').removeClass().addClass('fa fa-microphone')

				}

				e.preventDefault();

            });

			//remove popover
			$(document).mouseup(function(e) {
				if (!$('#speech-btn .popover').is(e.target) && $('#speech-btn .popover').has(e.target).length === 0) {
					$('#speech-btn .popover').fadeOut(250);
				}
			});


			$("#speech-help-btn").on("click", function() {
				commands.help();
			});

		}
		else {
			$("#speech-btn").addClass("display-none");
		}


	}



    return {
        restrict: 'AE',
        link: link
    }
});

'use strict';

angular.module('SmartAdmin.Layout').directive('stateBreadcrumbs', function ($rootScope, $state) {


    return {
        restrict: 'EA',
        replace: true,
        template: '<ol class="breadcrumb"><li>Home</li></ol>',
        link: function (scope, element) {

            function setBreadcrumbs(breadcrumbs) {
                var html = '';
                angular.forEach(breadcrumbs, function (crumb) {
                    html += '<li>' + crumb + '</li>'
                });
                element.html(html)
            }

            function fetchBreadcrumbs(stateName, breadcrunbs) {

                var state = $state.get(stateName);

                if (state && state.data && state.data.title && breadcrunbs.indexOf(state.data.title) == -1) {
                    breadcrunbs.unshift(state.data.title)
                }

                var parentName = stateName.replace(/.?\w+$/, '');
                if (parentName) {
                    return fetchBreadcrumbs(parentName, breadcrunbs);
                } else {
                    return breadcrunbs;
                }
            }

            function processState(state) {
                var breadcrumbs;
                if (state.data && state.data.breadcrumbs) {
                    breadcrumbs = state.data.breadcrumbs;
                } else {
                    breadcrumbs = fetchBreadcrumbs(state.name, []);
                }
                setBreadcrumbs(breadcrumbs);
            }

            processState($state.current);

            $rootScope.$on('$stateChangeStart', function (event, state) {
                processState(state);
            })
        }
    }
});
'use strict';

angular.module('SmartAdmin.Layout').factory('SmartCss', function ($rootScope, $timeout) {

    var sheet = (function () {
        // Create the <style> tag
        var style = document.createElement("style");

        // Add a media (and/or media query) here if you'd like!
        // style.setAttribute("media", "screen")
        // style.setAttribute("media", "@media only screen and (max-width : 1024px)")

        // WebKit hack :(
        style.appendChild(document.createTextNode(""));

        // Add the <style> element to the page
        document.head.appendChild(style);

        return style.sheet;
    })();

    var _styles = {};


    var SmartCss = {
        writeRule: function(selector){
            SmartCss.deleteRuleFor(selector);
            if(_.has(_styles, selector)){
                var css = selector + '{ ' + _.map(_styles[selector], function(v, k){
                    return  k + ':' +  v + ';'
                }).join(' ') +'}';
                sheet.insertRule(css, _.size(_styles) - 1);
            }
        },
        add: function (selector, property, value, delay) {
            if(!_.has(_styles, selector))
                _styles[selector] = {};

            if(value == undefined || value == null || value == '')
                delete _styles[selector][property];
            else
                _styles[selector][property] = value;


            if(_.keys(_styles[selector]).length == 0)
                delete _styles[selector];

            if(!delay)
                delay = 0;
            $timeout(function(){
                SmartCss.writeRule(selector);
            }, delay);

        },
        remove: function(selector, property, delay){
            SmartCss.add(selector, property, null, delay);
        },
        deleteRuleFor: function (selector) {
            _(sheet.rules).forEach(function (rule, idx) {
                if (rule.selectorText == selector) {
                    sheet.deleteRule(idx);
                }
            });
        },
        appViewSize: null
    };

    $rootScope.$on('$smartContentResize', function (event, data) {
        SmartCss.appViewSize = data;
    });

    return SmartCss;

});




'use strict';

angular.module('SmartAdmin.Layout').factory('lazyScript', function($q, $http){

    var cache = {};

    function isPending(scriptName){
        return (cache.hasOwnProperty(scriptName) && cache[scriptName].promise && cache[scriptName].promise.$$state.pending)
    }

    function isRegistered(scriptName){
        return cache.hasOwnProperty(scriptName)
    }
    function loadScript(scriptName){
        if(!cache[scriptName]){
            cache[scriptName] = $q.defer();
            var el = document.createElement( 'script' );
            el.onload = function(script){
                console.log('script is lazy loaded:', scriptName)
                cache[scriptName].resolve(scriptName);
            };
            el.src = scriptName;
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(el, x);
            
        }
        return cache[scriptName].promise;

    }

    function register(scriptName){
        if(isPending(scriptName)){
            return cache[scriptName].promise
        }
        if(isRegistered(scriptName)){
            return $q.resolve(scriptName);
        } else {
            var dfd = $q.defer();

            loadScript(scriptName).then(function(){
                dfd.resolve(scriptName);
            });

            return dfd.promise; 

        }
    }
    return {
        register: function (scripts) {
            
            var dfd = $q.defer();
            var promises = [];
            if (angular.isString(scripts))
                scripts = [scripts];

            angular.forEach(scripts, function(script){
                promises.push(register(script));
            })

            $q.all(promises).then(function(resolves){
                dfd.resolve(resolves);
            })
            return dfd.promise;

        }
    };
});

"use strict";

angular.module('SmartAdmin.UI').directive('smartPopoverHtml', function () {
    return {
        restrict: "A",
        link: function(scope, element, attributes){
            var options = {};
            options.content = attributes.smartPopoverHtml;
            options.placement = attributes.popoverPlacement || 'top';
            options.html = true;
            options.trigger =  attributes.popoverTrigger || 'click';
            options.title =  attributes.popoverTitle || attributes.title;
            element.popover(options)

        }

    };
});


"use strict";

angular.module('SmartAdmin.UI').directive('smartTooltipHtml', function () {
        return {
            restrict: 'A',
            link: function(scope, element, attributes){
                element.tooltip({
                    placement: attributes.tooltipPlacement || 'top',
                    html: true,
                    title: attributes.smartTooltipHtml
                })
            }
        };
    }
);

angular.module('app.admin')
    .controller('ModulosAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.module = {
                moduleId: null,
                name: '',
                namePermission: '',
                codePermission: '',
                permissions: [] // {modulePermissionId, name, code}, {...}
            };

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('module.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('module.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('module.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'moduleGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;
                        $scope.module.moduleId = +data.moduleId;
                        $scope.module.name = data.name;
                        for (var i = 0; i < data.permissions.length; i++) {
                            $scope.module.permissions.push($.extend(data.permissions[i], {
                                updating: false,
                                error: ''
                            }));
                        }

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar el modulo",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            $scope.addPermission = function () {
                $scope.form.submited = true;
                if (!!$scope.module.namePermission && !!$scope.module.codePermission) {
                    $scope.module.codePermission = $scope.module.codePermission.toUpperCase();

                    if ($scope.module.permissions.findIndex(function (permission) {
                            return permission !== null && permission.code == $scope.module.codePermission
                        }) < 0) {
                        $scope.module.permissions.push({
                            modulePermissionId: null,
                            name: $scope.module.namePermission,
                            code: $scope.module.codePermission,
                            updating: false,
                            error: ''
                        });
                        $scope.module.namePermission = '';
                        $scope.module.codePermission = '';
                        $scope.duplexPermission = false;
                    }
                    else
                        $scope.duplexPermission = true;
                }
                else
                    $scope.duplexPermission = false;
            };
            $scope.updatePermission = function (index) {
                var indexEditing;
                var currentEditing = $scope.module.permissions.find(function (permission, index) {
                    indexEditing = index;
                    return permission !== null && permission.updating == true
                });
                if (!!currentEditing) {
                    if (currentEditing.name === '' || currentEditing.code === '') {
                        currentEditing.error = 'Ingresa la información completa del permiso';
                        return;
                    }
                    else {
                        currentEditing.code = currentEditing.code.toUpperCase();
                        var indexFound = $scope.module.permissions.find(function (permission, index) {
                            return permission !== null && (permission.code == currentEditing.code && index != indexEditing)
                        });
                        if (!indexFound) {
                            currentEditing.error = '';
                            currentEditing.updating = false;
                        }
                        else {
                            currentEditing.error = 'Permiso duplicado.';
                            return;
                        }
                    }
                }
                else
                    indexEditing = undefined;
                if (index !== undefined && index !== indexEditing)
                    $scope.module.permissions[index].updating = !$scope.module.permissions[index].updating;
            };
            $scope.deletePermission = function (index) {
                $scope.module.permissions[index] = null;
            };
            $scope.cancel = function () {
                $state.go('app.admin.modulos');
                return false;
            };
            $scope.save = function () {
                $scope.updatePermission();
                $scope.addPermission();

                $scope.module.permissions = $scope.module.permissions.filter(function (permission) {
                    return permission !== null
                });

                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                }
                else if (
                    !!$scope.module.name && !$scope.module.namePermission && !$scope.module.codePermission
                    && !!$scope.module.permissions.length
                    && $scope.module.permissions.findIndex(function (permission) {
                        return permission.error != ''
                    }) < 0
                ) {
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('module.add')) {
                                restService.sendRequest($scope.module, 'modulePOST').then(function () {
                                    $.bigBox({
                                        title: "Nuevo modulo agregado",
                                        content: "Se guardo la información del modulo: <b>" + $scope.module.name + "</b>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del modulo';

                                    $.bigBox({
                                        title: "Error al guardar modulo",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('module.edit')) {
                                restService.sendRequest($scope.module, 'modulePUT', '/' + $scope.module.moduleId).then(function () {
                                    $.bigBox({
                                        title: "Modulo actualizado",
                                        content: "Se actualizo la información del modulo: <b>" + $scope.module.name + "</b>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del modulo';

                                    $.bigBox({
                                        title: "Error al guardar modulo",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);
angular.module('app.admin')
    .controller('ModulosCtrl', ['$scope', '$compile', '$state', 'DTOptionsBuilder', 'DTColumnBuilder', 'permissionsService', 'restService',
        function($scope, $compile, $state, DTOptionsBuilder, DTColumnBuilder, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;

            /* Datatable (DT), creacion y asignacion de eventos */
            var dataArrayResolved = [];
            $scope.standardOptions = DTOptionsBuilder
                .fromFnPromise(restService.sendRequest(null, 'moduleGET'))
                .withDataProp('data')
                .withDOM("t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap()
                .withOption('createdRow', createdRow);
            $scope.standardColumns = [
                DTColumnBuilder.newColumn('name').notSortable(),
                DTColumnBuilder.newColumn(null).notSortable().withClass('table-actions-buttons').renderWith(function (data) {
                    dataArrayResolved[data.moduleId] = data.name;
                    return '<a has-permission="MODULE_VIEW" title="Ver" ui-sref="app.admin.modulos.accion({accion: \'ver\', id: ' + data.moduleId + '})" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>' +
                        '<a has-permission="MODULE_EDIT" title="Editar" ui-sref="app.admin.modulos.accion({accion: \'editar\', id: ' + data.moduleId + '})" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>' +
                        '<a has-permission="MODULE_DELETE" title="Eliminar" ng-click="accion(\'delete\', ' + data.moduleId + ')" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }

            // Callback para funcionalidad de filtros
            $scope.instanceCallback = function (dtInstance) {
                $scope.dtInstance = dtInstance;
                var table = dtInstance.DataTable;
                table.columns().eq(0).each(function (colIdx) {
                    $('input, select', table.column(colIdx).header()).on('keyup change', function () {
                        if (this.getAttribute('id') == 'table-lenght-ctrl') {
                            table
                                .page.len(this.value)
                                .draw();
                        }
                        else {
                            var isSelect = (this.nodeName == 'SELECT');
                            var searchValue = (isSelect && this.value !== '') ? '\\b' + this.value + '\\b' : this.value;
                            table
                                .column(colIdx)
                                .search(searchValue, isSelect, !isSelect)
                                .draw();
                        }
                    });
                });
            };

            $scope.accion = function (accion, id) {
                switch (accion) {
                    case 'delete':
                    case 'eliminar':
                        if (permissionsService.canAccess('module.delete')) {
                            $.SmartMessageBox({
                                title: "Borrar Modulo",
                                content: "Esta a punto de borrar la información del modulo <b>" + dataArrayResolved[id] + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'moduleDELETE', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se borro la información del modulo <b>" + dataArrayResolved[id] + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'moduleGET'));
                                        else
                                            $state.reload();
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo borrar el modulo solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                }
            };
        }
    ]);
angular.module('app.admin')
    .controller('PerfilesAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.profile = {
                profileId: null,
                profileName: '',
                permissions: [], // [ <int>, <int>, ... ]
                tree: []
            };

            // Revisar permisos y evaluar solo letura de los campos
            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('profile.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    restService.sendRequest(null, 'profileTREE').then(function (data) {
                        data = data.data;
                        if (data.length < 1) {
                            $.bigBox({
                                title: "No existen modulos",
                                content: "No existen modulos de permisos para seleccionar, <br>agregue algun modulo desde el menu de <b>Modulos</b>",
                                color: "#C46A69",
                                icon: "fa fa-warning shake animated",
                                timeout: 6000
                            });
                            $scope.cancel();
                        }
                        $scope.profile.tree = data;

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar el arbol de permisos",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('profile.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('profile.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'profileGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;
                        $scope.profile.profileId = +data.profileId;
                        $scope.profile.profileName = data.profileName;
                        $scope.profile.tree = data.tree;
                        $scope.profile.tree.forEach(ifAllChildrenAreSelected);
                        if ($scope.readOnly)
                            readonlyTree($scope.profile.tree);

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar perfil",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            function ifAllChildrenAreSelected(node) {
                if (!node.active) {
                    var allChildrenSelected = true;
                    if (node.children && !!node.children.length) {
                        for (var i = 0; i < node.children.length; i++) {
                            if (!!node.children[i].children.length)
                                node.children[i].children.forEach(ifAllChildrenAreSelected);
                            if (allChildrenSelected)
                                allChildrenSelected = node.children[i].active;
                        }
                    }
                    node.active = allChildrenSelected;
                }
            }

            function readonlyTree(permissionNode) {
                permissionNode.forEach(function (node) {
                    node.readonly = $scope.readOnly;
                    if (node.children && !!node.children.length)
                        readonlyTree(node.children);
                });
            }

            $scope.cancel = function () {
                $state.go('app.admin.perfiles');
                return false;
            };

            // Guardar informacion, crea o edita un perfil con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                // Revisar que el formulario no tenga errores
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                } else {
                    $scope.permissions = [];
                    $(document.form).find('input:checkbox[permission]:checked').each(function () {
                        $scope.profile.permissions.push(+this.value);
                    });

                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('profile.add')) {
                                restService.sendRequest($scope.profile, 'profilePOST').then(function () {
                                    $.bigBox({
                                        title: "Nuevo perfil agregado",
                                        content: "Se agrego el perfil: <b>" + $scope.profile.profileName + "</b>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del perfil';

                                    $.bigBox({
                                        title: "Error al guardar perfil",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('profile.edit')) {
                                $scope.profile.profileId = $stateParams.id;
                                restService.sendRequest($scope.profile, 'profilePUT', '/' + $scope.profile.profileId).then(function () {
                                    $.bigBox({
                                        title: "Perfil actualizado",
                                        content: "Se actualizo el perfil: <b>" + $scope.profile.profileName + "</b>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del perfil';

                                    $.bigBox({
                                        title: "Error al guardar perfil",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);
angular.module('app.admin')
    .controller('PerfilesCtrl', ['$scope', '$compile', '$state', 'DTOptionsBuilder', 'DTColumnBuilder', 'permissionsService', 'restService',
        function ($scope, $compile, $state, DTOptionsBuilder, DTColumnBuilder, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;

            /* Datatable (DT), creacion y asignacion de eventos */
            var dataArrayResolved = [];
            $scope.standardOptions = DTOptionsBuilder
                .fromFnPromise(restService.sendRequest(null, 'profileGET'))
                .withDataProp('data')
                .withDOM("t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap()
                .withOption('createdRow', createdRow);
            $scope.standardColumns = [
                DTColumnBuilder.newColumn('profileName').notSortable(),
                DTColumnBuilder.newColumn(null).notSortable().withClass('table-actions-buttons').renderWith(function (data) {
                    dataArrayResolved[data.profileId] = data.profileName;
                    return '<a has-permission="PROFILE_VIEW" title="Ver" ui-sref="app.admin.perfiles.accion ({accion: \'ver\', id: ' + data.profileId + '})" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>' +
                        '<a has-permission="PROFILE_EDIT" title="Editar" ui-sref="app.admin.perfiles.accion({accion: \'editar\', id: ' + data.profileId + '})" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>' +
                        '<a has-permission="PROFILE_DELETE" title="Eliminar" ng-click="accion(\'delete\',' + data.profileId + ')" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }

            // Callback para funcionalidad de filtros
            $scope.instanceCallback = function (dtInstance) {
                $scope.dtInstance = dtInstance;
                var table = dtInstance.DataTable;
                table.columns().eq(0).each(function (colIdx) {
                    $('input, select', table.column(colIdx).header()).on('keyup change', function () {
                        if (this.getAttribute('id') == 'table-lenght-ctrl') {
                            table
                                .page.len(this.value)
                                .draw();
                        }
                        else {
                            var isSelect = (this.nodeName === 'SELECT');
                            var searchValue = (isSelect && this.value !== '') ? '\\b' + this.value + '\\b' : this.value;
                            table
                                .column(colIdx)
                                .search(searchValue, isSelect, !isSelect)
                                .draw();
                        }
                    });
                });
            };

            $scope.accion = function (accion, id) {
                switch (accion) {
                    case 'delete':
                    case 'eliminar':
                        if (permissionsService.canAccess('profile.delete')) {
                            $.SmartMessageBox({
                                title: "Borrar Perfil",
                                content: "Esta a punto de borrar la información del perfil <b>" + dataArrayResolved[id] + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'profileDELETE', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se borro la información del perfil <b>" + dataArrayResolved[id] + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'profileGET'));
                                        else
                                            $state.reload();
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo borrar el perfil solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                }
            };
        }
    ]);
angular.module('app.admin')
    .directive('perfilesTreeview', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            scope: {
                'items': '=',
                'readonly': '='
            },
            template: '<li ng-class="{parent_li: item.children.length}" ng-repeat="item in items" role="treeitem">' +
            '<perfiles-treeview-content></perfiles-treeview-content>' +
            '<ul ng-if="item.children.length" perfiles-treeview ng-show="item.expanded" items="item.children" role="group" class="smart-treeview-group"></ul>' +
            '</li>',
            compile: function (element, attributes) {
                // Break the recursion loop by removing the contents
                var contents = element.contents().remove();
                var compiledContents;
                return {
                    post: function (scope, element) {
                        // Compile the contents
                        if (!compiledContents) {
                            compiledContents = $compile(contents);
                        }
                        // Re-add the compiled contents to the element
                        compiledContents(scope, function (clone) {
                            element.append(clone);
                        });
                    }
                };
            }
        };
    }])
    .directive('perfilesTreeviewContent', function () {
        return {
            restrict: 'E',
            link: function (scope, element) {
                var iconOpenToggle = '', iconFolder = '', isPermission = '';
                if (scope.item.module || (scope.item.children && scope.item.children.length)) {
                    iconOpenToggle = '<i class="fa fa-lg fa-caret-right"></i>';
                    iconFolder = '<i class="fa fa-folder"></i>';
                }
                if (!scope.item.module) {
                    isPermission = 'permission';
                }
                var htmlCheckbox = '<span>' +
                    iconOpenToggle +
                    '<input type="checkbox"' +
                    'value="' + scope.item.id + '"';
                if (scope.item.active) {
                    htmlCheckbox += 'checked ';
                }
                if (scope.item.readonly) {
                    htmlCheckbox += 'disabled ';
                }
                htmlCheckbox += isPermission + '>' +
                    iconFolder +
                    scope.item.content +
                    '</span>';
                var $content = $(htmlCheckbox);

                function handleExpanded() {
                    $content.find('>i:first-child')
                        .toggleClass('fa-caret-right', !scope.item.expanded)
                        .toggleClass('fa-caret-down', !!scope.item.expanded);
                }

                if (scope.item.children && scope.item.children.length) {
                    $content.on('click', function () {
                        scope.$apply(function () {
                            scope.item.expanded = !scope.item.expanded;
                            handleExpanded();
                        });
                    });
                    $content.find('input').on('click', function (e) {
                        $(e.target.parentNode.parentNode).find('input:checkbox').each(function () {
                            this.checked = e.target.checked;
                        });
                        e.stopPropagation();
                    });

                    handleExpanded();
                }
                $content.find('input').on('click', function (e) {
                    var ulNodeClicked = e.target.parentNode.parentNode.parentNode;
                    toggleParentCheckboxOnChildrens(ulNodeClicked);
                });
                function toggleParentCheckboxOnChildrens(nodeOfCheckbox) {
                    if ($(nodeOfCheckbox).attr('role') !== 'tree') {
                        var childrensChecked = true;
                        $(nodeOfCheckbox).children().each(function () {
                            if (childrensChecked)
                                childrensChecked = this.children[0].getElementsByTagName('input')[0].checked;
                        });
                        nodeOfCheckbox.previousElementSibling.getElementsByTagName('input')[0].checked = childrensChecked;
                        toggleParentCheckboxOnChildrens(nodeOfCheckbox.parentNode.parentNode);
                    }
                }

                element.replaceWith($content);
            }
        }
    });
angular.module('app.admin')
    .controller('UsuariosAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'md5', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, md5, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.user = {
                userId: null,
                profileId: null,
                name: '',
                username: '',
                email: '',
                validEmail: false,
                realEmail: '',
                password: '',
                passwordConfirm: '',
                active: ''
            };

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('user.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    $scope.user.active = 'true';
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('user.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('user.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'userGET', '/findById/' + $stateParams.id).then(function (data) {
                        data = data.data;

                        $scope.user.userId = data.userId;
                        $scope.user.profileId = '' + data.profileId;
                        $scope.user.name = data.name;
                        $scope.user.email = data.email;
                        $scope.user.realEmail = data.email;
                        $scope.user.validEmail = true;
                        $scope.user.active = '' + data.active;

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar el usuario",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }
            // Number input
            $('input[type=number]').on('keydown', function (e) {
                if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57) || e.keyCode == 107 || e.keyCode == 187 || e.keyCode == 8 || e.keyCode == 46)
                    return true;
                else
                    return false;
            });

            // Email validator
            $('input[type=email]').on('keyup change input paste cut', function (ev) {
                var element = ev.target;
                $scope.user.validEmail = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($(element).val());
            });

            $scope.listOfProfiles = [];
            restService.sendRequest(null, 'profileGET').then(function (data) {
                $scope.listOfProfiles = data.data;
            }, function () {
                $.bigBox({
                    title: "Error al cargar perfiles",
                    content: "No se pudo cargar la lista de perfiles",
                    color: "#C46A69",
                    icon: "fa fa-warning shake animated",
                    timeout: 6000
                });
                $scope.cancel();
            });

            $scope.cancel = function () {
                $state.go('app.admin.usuarios');
                return false;
            };

            // Guardar informacion, crea o edita un usuario con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                    $scope.form.passwordConfirm.$setTouched();
                } else {
                    var tmpPassword;
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('user.add')) {
                                if (!!$scope.user.password && $scope.user.passwordConfirm && $scope.user.password === $scope.user.passwordConfirm) {
                                    $scope.user.username = $scope.user.email;
                                    $scope.user.active = ($scope.user.active.toLowerCase() === 'true');

                                    tmpPassword = $scope.user.password;
                                    $scope.user.password = md5.createHash($scope.user.password + '{' + $scope.user.email + '}');
                                    $scope.user.passwordConfirm = $scope.user.password;

                                    restService.sendRequest($scope.user, 'userPOST').then(function () {
                                        $.bigBox({
                                            title: "Nuevo usuario agregado",
                                            content: "Se guardo la información del usuario: <b>" + $scope.user.name + "</b> - <i>" + $scope.user.email + "</i>",
                                            color: "#739E73",
                                            icon: "fa fa-check",
                                            timeout: 8000
                                        });
                                        $scope.cancel();
                                    }, function (error) {
                                        $scope.user.password = tmpPassword;
                                        $scope.user.passwordConfirm = $scope.user.password;

                                        var errorMsg;
                                        if (!!error.code) {
                                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                                            else errorMsg += error.message;
                                        }
                                        else errorMsg = 'No se pudo guardar la información del usuario';

                                        $.bigBox({
                                            title: "Error al guardar el usuario",
                                            content: errorMsg,
                                            color: "#C46A69",
                                            icon: "fa fa-warning shake animated",
                                            timeout: 6000
                                        });
                                    });
                                }
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('user.edit')) {
                                $scope.user.email = $scope.user.realEmail;

                                var passwordOK = true;
                                tmpPassword = $scope.user.password;
                                if (!!$scope.user.password) {
                                    if ($scope.user.password === $scope.user.passwordConfirm) {
                                        $scope.user.password = md5.createHash($scope.user.password + '{' + $scope.user.email + '}');
                                        $scope.user.passwordConfirm = $scope.user.password;
                                    } else {
                                        $scope.form.passwordConfirm.$setTouched();
                                        passwordOK = false;
                                    }
                                }

                                if (passwordOK) {
                                    $scope.user.username = $scope.user.email;
                                    $scope.user.active = ($scope.user.active.toLowerCase() === 'true');
                                    restService.sendRequest($scope.user, 'userPUT', '/' + $scope.user.userId).then(function () {
                                        $.bigBox({
                                            title: "Usuario actualizado",
                                            content: "Se actualizo la información del usuario: <b>" + $scope.user.name + "</b> - <i>" + $scope.user.email + "</i>",
                                            color: "#739E73",
                                            icon: "fa fa-check",
                                            timeout: 8000
                                        });
                                        $scope.cancel();
                                    }, function (error) {
                                        $scope.user.password = tmpPassword;
                                        $scope.user.passwordConfirm = $scope.user.password;

                                        var errorMsg;
                                        if (!!error.code) {
                                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                                            else errorMsg += error.message;
                                        }
                                        else errorMsg = 'No se pudo guardar la información del usuario';

                                        $.bigBox({
                                            title: "Error al guardar el usuario",
                                            content: errorMsg,
                                            color: "#C46A69",
                                            icon: "fa fa-warning shake animated",
                                            timeout: 6000
                                        });
                                    });
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);
angular.module('app.admin')
    .controller('UsuariosCtrl', ['$scope', '$compile', '$state', 'DTOptionsBuilder', 'DTColumnBuilder', 'permissionsService', 'restService',
        function ($scope, $compile, $state, DTOptionsBuilder, DTColumnBuilder, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.statusFilter = {
                'true': 'Activo',
                'false': 'Inactivo'
            };

            /* Datatable (DT), creacion y asignacion de eventos */
            var dataArrayResolved = [];
            $scope.standardOptions = DTOptionsBuilder
                .fromFnPromise(restService.sendRequest(null, 'userGET'))
                .withDataProp('data')
                .withDOM("t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap()
                .withOption('aaSorting', [[1, 'asc'], [0, 'asc']])
                .withOption('createdRow', createdRow);
            $scope.standardColumns = [
                DTColumnBuilder.newColumn('name').notSortable(),
                DTColumnBuilder.newColumn('email').notSortable(),
                DTColumnBuilder.newColumn('active').notSortable().renderWith(function (data) {
                    return $scope.statusFilter[data];
                }),
                DTColumnBuilder.newColumn(null).notSortable().withClass('table-actions-buttons').renderWith(function (data) {
                    dataArrayResolved[data.userId] = data.name;
                    return '<a has-permission="USER_VIEW" title="Ver" ui-sref="app.admin.usuarios.accion({accion: \'ver\', id: ' + data.userId + '})" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>' +
                        '<a has-permission="USER_EDIT" title="Editar" ui-sref="app.admin.usuarios.accion({accion: \'editar\', id: ' + data.userId + '})" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>' +
                        '<a has-permission="USER_EDIT" title="' + (data.active ? 'Desactivar' : 'Activar') + '" ng-click="accion(\'' + (data.active ? 'deactivate' : 'activate') + '\', ' + data.userId + ')" class="btn btn-default btn-xs"><i class="fa ' + (data.active ? 'fa-ban' : 'fa-check-circle-o') + '"></i></a>' +
                        '<a has-permission="USER_DELETE" title="Eliminar" ng-click="accion(\'delete\', ' + data.userId + ')" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }

            // Callback para funcionalidad de filtros
            $scope.instanceCallback = function (dtInstance) {
                $scope.dtInstance = dtInstance;
                var table = dtInstance.DataTable;
                table.columns().eq(0).each(function (colIdx) {
                    $('input, select', table.column(colIdx).header()).on('keyup change', function () {
                        if (this.getAttribute('id') == 'table-lenght-ctrl') {
                            table
                                .page.len(this.value)
                                .draw();
                        }
                        else {
                            var isSelect = (this.nodeName == 'SELECT');
                            var searchValue = (isSelect && this.value !== '') ? '\\b' + this.value + '\\b' : this.value;
                            table
                                .column(colIdx)
                                .search(searchValue, isSelect, !isSelect)
                                .draw();
                        }
                    });
                });
            };

            $scope.accion = function (accion, id) {
                switch (accion) {
                    case 'delete':
                    case 'eliminar':
                        if (permissionsService.canAccess('user.delete')) {
                            $.SmartMessageBox({
                                title: "Borrar Usuario",
                                content: "Esta a punto de borrar la información del usuario <b>" + dataArrayResolved[id] + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'userDELETE', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se borro la información del usuario <b>" + dataArrayResolved[id] + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'userGET'));
                                        else
                                            $state.reload();
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo borrar el usuario solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                    case 'activate':
                    case 'deactivate':
                        if (permissionsService.canAccess('user.edit')) {
                            var active = (accion.toLowerCase() === 'activate');
                            $.SmartMessageBox({
                                title: (active ? 'Activar' : 'Desactivar') + " Usuario",
                                content: "Esta a punto de " + (active ? 'activar' : 'desactivar') + " el usuario <b>" + dataArrayResolved[id] + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'userPUT', '/' + id + '/' + active).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se " + (active ? 'activo' : 'desactivo') + " el usuario <b>" + dataArrayResolved[id] + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'userGET'));
                                        else
                                            $state.reload();
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo " + (active ? 'activar' : 'desactivar') + " el usuario solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                }
            };
        }
    ]);
'use strict';

angular.module('app.chat').factory('ChatApi', function ($q, $rootScope, User, $http, APP_CONFIG) {
    var dfd = $q.defer();
    var _user;
    var ChatSrv = {
        initialized: dfd.promise,
        users: [],
        messages: [],
        statuses: ['Online', 'Busy', 'Away', 'Log Off'],
        status: 'Online',
        setUser: function (user) {
            if (ChatSrv.users.indexOf(_user) != -1)
                ChatSrv.users.splice(ChatSrv.users.indexOf(_user), 1);
            _user = user;
            ChatSrv.users.push(_user);
        },
        sendMessage: function (text) {
            var message = {
                user: _user,
                body: text,
                date: new Date()
            };
            this.messages.push(message);
        }
    };


    $http.get(APP_CONFIG.apiRootUrl + '/chat.json').then(function(res){
        ChatSrv.messages = res.data.messages;
        ChatSrv.users = res.data.users;
        dfd.resolve();
    });

    ChatSrv.initialized.then(function () {

        User.initialized.then(function () {
            ChatSrv.setUser({
                username: User.username,
                picture: User.picture,
                status: ChatSrv.status
            });
        });

        $rootScope.$watch(function () {
            return User.username
        }, function (name, oldName) {
            if (name != oldName) {
                ChatSrv.setUser({
                    username: User.username,
                    picture: User.picture,
                    status: ChatSrv.status
                });
            }
        });
    });


    return ChatSrv;

});
(function() {
        
   'use strict';

    /*
    * SMARTCHAT PLUGIN ARRAYS & CONFIG
    * Dependency: js/plugin/moment/moment.min.js 
    *             js/plugin/cssemotions/jquery.cssemoticons.min.js 
    *             js/smart-chat-ui/smart.chat.ui.js
    * (DO NOT CHANGE) 
    */ 
        var boxList = [],
        showList = [],
        nameList = [],
        idList = [];
    /*
    * Width of the chat boxes, and the gap inbetween in pixel (minus padding)
    */ 
        var chatbox_config = {
            width: 200,
            gap: 35,
            offset: 0
        };



    /*
    * SMART CHAT ENGINE
    * Copyright (c) 2013 Wen Pu
    * Modified by MyOrange
    * All modifications made are hereby copyright (c) 2014-2015 MyOrange
    */

    // TODO: implement destroy()
    (function($) {
    $.widget("ui.chatbox", {
        options: {
            id: null, //id for the DOM element
            title: null, // title of the chatbox
            user: null, // can be anything associated with this chatbox
            hidden: false,
            offset: 0, // relative to right edge of the browser window
            width: 300, // width of the chatbox
            status: 'online', //
            alertmsg: null,
            alertshow: null,
            messageSent: function(id, user, msg) {
                // override this
                this.boxManager.addMsg(user.first_name, msg);
            },
            boxClosed: function(id) {
            }, // called when the close icon is clicked
            boxManager: {
                // thanks to the widget factory facility
                // similar to http://alexsexton.com/?p=51
                init: function(elem) {
                    this.elem = elem;
                },
                addMsg: function(peer, msg) {
                    var self = this;
                    var box = self.elem.uiChatboxLog;
                    var e = document.createElement('div');
                    box.append(e);
                    $(e).hide();

                    var systemMessage = false;

                    if (peer) {
                        var peerName = document.createElement("b");
                        $(peerName).text(peer + ": ");
                        e.appendChild(peerName);
                    } else {
                        systemMessage = true;
                    }

                    var msgElement = document.createElement(
                        systemMessage ? "i" : "span");
                    $(msgElement).text(msg);
                    e.appendChild(msgElement);
                    $(e).addClass("ui-chatbox-msg");
                    $(e).css("maxWidth", $(box).width());
                    $(e).fadeIn();
                    //$(e).prop( 'title', moment().calendar() ); // add dep: moment.js
                    $(e).find("span").emoticonize(); // add dep: jquery.cssemoticons.js
                    self._scrollToBottom();

                    if (!self.elem.uiChatboxTitlebar.hasClass("ui-state-focus")
                        && !self.highlightLock) {
                        self.highlightLock = true;
                        self.highlightBox();
                    }
                },
                highlightBox: function() {
                    var self = this;
                    self.elem.uiChatboxTitlebar.effect("highlight", {}, 300);
                    self.elem.uiChatbox.effect("bounce", {times: 2}, 300, function() {
                        self.highlightLock = false;
                        self._scrollToBottom();
                    });
                },
                toggleBox: function() {
                    this.elem.uiChatbox.toggle();
                },
                _scrollToBottom: function() {
                    var box = this.elem.uiChatboxLog;
                    box.scrollTop(box.get(0).scrollHeight);
                }
            }
        },
        toggleContent: function(event) {
            this.uiChatboxContent.toggle();
            if (this.uiChatboxContent.is(":visible")) {
                this.uiChatboxInputBox.focus();
            }
        },
        widget: function() {
            return this.uiChatbox
        },
        _create: function() {
            var self = this,
            options = self.options,
            title = options.title || "No Title",
            // chatbox
            uiChatbox = (self.uiChatbox = $('<div></div>'))
                .appendTo(document.body)
                .addClass('ui-widget ' +
                          //'ui-corner-top ' +
                          'ui-chatbox'
                         )
                .attr('outline', 0)
                .focusin(function() {
                    // ui-state-highlight is not really helpful here
                    //self.uiChatbox.removeClass('ui-state-highlight');
                    self.uiChatboxTitlebar.addClass('ui-state-focus');
                })
                .focusout(function() {
                    self.uiChatboxTitlebar.removeClass('ui-state-focus');
                }),
            // titlebar
            uiChatboxTitlebar = (self.uiChatboxTitlebar = $('<div></div>'))
                .addClass('ui-widget-header ' +
                          //'ui-corner-top ' +
                          'ui-chatbox-titlebar ' +
                          self.options.status +
                          ' ui-dialog-header' // take advantage of dialog header style
                         )
                .click(function(event) {
                    self.toggleContent(event);
                })
                .appendTo(uiChatbox),
            uiChatboxTitle = (self.uiChatboxTitle = $('<span></span>'))
                .html(title)
                .appendTo(uiChatboxTitlebar),
            uiChatboxTitlebarClose = (self.uiChatboxTitlebarClose = $('<a href="#" rel="tooltip" data-placement="top" data-original-title="Hide"></a>'))
                .addClass(//'ui-corner-all ' +
                          'ui-chatbox-icon '
                         )
                .attr('role', 'button')
                .hover(function() { uiChatboxTitlebarClose.addClass('ui-state-hover'); },
                       function() { uiChatboxTitlebarClose.removeClass('ui-state-hover'); })
                .click(function(event) {
                    uiChatbox.hide();
                    self.options.boxClosed(self.options.id);
                    return false;
                })
                .appendTo(uiChatboxTitlebar),
            uiChatboxTitlebarCloseText = $('<i></i>')
                .addClass('fa ' +
                          'fa-times')
                .appendTo(uiChatboxTitlebarClose),
            uiChatboxTitlebarMinimize = (self.uiChatboxTitlebarMinimize = $('<a href="#" rel="tooltip" data-placement="top" data-original-title="Minimize"></a>'))
                .addClass(//'ui-corner-all ' +
                          'ui-chatbox-icon'
                         )
                .attr('role', 'button')
                .hover(function() { uiChatboxTitlebarMinimize.addClass('ui-state-hover'); },
                       function() { uiChatboxTitlebarMinimize.removeClass('ui-state-hover'); })
                .click(function(event) {
                    self.toggleContent(event);
                    return false;
                })
                .appendTo(uiChatboxTitlebar),
            uiChatboxTitlebarMinimizeText = $('<i></i>')
                .addClass('fa ' +
                          'fa-minus')
                .appendTo(uiChatboxTitlebarMinimize),
            // content
            uiChatboxContent = (self.uiChatboxContent = $('<div class="'+ self.options.alertshow +'"><span class="alert-msg">'+ self.options.alertmsg + '</span></div>'))
                .addClass('ui-widget-content ' +
                          'ui-chatbox-content '
                         )
                .appendTo(uiChatbox),
            uiChatboxLog = (self.uiChatboxLog = self.element)
                .addClass('ui-widget-content ' +
                          'ui-chatbox-log ' +
                          'custom-scroll'
                         )
                .appendTo(uiChatboxContent),
            uiChatboxInput = (self.uiChatboxInput = $('<div></div>'))
                .addClass('ui-widget-content ' +
                          'ui-chatbox-input'
                         )
                .click(function(event) {
                    // anything?
                })
                .appendTo(uiChatboxContent),
            uiChatboxInputBox = (self.uiChatboxInputBox = $('<textarea></textarea>'))
                .addClass('ui-widget-content ' +
                          'ui-chatbox-input-box '
                         )
                .appendTo(uiChatboxInput)
                .keydown(function(event) {
                    if (event.keyCode && event.keyCode == $.ui.keyCode.ENTER) {
                        var msg = $.trim($(this).val());
                        if (msg.length > 0) {
                            self.options.messageSent(self.options.id, self.options.user, msg);
                        }
                        $(this).val('');
                        return false;
                    }
                })
                .focusin(function() {
                    uiChatboxInputBox.addClass('ui-chatbox-input-focus');
                    var box = $(this).parent().prev();
                    box.scrollTop(box.get(0).scrollHeight);
                })
                .focusout(function() {
                    uiChatboxInputBox.removeClass('ui-chatbox-input-focus');
                });

            // disable selection
            uiChatboxTitlebar.find('*').add(uiChatboxTitlebar).disableSelection();

            // switch focus to input box when whatever clicked
            uiChatboxContent.children().click(function() {
                // click on any children, set focus on input box
                self.uiChatboxInputBox.focus();
            });

            self._setWidth(self.options.width);
            self._position(self.options.offset);

            self.options.boxManager.init(self);

            if (!self.options.hidden) {
                uiChatbox.show();
            }
            
            $(".ui-chatbox [rel=tooltip]").tooltip();
            //console.log("tooltip created");
        },
        _setOption: function(option, value) {
            if (value != null) {
                switch (option) {
                case "hidden":
                    if (value)
                        this.uiChatbox.hide();
                    else
                        this.uiChatbox.show();
                    break;
                case "offset":
                    this._position(value);
                    break;
                case "width":
                    this._setWidth(value);
                    break;
                }
            }
            $.Widget.prototype._setOption.apply(this, arguments);
        },
        _setWidth: function(width) {
            this.uiChatbox.width((width + 28) + "px");
            //this.uiChatboxTitlebar.width((width + 28) + "px");
            //this.uiChatboxLog.width(width + "px");
           // this.uiChatboxInput.css("maxWidth", width + "px");
            // padding:2, boarder:2, margin:5
            this.uiChatboxInputBox.css("width", (width + 18) + "px");
        },
        _position: function(offset) {
            this.uiChatbox.css("right", offset);
        }
    });
    }(jQuery));


    /*
    * jQuery CSSEmoticons plugin 0.2.9
    *
    * Copyright (c) 2010 Steve Schwartz (JangoSteve)
    *
    * Dual licensed under the MIT and GPL licenses:
    *   http://www.opensource.org/licenses/mit-license.php
    *   http://www.gnu.org/licenses/gpl.html
    *
    * Date: Sun Oct 22 1:00:00 2010 -0500
    */
    (function($) {
    $.fn.emoticonize = function(options) {

    var opts = $.extend({}, $.fn.emoticonize.defaults, options);

    var escapeCharacters = [ ")", "(", "*", "[", "]", "{", "}", "|", "^", "<", ">", "\\", "?", "+", "=", "." ];

    var threeCharacterEmoticons = [
        // really weird bug if you have :{ and then have :{) in the same container anywhere *after* :{ then :{ doesn't get matched, e.g. :] :{ :) :{) :) :-) will match everything except :{
        //  But if you take out the :{) or even just move :{ to the right of :{) then everything works fine. This has something to do with the preMatch string below I think, because
        //  it'll work again if you set preMatch equal to '()'
        //  So for now, we'll just remove :{) from the emoticons, because who actually uses this mustache man anyway?
      // ":{)",
      ":-)", ":o)", ":c)", ":^)", ":-D", ":-(", ":-9", ";-)", ":-P", ":-p", ":-Þ", ":-b", ":-O", ":-/", ":-X", ":-#", ":'(", "B-)", "8-)", ";*(", ":-*", ":-\\",
      "?-)", // <== This is my own invention, it's a smiling pirate (with an eye-patch)!
      // and the twoCharacterEmoticons from below, but with a space inserted
      ": )", ": ]", "= ]", "= )", "8 )", ": }", ": D", "8 D", "X D", "x D", "= D", ": (", ": [", ": {", "= (", "; )", "; ]", "; D", ": P", ": p", "= P", "= p", ": b", ": Þ", ": O", "8 O", ": /", "= /", ": S", ": #", ": X", "B )", ": |", ": \\", "= \\", ": *", ": &gt;", ": &lt;"//, "* )"
    ];

    var twoCharacterEmoticons = [ // separate these out so that we can add a letter-spacing between the characters for better proportions
      ":)", ":]", "=]", "=)", "8)", ":}", ":D", ":(", ":[", ":{", "=(", ";)", ";]", ";D", ":P", ":p", "=P", "=p", ":b", ":Þ", ":O", ":/", "=/", ":S", ":#", ":X", "B)", ":|", ":\\", "=\\", ":*", ":&gt;", ":&lt;"//, "*)"
    ];

    var specialEmoticons = { // emoticons to be treated with a special class, hash specifies the additional class to add, along with standard css-emoticon class
      "&gt;:)": { cssClass: "red-emoticon small-emoticon spaced-emoticon" },
      "&gt;;)": { cssClass: "red-emoticon small-emoticon spaced-emoticon"},
      "&gt;:(": { cssClass: "red-emoticon small-emoticon spaced-emoticon" },
      "&gt;: )": { cssClass: "red-emoticon small-emoticon" },
      "&gt;; )": { cssClass: "red-emoticon small-emoticon"},
      "&gt;: (": { cssClass: "red-emoticon small-emoticon" },
      ";(":     { cssClass: "red-emoticon spaced-emoticon" },
      "&lt;3":  { cssClass: "pink-emoticon counter-rotated" },
      "O_O":    { cssClass: "no-rotate" },
      "o_o":    { cssClass: "no-rotate" },
      "0_o":    { cssClass: "no-rotate" },
      "O_o":    { cssClass: "no-rotate" },
      "T_T":    { cssClass: "no-rotate" },
      "^_^":    { cssClass: "no-rotate" },
      "O:)":    { cssClass: "small-emoticon spaced-emoticon" },
      "O: )":   { cssClass: "small-emoticon" },
      "8D":     { cssClass: "small-emoticon spaced-emoticon" },
      "XD":     { cssClass: "small-emoticon spaced-emoticon" },
      "xD":     { cssClass: "small-emoticon spaced-emoticon" },
      "=D":     { cssClass: "small-emoticon spaced-emoticon" },
      "8O":     { cssClass: "small-emoticon spaced-emoticon" },
      "[+=..]":  { cssClass: "no-rotate nintendo-controller" }
      //"OwO":  { cssClass: "no-rotate" }, // these emoticons overflow and look weird even if they're made even smaller, could probably fix this with some more css trickery
      //"O-O":  { cssClass: "no-rotate" },
      //"O=)":    { cssClass: "small-emoticon" } 
    }

    var specialRegex = new RegExp( '(\\' + escapeCharacters.join('|\\') + ')', 'g' );
    // One of these characters must be present before the matched emoticon, or the matched emoticon must be the first character in the container HTML
    //  This is to ensure that the characters in the middle of HTML properties or URLs are not matched as emoticons
    //  Below matches ^ (first character in container HTML), \s (whitespace like space or tab), or \0 (NULL character)
    // (<\\S+.*>) matches <\\S+.*> (matches an HTML tag like <span> or <div>), but haven't quite gotten it working yet, need to push this fix now
    var preMatch = '(^|[\\s\\0])';

    for ( var i=threeCharacterEmoticons.length-1; i>=0; --i ){
      threeCharacterEmoticons[i] = threeCharacterEmoticons[i].replace(specialRegex,'\\$1');
      threeCharacterEmoticons[i] = new RegExp( preMatch+'(' + threeCharacterEmoticons[i] + ')', 'g' );
    }

    for ( var i=twoCharacterEmoticons.length-1; i>=0; --i ){
      twoCharacterEmoticons[i] = twoCharacterEmoticons[i].replace(specialRegex,'\\$1');
      twoCharacterEmoticons[i] = new RegExp( preMatch+'(' + twoCharacterEmoticons[i] + ')', 'g' );
    }

    for ( var emoticon in specialEmoticons ){
      specialEmoticons[emoticon].regexp = emoticon.replace(specialRegex,'\\$1');
      specialEmoticons[emoticon].regexp = new RegExp( preMatch+'(' + specialEmoticons[emoticon].regexp + ')', 'g' );
    }

    var exclude = 'span.css-emoticon';
    if(opts.exclude){ exclude += ','+opts.exclude; }
    var excludeArray = exclude.split(',')

    return this.not(exclude).each(function() {
      var container = $(this);
      var cssClass = 'css-emoticon'
      if(opts.animate){ cssClass += ' un-transformed-emoticon animated-emoticon'; }
      
      for( var emoticon in specialEmoticons ){
        var specialCssClass = cssClass + " " + specialEmoticons[emoticon].cssClass;
        container.html(container.html().replace(specialEmoticons[emoticon].regexp,"$1<span class='" + specialCssClass + "'>$2</span>"));
      }
      $(threeCharacterEmoticons).each(function(){
        container.html(container.html().replace(this,"$1<span class='" + cssClass + "'>$2</span>"));
      });                                                          
      $(twoCharacterEmoticons).each(function(){                    
        container.html(container.html().replace(this,"$1<span class='" + cssClass + " spaced-emoticon'>$2</span>"));
      });
      // fix emoticons that got matched more then once (where one emoticon is a subset of another emoticon), and thus got nested spans
      $.each(excludeArray,function(index,item){
        container.find($.trim(item)+" span.css-emoticon").each(function(){
          $(this).replaceWith($(this).text());
        });
      });
      if(opts.animate){
        setTimeout(function(){$('.un-transformed-emoticon').removeClass('un-transformed-emoticon');}, opts.delay);
      }
    });
    }

    $.fn.unemoticonize = function(options) {
    var opts = $.extend({}, $.fn.emoticonize.defaults, options);
    return this.each(function() {
      var container = $(this);
      container.find('span.css-emoticon').each(function(){
        // add delay equal to animate speed if animate is not false
        var span = $(this);
        if(opts.animate){
          span.addClass('un-transformed-emoticon');
          setTimeout(function(){span.replaceWith(span.text());}, opts.delay); 
        }else{
          span.replaceWith(span.text());
        }
      });
    });
    }

    $.fn.emoticonize.defaults = {animate: true, delay: 500, exclude: 'pre,code,.no-emoticons'}
    })(jQuery);

    var chatboxManager = function () {
        
    var init = function (options) {
        $.extend(chatbox_config, options)
    };


    var delBox = function (id) {
        // TODO
    };

    var getNextOffset = function () {
        return (chatbox_config.width + chatbox_config.gap) * showList.length;
    };

    var boxClosedCallback = function (id) {
        // close button in the titlebar is clicked
        var idx = showList.indexOf(id);
        if (idx != -1) {
            showList.splice(idx, 1);
            var diff = chatbox_config.width + chatbox_config.gap;
            for (var i = idx; i < showList.length; i++) {
                chatbox_config.offset = $("#" + showList[i]).chatbox("option", "offset");
                $("#" + showList[i]).chatbox("option", "offset", chatbox_config.offset - diff);
            }
        } else {
            alert("NOTE: Id missing from array: " + id);
        }
    };

    // caller should guarantee the uniqueness of id
    var addBox = function (id, user, name) {
        var idx1 = showList.indexOf(id);
        var idx2 = boxList.indexOf(id);
        if (idx1 != -1) {
            // found one in show box, do nothing
        } else if (idx2 != -1) {
            // exists, but hidden
            // show it and put it back to showList
            $("#" + id).chatbox("option", "offset", getNextOffset());
            var manager = $("#" + id).chatbox("option", "boxManager");
            manager.toggleBox();
            showList.push(id);
        } else {
            var el = document.createElement('div');
            el.setAttribute('id', id);
            $(el).chatbox({
                id: id,
                user: user,
                title: '<i title="' + user.status + '"></i>' + user.first_name + " " + user.last_name,
                hidden: false,
                offset: getNextOffset(),
                width: chatbox_config.width,
                status: user.status,
                alertmsg: user.alertmsg,
                alertshow: user.alertshow,
                messageSent: dispatch,
                boxClosed: boxClosedCallback
            });
            boxList.push(id);
            showList.push(id);
            nameList.push(user.first_name);
        }
    };

    var messageSentCallback = function (id, user, msg) {
        var idx = boxList.indexOf(id);
        chatbox_config.messageSent(nameList[idx], msg);
    };

    // not used in demo
    var dispatch = function (id, user, msg) {
        //$("#log").append("<i>" + moment().calendar() + "</i> you said to <b>" + user.first_name + " " + user.last_name + ":</b> " + msg + "<br/>");
        if ($('#chatlog').length){
            $("#chatlog").append("You said to <b>" + user.first_name + " " + user.last_name + ":</b> " + msg + "<br/>").effect("highlight", {}, 500);;
        }
        $("#" + id).chatbox("option", "boxManager").addMsg("Me", msg);
    }

    return {
        init: init,
        addBox: addBox,
        delBox: delBox,
        dispatch: dispatch
    };
    }();

    var link = function (scope, element, attributes) {

        $('a[data-chat-id]').click(function (event, ui) {
            if(!$(this).hasClass('offline')){

                var $this = $(this),
                    temp_chat_id = $this.attr("data-chat-id"),
                    fname = $this.attr("data-chat-fname"),
                    lname = $this.attr("data-chat-lname"),
                    status = $this.attr("data-chat-status") || "online",
                    alertmsg = $this.attr("data-chat-alertmsg"),
                    alertshow =  $this.attr("data-chat-alertshow") || false;


                chatboxManager.addBox(temp_chat_id, {
                    // dest:"dest" + counter, 
                    // not used in demo
                    title: "username" + temp_chat_id,
                    first_name: fname,
                    last_name: lname,
                    status: status,
                    alertmsg: alertmsg,
                    alertshow: alertshow
                    //you can add your own options too
                });
            }

            event.preventDefault();

        });

    }

    angular.module('app.chat').directive('asideChatWidget', function (ChatApi) {
        return {
            restrict: 'A',
            replace: true,
            templateUrl: 'app/dashboard/chat/directives/aside-chat-widget.tpl.html',
            link: link
        }
    });

})(); 
"use strict";

angular.module('app.chat').directive('chatUsers', function(ChatApi){
    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/dashboard/chat/directives/chat-users.tpl.html',
        scope: true,
        link: function(scope, element){
            scope.open = false;
            scope.openToggle = function(){
                scope.open = !scope.open;
            };

            scope.chatUserFilter = '';

            ChatApi.initialized.then(function () {
                scope.chatUsers = ChatApi.users;
            });
        }
    }
});

"use strict";

angular.module('app.chat').directive('chatWidget', function (ChatApi) {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: 'app/dashboard/chat/directives/chat-widget.tpl.html',
        scope: {},
        link: function (scope, element) {
            scope.newMessage = '';

            scope.sendMessage = function () {
                ChatApi.sendMessage(scope.newMessage);
                scope.newMessage = '';
            };

            scope.messageTo = function(user){
                scope.newMessage += (user.username + ', ');
            };

            ChatApi.initialized.then(function () {
                scope.chatMessages = ChatApi.messages;
            });
            scope.$watch(function () {
                return ChatApi.messages.length
            }, function (count) {
                if (count){
                    var $body = $('.chat-body', element);
                    $body.animate({scrollTop: $body[0].scrollHeight});
                }
            })
        }
    }
});
"use strict";

 angular.module('app').directive('todoList', function ($timeout, Todo) {

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/dashboard/todo/directives/todo-list.tpl.html',
        scope: {
            todos: '='
        },
        link: function (scope, element, attributes) {
            scope.title = attributes.title
            scope.icon = attributes.icon
            scope.state = attributes.state
            scope.filter = {
                state: scope.state
            }

            element.find('.todo').sortable({
                handle: '.handle',
                connectWith: ".todo",
                receive: function (event, ui) {

                    console.log(ui.item.scope().todo,scope.state)
                    var todo = ui.item.scope().todo;
                    var state = scope.state
                    // // console.log(ui.item, todo, state)
                    // // console.log(state, todo)
                    if (todo && state) {
                        todo.setState(state);
                         // ui.sender.sortable("cancel");
                        // scope.$apply();
                    } else {
                        console.log('Wat', todo, state);
                    }
                    
                }
            }).disableSelection();

        }
    }
});
"use strict";

angular.module('app').factory('Todo', function (Restangular, APP_CONFIG, $httpBackend) {

    var normalize = function(todo) {
        if(!todo._id){
            todo._id = _.unique('todo')
        }
        todo.toggle = function(){
            if (!todo.completedAt) {
                todo.state = 'Completed';
                todo.completedAt = JSON.stringify(new Date());
            } else {
                todo.state = 'Critical';
                todo.completedAt = null;
            }
        };

        todo.setState = function(state){
            todo.state = state;
            if (state == 'Completed') {
                todo.completedAt = JSON.stringify(new Date());
            } else {
                todo.completedAt = null;
            }
        };

        return todo;
    };

    var Todo = Restangular.all(APP_CONFIG.apiRootUrl + '/todos.json');

    Restangular.extendModel(APP_CONFIG.apiRootUrl + '/todos.json', normalize);
    Todo.normalize = normalize;

    return Todo
});
angular.module('app.proyectos')
    .controller('SeccionAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.section = {
                sectionId: null,
                name: '',
                code: '',
                active: '',
                project: {
                    projectId: null
                }
            };

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('project.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    $scope.section.active = 'true';
                    $scope.section.project.projectId = $stateParams.proyecto;
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('project.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('project.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'sectionGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;

                        $scope.section.sectionId = data.sectionId;
                        $scope.section.name = data.name;
                        $scope.section.code = data.code;
                        $scope.section.active = '' + data.active;
                        $scope.section.project.projectId = data.project.projectId;

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            $scope.cancel = function () {
                $state.go('app.proyecto.seccion', {proyecto: $scope.section.project.projectId});
                return false;
            };

            // Guardar informacion, crea o edita un usuario con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                } else if (!!$scope.section.name && !!$scope.section.code) {
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('project.add')) {
                                $scope.section.active = ($scope.section.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.section, 'sectionPOST').then(function () {
                                    $.bigBox({
                                        title: "Nueva sección agregada",
                                        content: "Se guardo la información de la sección: <b>" + $scope.section.name + "</b> - <i>" + $scope.section.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información de la sección';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('project.edit')) {
                                $scope.section.active = ($scope.section.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.section, 'sectionPUT', '/' + $scope.section.sectionId).then(function () {
                                    $.bigBox({
                                        title: "Sección actualizada",
                                        content: "Se actualizo la información de la sección: <b>" + $scope.section.name + "</b> - <i>" + $scope.section.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información de la sección';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);

angular.module('app.proyectos')
    .controller('SeccionCtrl', ['$scope', '$compile', '$state', 'DTOptionsBuilder', 'DTColumnBuilder', 'permissionsService', 'restService', '$stateParams',
        function ($scope, $compile, $state, DTOptionsBuilder, DTColumnBuilder, permissionsService, restService, $stateParams) {
            $scope.pageTitle = $state.current.data.title;
            $scope.projectId = $stateParams.proyecto;

            /* Obtener informacion para seleccionar proyecto */
            function loadProjects() {
                restService.sendRequest(null, 'projectGET').then(function (data) {
                    $scope.listProjects = data.data;
                }, function () {
                    $.bigBox({
                        title: "Error al cargar",
                        content: "No se pudo cargar la lista de proyectos",
                        color: "#C46A69",
                        icon: "fa fa-warning shake animated",
                        timeout: 6000
                    });
                    $state.go('app.proyecto');
                });
            }

            loadProjects();
            /* ~ */

            $scope.updateData = function() {
                if (!!$scope.projectId)
                    $state.go('app.proyecto.seccion', {proyecto: $scope.projectId});
            };

            $scope.statusFilter = {
                'true': 'Activo',
                'false': 'Inactivo'
            };

            /* Datatable (DT), creacion y asignacion de eventos */
            var dataArrayResolved = [];

            $scope.standardOptions = DTOptionsBuilder
                .fromFnPromise(restService.sendRequest(null, 'sectionGET', '?projectId=' + ($scope.projectId || 0)))
                .withDataProp('data')
                .withDOM("t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap()
                .withOption('createdRow', createdRow);
            $scope.standardColumns = [
                DTColumnBuilder.newColumn('name').notSortable().renderWith(function (row, type, data) {
                    return '<a ui-sref="app.proyecto.seccion.poligono({seccion: ' + data.sectionId + '})">' + row + '</a>';
                }),
                DTColumnBuilder.newColumn('code').notSortable(),
                DTColumnBuilder.newColumn('active').notSortable().renderWith(function (data) {
                    return $scope.statusFilter[data];
                }),
                DTColumnBuilder.newColumn(null).notSortable().withClass('table-actions-buttons').renderWith(function (data) {
                    dataArrayResolved[data.sectionId] = data;
                    return '<a has-permission="PROJECT_VIEW" title="Ver" ui-sref="app.proyecto.seccion.accion({accion: \'ver\', id: ' + data.sectionId + '})" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>' +
                        '<a has-permission="PROJECT_EDIT" title="Editar" ui-sref="app.proyecto.seccion.accion({accion: \'editar\', id: ' + data.sectionId + '})" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>' +
                        '<a has-permission="PROJECT_EDIT" title="' + (data.active ? 'Desactivar' : 'Activar') + '" ng-click="accion(\'' + (data.active ? 'deactivate' : 'activate') + '\', ' + data.sectionId + ')" class="btn btn-default btn-xs"><i class="fa ' + (data.active ? 'fa-ban' : 'fa-check-circle-o') + '"></i></a>' +
                        '<a has-permission="PROJECT_DELETE" title="Eliminar" ng-click="accion(\'delete\', ' + data.sectionId + ')" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }

            // Callback para funcionalidad de filtros
            $scope.instanceCallback = function (dtInstance) {
                $scope.dtInstance = dtInstance;
                var table = dtInstance.DataTable;
                table.columns().eq(0).each(function (colIdx) {
                    $('input, select', table.column(colIdx).header()).on('keyup change', function () {
                        if (this.getAttribute('id') == 'table-lenght-ctrl') {
                            table
                                .page.len(this.value)
                                .draw();
                        }
                        else {
                            var isSelect = (this.nodeName == 'SELECT');
                            var searchValue = (isSelect && this.value !== '') ? '\\b' + this.value + '\\b' : this.value;
                            table
                                .column(colIdx)
                                .search(searchValue, isSelect, !isSelect)
                                .draw();
                        }
                    });
                });
            };

            $scope.accion = function (accion, id) {
                switch (accion) {
                    case 'delete':
                    case 'eliminar':
                        if (permissionsService.canAccess('project.delete')) {
                            $.SmartMessageBox({
                                title: "Borrar Sección",
                                content: "Esta a punto de borrar la información de la sección <b>" + dataArrayResolved[id].name + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'sectionDELETE', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se borro la información de la sección <b>" + dataArrayResolved[id].name + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'sectionGET', '?projectId=' + ($scope.projectId || 0)));
                                        else
                                            $state.go('app.proyecto.seccion', {proyecto: $scope.projectId}, {reload: true});
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo borrar la sección solicitada...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                    case 'activate':
                    case 'deactivate':
                        if (permissionsService.canAccess('project.edit')) {
                            var active = (accion.toLowerCase() === 'activate');
                            $.SmartMessageBox({
                                title: (active ? 'Activar' : 'Desactivar') + " Proyecto",
                                content: "Esta a punto de " + (active ? 'activar' : 'desactivar') + " la sección <b>" + dataArrayResolved[id].name + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    dataArrayResolved[id].active = active;
                                    restService.sendRequest(dataArrayResolved[id], 'sectionPUT', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se " + (active ? 'activo' : 'desactivo') + " la sección <b>" + dataArrayResolved[id].name + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'sectionGET', '?projectId=' + ($scope.projectId || 0)));
                                        else
                                            $state.go('app.proyecto.seccion', {proyecto: $scope.projectId}, {reload: true});
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo " + (active ? 'activar' : 'desactivar') + " la sección solicitada...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                }
            };
        }
    ]);
"use strict";


angular.module('SmartAdmin.Forms').directive('bootstrapAttributeForm', function(){

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_common/forms/directives/bootstrap-validation/bootstrap-attribute-form.tpl.html',
        link: function(scope, form){
            form.bootstrapValidator();


        }

    }
});
"use strict";


angular.module('SmartAdmin.Forms').directive('bootstrapButtonGroupForm', function(){

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_common/forms/directives/bootstrap-validation/bootstrap-button-group-form.tpl.html',
        link: function(scope, form){
            form.bootstrapValidator({
                excluded : ':disabled',
                feedbackIcons : {
                    valid : 'glyphicon glyphicon-ok',
                    invalid : 'glyphicon glyphicon-remove',
                    validating : 'glyphicon glyphicon-refresh'
                },
                fields : {
                    gender : {
                        validators : {
                            notEmpty : {
                                message : 'The gender is required'
                            }
                        }
                    },
                    'languages[]' : {
                        validators : {
                            choice : {
                                min : 1,
                                max : 2,
                                message : 'Please choose 1 - 2 languages you can speak'
                            }
                        }
                    }
                }
            });


        }

    }
});
"use strict";


angular.module('SmartAdmin.Forms').directive('bootstrapContactForm', function(){

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_common/forms/directives/bootstrap-validation/bootstrap-contact-form.tpl.html',
        link: function(scope, form){
            form.bootstrapValidator({
                container : '#messages',
                feedbackIcons : {
                    valid : 'glyphicon glyphicon-ok',
                    invalid : 'glyphicon glyphicon-remove',
                    validating : 'glyphicon glyphicon-refresh'
                },
                fields : {
                    fullName : {
                        validators : {
                            notEmpty : {
                                message : 'The full name is required and cannot be empty'
                            }
                        }
                    },
                    email : {
                        validators : {
                            notEmpty : {
                                message : 'The email address is required and cannot be empty'
                            },
                            emailAddress : {
                                message : 'The email address is not valid'
                            }
                        }
                    },
                    title : {
                        validators : {
                            notEmpty : {
                                message : 'The title is required and cannot be empty'
                            },
                            stringLength : {
                                max : 100,
                                message : 'The title must be less than 100 characters long'
                            }
                        }
                    },
                    content : {
                        validators : {
                            notEmpty : {
                                message : 'The content is required and cannot be empty'
                            },
                            stringLength : {
                                max : 500,
                                message : 'The content must be less than 500 characters long'
                            }
                        }
                    }
                }
            });

        }

    }

});
"use strict";


angular.module('SmartAdmin.Forms').directive('bootstrapMovieForm', function(){

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_common/forms/directives/bootstrap-validation/bootstrap-movie-form.tpl.html',
        link: function(scope, form){
            form.bootstrapValidator({
                feedbackIcons : {
                    valid : 'glyphicon glyphicon-ok',
                    invalid : 'glyphicon glyphicon-remove',
                    validating : 'glyphicon glyphicon-refresh'
                },
                fields : {
                    title : {
                        group : '.col-md-8',
                        validators : {
                            notEmpty : {
                                message : 'The title is required'
                            },
                            stringLength : {
                                max : 200,
                                message : 'The title must be less than 200 characters long'
                            }
                        }
                    },
                    genre : {
                        group : '.col-md-4',
                        validators : {
                            notEmpty : {
                                message : 'The genre is required'
                            }
                        }
                    },
                    director : {
                        group : '.col-md-4',
                        validators : {
                            notEmpty : {
                                message : 'The director name is required'
                            },
                            stringLength : {
                                max : 80,
                                message : 'The director name must be less than 80 characters long'
                            }
                        }
                    },
                    writer : {
                        group : '.col-md-4',
                        validators : {
                            notEmpty : {
                                message : 'The writer name is required'
                            },
                            stringLength : {
                                max : 80,
                                message : 'The writer name must be less than 80 characters long'
                            }
                        }
                    },
                    producer : {
                        group : '.col-md-4',
                        validators : {
                            notEmpty : {
                                message : 'The producer name is required'
                            },
                            stringLength : {
                                max : 80,
                                message : 'The producer name must be less than 80 characters long'
                            }
                        }
                    },
                    website : {
                        group : '.col-md-6',
                        validators : {
                            notEmpty : {
                                message : 'The website address is required'
                            },
                            uri : {
                                message : 'The website address is not valid'
                            }
                        }
                    },
                    trailer : {
                        group : '.col-md-6',
                        validators : {
                            notEmpty : {
                                message : 'The trailer link is required'
                            },
                            uri : {
                                message : 'The trailer link is not valid'
                            }
                        }
                    },
                    review : {
                        // The group will be set as default (.form-group)
                        validators : {
                            stringLength : {
                                max : 500,
                                message : 'The review must be less than 500 characters long'
                            }
                        }
                    },
                    rating : {
                        // The group will be set as default (.form-group)
                        validators : {
                            notEmpty : {
                                message : 'The rating is required'
                            }
                        }
                    }
                }
            });

        }

    }

});
"use strict";


angular.module('SmartAdmin.Forms').directive('bootstrapProductForm', function(){

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_common/forms/directives/bootstrap-validation/bootstrap-product-form.tpl.html',
        link: function(scope, form){
            form.bootstrapValidator({
                feedbackIcons : {
                    valid : 'glyphicon glyphicon-ok',
                    invalid : 'glyphicon glyphicon-remove',
                    validating : 'glyphicon glyphicon-refresh'
                },
                fields : {
                    price : {
                        validators : {
                            notEmpty : {
                                message : 'The price is required'
                            },
                            numeric : {
                                message : 'The price must be a number'
                            }
                        }
                    },
                    amount : {
                        validators : {
                            notEmpty : {
                                message : 'The amount is required'
                            },
                            numeric : {
                                message : 'The amount must be a number'
                            }
                        }
                    },
                    color : {
                        validators : {
                            notEmpty : {
                                message : 'The color is required'
                            }
                        }
                    },
                    size : {
                        validators : {
                            notEmpty : {
                                message : 'The size is required'
                            }
                        }
                    }
                }
            });
        }

    }
});
"use strict";


angular.module('SmartAdmin.Forms').directive('bootstrapProfileForm', function(){

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_common/forms/directives/bootstrap-validation/bootstrap-profile-form.tpl.html',
        link: function(scope, form){
           form.bootstrapValidator({
                feedbackIcons : {
                    valid : 'glyphicon glyphicon-ok',
                    invalid : 'glyphicon glyphicon-remove',
                    validating : 'glyphicon glyphicon-refresh'
                },
                fields : {
                    email : {
                        validators : {
                            notEmpty : {
                                message : 'The email address is required'
                            },
                            emailAddress : {
                                message : 'The email address is not valid'
                            }
                        }
                    },
                    password : {
                        validators : {
                            notEmpty : {
                                message : 'The password is required'
                            }
                        }
                    }
                }
            });
        }

    }

});
"use strict";


angular.module('SmartAdmin.Forms').directive('bootstrapTogglingForm', function(){

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/_common/forms/directives/bootstrap-validation/bootstrap-toggling-form.tpl.html',
        link: function(scope, form){
            form.bootstrapValidator({
                feedbackIcons : {
                    valid : 'glyphicon glyphicon-ok',
                    invalid : 'glyphicon glyphicon-remove',
                    validating : 'glyphicon glyphicon-refresh'
                },
                fields : {
                    firstName : {
                        validators : {
                            notEmpty : {
                                message : 'The first name is required'
                            }
                        }
                    },
                    lastName : {
                        validators : {
                            notEmpty : {
                                message : 'The last name is required'
                            }
                        }
                    },
                    company : {
                        validators : {
                            notEmpty : {
                                message : 'The company name is required'
                            }
                        }
                    },
                    // These fields will be validated when being visible
                    job : {
                        validators : {
                            notEmpty : {
                                message : 'The job title is required'
                            }
                        }
                    },
                    department : {
                        validators : {
                            notEmpty : {
                                message : 'The department name is required'
                            }
                        }
                    },
                    mobilePhone : {
                        validators : {
                            notEmpty : {
                                message : 'The mobile phone number is required'
                            },
                            digits : {
                                message : 'The mobile phone number is not valid'
                            }
                        }
                    },
                    // These fields will be validated when being visible
                    homePhone : {
                        validators : {
                            digits : {
                                message : 'The home phone number is not valid'
                            }
                        }
                    },
                    officePhone : {
                        validators : {
                            digits : {
                                message : 'The office phone number is not valid'
                            }
                        }
                    }
                }
            }).find('button[data-toggle]').on('click', function() {
                var $target = $($(this).attr('data-toggle'));
                // Show or hide the additional fields
                // They will or will not be validated based on their visibilities
                $target.toggle();
                if (!$target.is(':visible')) {
                    // Enable the submit buttons in case additional fields are not valid
                    form.data('bootstrapValidator').disableSubmitButtons(false);
                }
            });
        }

    }



});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartCkEditor', function () {
    return {
        restrict: 'A',
        compile: function ( tElement) {
            tElement.removeAttr('smart-ck-editor data-smart-ck-editor');
            //CKEDITOR.basePath = 'bower_components/ckeditor/';

            CKEDITOR.replace( tElement.attr('name'), { height: '380px', startupFocus : true} );
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartDestroySummernote', function () {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-destroy-summernote data-smart-destroy-summernote')
            tElement.on('click', function() {
                angular.element(tAttributes.smartDestroySummernote).destroy();
            })
        }
    }
});

'use strict';

angular.module('SmartAdmin.Forms').directive('smartEditSummernote', function () {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-edit-summernote data-smart-edit-summernote');
            tElement.on('click', function(){
                angular.element(tAttributes.smartEditSummernote).summernote({
                    focus : true
                });  
            });
        }
    }
});

'use strict';

angular.module('SmartAdmin.Forms').directive('smartMarkdownEditor', function () {
    return {
        restrict: 'A',
        compile: function (element, attributes) {
            element.removeAttr('smart-markdown-editor data-smart-markdown-editor')

            var options = {
                autofocus:false,
                savable:true,
                fullscreen: {
                    enable: false
                }
            };

            if(attributes.height){
                options.height = parseInt(attributes.height);
            }

            element.markdown(options);
        }
    }
});

'use strict';

angular.module('SmartAdmin.Forms').directive('smartSummernoteEditor', function (lazyScript) {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-summernote-editor data-smart-summernote-editor');

            var options = {
                focus : true,
                tabsize : 2
            };

            if(tAttributes.height){
                options.height = tAttributes.height;
            }

            lazyScript.register('build/vendor.ui.js').then(function(){
                tElement.summernote(options);                
            });
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartCheckoutForm', function (formsCommon, lazyScript) {
    return {
        restrict: 'A',
        link: function (scope, form) {
           lazyScript.register('build/vendor.ui.js').then(function(){

               scope.countries = formsCommon.countries;

               form.validate(angular.extend({
                    // Rules for form validation
                    rules : {
                        fname : {
                            required : true
                        },
                        lname : {
                            required : true
                        },
                        email : {
                            required : true,
                            email : true
                        },
                        phone : {
                            required : true
                        },
                        country : {
                            required : true
                        },
                        city : {
                            required : true
                        },
                        code : {
                            required : true,
                            digits : true
                        },
                        address : {
                            required : true
                        },
                        name : {
                            required : true
                        },
                        card : {
                            required : true,
                            creditcard : true
                        },
                        cvv : {
                            required : true,
                            digits : true
                        },
                        month : {
                            required : true
                        },
                        year : {
                            required : true,
                            digits : true
                        }
                    },

                    // Messages for form validation
                    messages : {
                        fname : {
                            required : 'Please enter your first name'
                        },
                        lname : {
                            required : 'Please enter your last name'
                        },
                        email : {
                            required : 'Please enter your email address',
                            email : 'Please enter a VALID email address'
                        },
                        phone : {
                            required : 'Please enter your phone number'
                        },
                        country : {
                            required : 'Please select your country'
                        },
                        city : {
                            required : 'Please enter your city'
                        },
                        code : {
                            required : 'Please enter code',
                            digits : 'Digits only please'
                        },
                        address : {
                            required : 'Please enter your full address'
                        },
                        name : {
                            required : 'Please enter name on your card'
                        },
                        card : {
                            required : 'Please enter your card number'
                        },
                        cvv : {
                            required : 'Enter CVV2',
                            digits : 'Digits only'
                        },
                        month : {
                            required : 'Select month'
                        },
                        year : {
                            required : 'Enter year',
                            digits : 'Digits only please'
                        }
                    }
                }, formsCommon.validateOptions));
            });
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartCommentForm', function (formsCommon, lazyScript) {
    return {
        restrict: 'A',
        link: function (scope, form) {
            lazyScript.register('build/vendor.ui.js').then(function(){
                form.validate(angular.extend({
                    // Rules for form validation
                    rules : {
                        name : {
                            required : true
                        },
                        email : {
                            required : true,
                            email : true
                        },
                        url : {
                            url : true
                        },
                        comment : {
                            required : true
                        }
                    },

                    // Messages for form validation
                    messages : {
                        name : {
                            required : 'Enter your name',
                        },
                        email : {
                            required : 'Enter your email address',
                            email : 'Enter a VALID email'
                        },
                        url : {
                            email : 'Enter a VALID url'
                        },
                        comment : {
                            required : 'Please enter your comment'
                        }
                    },

                    // Ajax form submition
                    submitHandler : function() {
                        form.ajaxSubmit({
                            success : function() {
                                form.addClass('submited');
                            }
                        });
                    }

                }, formsCommon.validateOptions));
            });

        }
    }
});

'use strict';

angular.module('SmartAdmin.Forms').directive('smartContactsForm', function (formsCommon, lazyScript) {
    return {
        restrict: 'A',
        link: function (scope, form) {
            lazyScript.register('build/vendor.ui.js').then(function(){
                form.validate(angular.extend({
                    // Rules for form validation
                    rules : {
                        name : {
                            required : true
                        },
                        email : {
                            required : true,
                            email : true
                        },
                        message : {
                            required : true,
                            minlength : 10
                        }
                    },

                    // Messages for form validation
                    messages : {
                        name : {
                            required : 'Please enter your name'
                        },
                        email : {
                            required : 'Please enter your email address',
                            email : 'Please enter a VALID email address'
                        },
                        message : {
                            required : 'Please enter your message'
                        }
                    },

                    // Ajax form submition
                    submitHandler : function() {
                        form.ajaxSubmit({
                            success : function() {
                                form.addClass('submited');
                            }
                        });
                    }
                }, formsCommon.validateOptions));
            });
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartOrderForm', function (formsCommon, lazyScript) {
    return {
        restrict: 'E',
        link: function (scope, form) {
            lazyScript.register('build/vendor.ui.js').then(function(){
                form.validate(angular.extend({
                    // Rules for form validation
                    rules : {
                        name : {
                            required : true
                        },
                        email : {
                            required : true,
                            email : true
                        },
                        phone : {
                            required : true
                        },
                        interested : {
                            required : true
                        },
                        budget : {
                            required : true
                        }
                    },

                    // Messages for form validation
                    messages : {
                        name : {
                            required : 'Please enter your name'
                        },
                        email : {
                            required : 'Please enter your email address',
                            email : 'Please enter a VALID email address'
                        },
                        phone : {
                            required : 'Please enter your phone number'
                        },
                        interested : {
                            required : 'Please select interested service'
                        },
                        budget : {
                            required : 'Please select your budget'
                        }
                    },

                }, formsCommon.validateOptions));
            });

        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartRegistrationForm', function (formsCommon, lazyScript) {
    return {
        restrict: 'A',
        link: function (scope, form, attributes) {
            lazyScript.register('build/vendor.ui.js').then(function(){
                form.validate(angular.extend({

                    // Rules for form validation
                    rules: {
                        username: {
                            required: true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        password: {
                            required: true,
                            minlength: 3,
                            maxlength: 20
                        },
                        passwordConfirm: {
                            required: true,
                            minlength: 3,
                            maxlength: 20,
                            equalTo: '#password'
                        },
                        firstname: {
                            required: true
                        },
                        lastname: {
                            required: true
                        },
                        gender: {
                            required: true
                        },
                        terms: {
                            required: true
                        }
                    },

                    // Messages for form validation
                    messages: {
                        login: {
                            required: 'Please enter your login'
                        },
                        email: {
                            required: 'Please enter your email address',
                            email: 'Please enter a VALID email address'
                        },
                        password: {
                            required: 'Please enter your password'
                        },
                        passwordConfirm: {
                            required: 'Please enter your password one more time',
                            equalTo: 'Please enter the same password as above'
                        },
                        firstname: {
                            required: 'Please select your first name'
                        },
                        lastname: {
                            required: 'Please select your last name'
                        },
                        gender: {
                            required: 'Please select your gender'
                        },
                        terms: {
                            required: 'You must agree with Terms and Conditions'
                        }
                    }

                }, formsCommon.validateOptions));
            });
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartReviewForm', function (formsCommon, lazyScript) {
    return {
        restrict: 'E',
        link: function (scope, form) {
            lazyScript.register('build/vendor.ui.js').then(function(){

                form.validate(angular.extend({
                    // Rules for form validation
                    rules : {
                        name : {
                            required : true
                        },
                        email : {
                            required : true,
                            email : true
                        },
                        review : {
                            required : true,
                            minlength : 20
                        },
                        quality : {
                            required : true
                        },
                        reliability : {
                            required : true
                        },
                        overall : {
                            required : true
                        }
                    },

                    // Messages for form validation
                    messages : {
                        name : {
                            required : 'Please enter your name'
                        },
                        email : {
                            required : 'Please enter your email address',
                            email : '<i class="fa fa-warning"></i><strong>Please enter a VALID email addres</strong>'
                        },
                        review : {
                            required : 'Please enter your review'
                        },
                        quality : {
                            required : 'Please rate quality of the product'
                        },
                        reliability : {
                            required : 'Please rate reliability of the product'
                        },
                        overall : {
                            required : 'Please rate the product'
                        }
                    }

                }, formsCommon.validateOptions));
            });
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartJcrop', function ($q) {
    return {
        restrict: 'A',
        scope: {
            coords: '=',
            options: '=',
            selection: '='
        },
        link: function (scope, element, attributes) {
            var jcropApi, imageWidth, imageHeight, imageLoaded = $q.defer();

            var listeners = {
                onSelectHandlers: [],
                onChangeHandlers: [],
                onSelect: function (c) {
                    angular.forEach(listeners.onSelectHandlers, function (handler) {
                        handler.call(jcropApi, c)
                    })
                },
                onChange: function (c) {
                    angular.forEach(listeners.onChangeHandlers, function (handler) {
                        handler.call(jcropApi, c)
                    })
                }
            };

            if (attributes.coords) {
                var coordsUpdate = function (c) {
                    scope.$apply(function () {
                        scope.coords = c;
                    });
                };
                listeners.onSelectHandlers.push(coordsUpdate);
                listeners.onChangeHandlers.push(coordsUpdate);
            }

            var $previewPane = $(attributes.smartJcropPreview),
                $previewContainer = $previewPane.find('.preview-container'),
                $previewImg = $previewPane.find('img');

            if ($previewPane.length && $previewImg.length) {
                var previewUpdate = function (coords) {
                    if (parseInt(coords.w) > 0) {
                        var rx = $previewContainer.width() / coords.w;
                        var ry = $previewContainer.height() / coords.h;

                        $previewImg.css({
                            width: Math.round(rx * imageWidth) + 'px',
                            height: Math.round(ry * imageHeight) + 'px',
                            marginLeft: '-' + Math.round(rx * coords.x) + 'px',
                            marginTop: '-' + Math.round(ry * coords.y) + 'px'
                        });
                    }
                };
                listeners.onSelectHandlers.push(previewUpdate);
                listeners.onChangeHandlers.push(previewUpdate);
            }


            var options = {
                onSelect: listeners.onSelect,
                onChange: listeners.onChange
            };

            if ($previewContainer.length) {
                options.aspectRatio = $previewContainer.width() / $previewContainer.height()
            }

            if (attributes.selection) {
                scope.$watch('selection', function (newVal, oldVal) {
                    if (newVal != oldVal) {
                        var rectangle = newVal == 'release' ? [imageWidth / 2, imageHeight / 2, imageWidth / 2, imageHeight / 2] : newVal;

                        var callback = newVal == 'release' ? function () {
                            jcropApi.release();
                        } : angular.noop;

                        imageLoaded.promise.then(function () {
                            if (scope.options && scope.options.animate) {
                                jcropApi.animateTo(rectangle, callback);
                            } else {
                                jcropApi.setSelect(rectangle);
                            }
                        });
                    }
                });
            }

            if (attributes.options) {

                var optionNames = [
                    'bgOpacity', 'bgColor', 'bgFade', 'shade', 'outerImage',
                    'allowSelect', 'allowMove', 'allowResize',
                    'aspectRatio'
                ];

                angular.forEach(optionNames, function (name) {
                    if (scope.options[name])
                        options[name] = scope.options[name]

                    scope.$watch('options.' + name, function (newVal, oldVal) {
                        if (newVal != oldVal) {
                            imageLoaded.promise.then(function () {
                                var update = {};
                                update[name] = newVal;
                                jcropApi.setOptions(update);
                            });
                        }
                    });

                });


                scope.$watch('options.disabled', function (newVal, oldVal) {
                    if (newVal != oldVal) {
                        if (newVal) {
                            jcropApi.disable();
                        } else {
                            jcropApi.enable();
                        }
                    }
                });

                scope.$watch('options.destroyed', function (newVal, oldVal) {
                    if (newVal != oldVal) {
                        if (newVal) {
                            jcropApi.destroy();
                        } else {
                            _init();
                        }
                    }
                });

                scope.$watch('options.src', function (newVal, oldVal) {
                    imageLoaded = $q.defer();
                    if (newVal != oldVal) {
                        jcropApi.setImage(scope.options.src, function () {
                            imageLoaded.resolve();
                        });
                    }
                });

                var updateSize = function(){
                    jcropApi.setOptions({
                        minSize: [scope.options.minSizeWidth, scope.options.minSizeHeight],
                        maxSize: [scope.options.maxSizeWidth, scope.options.maxSizeHeight]
                    });
                };

                scope.$watch('options.minSizeWidth', function (newVal, oldVal) {
                    if (newVal != oldVal) updateSize();
                });
                scope.$watch('options.minSizeHeight', function (newVal, oldVal) {
                    if (newVal != oldVal) updateSize();
                });
                scope.$watch('options.maxSizeWidth', function (newVal, oldVal) {
                    if (newVal != oldVal) updateSize();
                });
                scope.$watch('options.maxSizeHeight', function (newVal, oldVal) {
                    if (newVal != oldVal) updateSize();
                });
            }

            var _init = function () {
                element.Jcrop(options, function () {
                    jcropApi = this;
                    // Use the API to get the real image size
                    var bounds = this.getBounds();
                    imageWidth = bounds[0];
                    imageHeight = bounds[1];

                    if (attributes.selection && angular.isArray(scope.selection)) {
                        if (scope.options && scope.options.animate) {
                            jcropApi.animateTo(scope.selection);
                        } else {
                            jcropApi.setSelect(scope.selection);
                        }
                    }
                    imageLoaded.resolve();
                });
            };

            _init()


        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartClockpicker', function () {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-clockpicker data-smart-clockpicker');

            var options = {
                placement: 'bottom',
                donetext: 'Done'
            }

            tElement.clockpicker(options);
        }
    }
});

'use strict';

angular.module('SmartAdmin.Forms').directive('smartColorpicker', function () {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-colorpicker data-smart-colorpicker');


            var aOptions = _.pick(tAttributes, ['']);

            var options = _.extend(aOptions, {});

            tElement.colorpicker(options);
        }
    }
});
"use strict";

angular.module('SmartAdmin.Forms').directive('smartDatepicker', function () {
    return {
        restrict: 'A',
        scope: {
            options: '='
        },
        link: function (scope, element, attributes) {

            var onSelectCallbacks = [];
            if (attributes.minRestrict) {
                onSelectCallbacks.push(function (selectedDate) {
                    $(attributes.minRestrict).datepicker('option', 'minDate', selectedDate);
                });
            }
            if (attributes.maxRestrict) {
                onSelectCallbacks.push(function (selectedDate) {
                    $(attributes.maxRestrict).datepicker('option', 'maxDate', selectedDate);
                });
            }

            //Let others know about changes to the data field
            onSelectCallbacks.push(function (selectedDate) {
                //CVB - 07/14/2015 - Update the scope with the selected value
                element.triggerHandler("change");

                //CVB - 07/17/2015 - Update Bootstrap Validator
                var form = element.closest('form');

                if(typeof form.bootstrapValidator == 'function')
                    form.bootstrapValidator('revalidateField', element.attr('name'));
            });

            var options = _.extend({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                onSelect: function (selectedDate) {
                    angular.forEach(onSelectCallbacks, function (callback) {
                        callback.call(this, selectedDate)
                    })
                }
            }, scope.options || {});


            if (attributes.numberOfMonths) options.numberOfMonths = parseInt(attributes.numberOfMonths);

            if (attributes.dateFormat) options.dateFormat = attributes.dateFormat;

            if (attributes.defaultDate) options.defaultDate = attributes.defaultDate;

            if (attributes.changeMonth) options.changeMonth = attributes.changeMonth == "true";


            element.datepicker(options)
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartDuallistbox', function () {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-duallistbox data-smart-duallistbox');


            var aOptions = _.pick(tAttributes, ['nonSelectedFilter']);

            var options = _.extend(aOptions, {
                nonSelectedListLabel: 'Non-selected',
                selectedListLabel: 'Selected',
                preserveSelectionOnMove: 'moved',
                moveOnSelect: false
            });

            tElement.bootstrapDualListbox(options);
        }
    }
});

'use strict';

angular.module('SmartAdmin.Forms').directive('smartIonslider', function (lazyScript) {
    return {
        restrict: 'A',
        compile: function (element, attributes) {
            element.removeAttr('smart-ionslider data-smart-ionslider');

        	lazyScript.register('build/vendor.ui.js').then(function(){
            	element.ionRangeSlider();
        	});
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartKnob', function () {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-knob data-smart-knob');

            tElement.knob();
        }
    }
});
"use strict";

angular.module('SmartAdmin.Forms').directive('smartMaskedInput', function(lazyScript){
    return {
        restrict: 'A',
        compile: function(tElement, tAttributes){
            tElement.removeAttr('smart-masked-input data-smart-masked-input');

        	lazyScript.register('build/vendor.ui.js').then(function() {

                var options = {};
                if (tAttributes.maskPlaceholder) options.placeholder = tAttributes.maskPlaceholder;
                if (!$.mask.definitions['F']) $.mask.definitions['F'] = "[A-Fa-f0-9]";
                tElement.mask(tAttributes.smartMaskedInput, options);
            })
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartNouislider', function ($parse, lazyScript) {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            lazyScript.register('build/vendor.ui.js').then(function(){
                tElement.removeAttr('smart-nouislider data-smart-nouislider');

                tElement.addClass('noUiSlider');

                var options = {
                    range: {
                        min: tAttributes.rangeMin ? parseInt(tAttributes.rangeMin) : 0,
                        max: tAttributes.rangeMax ? parseInt(tAttributes.rangeMax) : 1000
                    },
                    start: $parse(tAttributes.start)()
                };

                if (tAttributes.step) options.step =  parseInt(tAttributes.step);

                if(tAttributes.connect) options.connect = tAttributes.connect == 'true' ? true : tAttributes.connect;

                tElement.noUiSlider(options);

                if(tAttributes.update) tElement.on('slide', function(){
                    $(tAttributes.update).text(JSON.stringify(tElement.val()));
                });                
            })
        }
    }
});
'use strict'

angular.module('SmartAdmin.Forms').directive('smartSelect2', function (lazyScript) {
    return {
        restrict: 'A',
        compile: function (element, attributes) {
            element.hide().removeAttr('smart-select2 data-smart-select2');
        	lazyScript.register('build/vendor.ui.js').then(function(){
	            element.show().select2();
        	})
        }
    }
});
'use strict'

angular.module('SmartAdmin.Forms').directive('smartSpinner', function () {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-spinner');

            var options = {};
            if(tAttributes.smartSpinner == 'deicimal'){
                options = {
                    step: 0.01,
                    numberFormat: "n"
                };
            }else if(tAttributes.smartSpinner == 'currency'){
                options = {
                    min: 5,
                    max: 2500,
                    step: 25,
                    start: 1000,
                    numberFormat: "C"
                };
            }

            tElement.spinner(options);
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartTagsinput', function () {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-tagsinput data-smart-tagsinput');
            tElement.tagsinput();
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartTimepicker', function () {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {
            tElement.removeAttr('smart-timepicker data-smart-timepicker');
            tElement.timepicker();
        }
    }
});

'use strict';

angular.module('SmartAdmin.Forms').directive('smartUislider', function ($parse, lazyScript) {
    return {
        restrict: 'A',
        compile: function (tElement, tAttributes) {

            tElement.removeAttr('smart-uislider data-smart-uislider');

            lazyScript.register('build/vendor.ui.js').then(function(){
			    tElement.bootstrapSlider();

			    $(tElement.data('bootstrapSlider').sliderElem).prepend(tElement);      	
            })

        }
    }
});
"use strict";

angular.module('SmartAdmin.Forms').directive('smartXeditable', function($timeout, $log){

	function link (scope, element, attrs, ngModel) {

        var defaults = {
            // display: function(value, srcData) {
            //     ngModel.$setViewValue(value);
            //     // scope.$apply();
            // }
        };

        var inited = false;

        var initXeditable = function() {

            var options = scope.options || {};
    		var initOptions = angular.extend(defaults, options);

            // $log.log(initOptions);
            element.editable('destroy');
            element.editable(initOptions);
        }

        scope.$watch("options", function(newValue) {

            if(!newValue) {
                return false;
            }

            initXeditable();

            // $log.log("Options changed...");

        }, true);

    }

    return {
    	restrict: 'A',
    	require: "ngModel",
        scope: {
            options: "="
        },
    	link: link 

    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartDropzone', function () {
    return function (scope, element, attrs) {
        var config, dropzone;

        config = scope[attrs.smartDropzone];

        // create a Dropzone for the element with the given options
        dropzone = new Dropzone(element[0], config.options);

        // bind the given event handlers
        angular.forEach(config.eventHandlers, function (handler, event) {
            dropzone.on(event, handler);
        });
    };
});

'use strict';

angular.module('SmartAdmin.Forms').directive('smartValidateForm', function (formsCommon) {
    return {
        restrict: 'A',
        link: function (scope, form, attributes) {

            var validateOptions = {
                rules: {},
                messages: {},
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            };
            form.find('[data-smart-validate-input], [smart-validate-input]').each(function () {
                var $input = $(this), fieldName = $input.attr('name');

                validateOptions.rules[fieldName] = {};

                if ($input.data('required') != undefined) {
                    validateOptions.rules[fieldName].required = true;
                }
                if ($input.data('email') != undefined) {
                    validateOptions.rules[fieldName].email = true;
                }

                if ($input.data('maxlength') != undefined) {
                    validateOptions.rules[fieldName].maxlength = $input.data('maxlength');
                }

                if ($input.data('minlength') != undefined) {
                    validateOptions.rules[fieldName].minlength = $input.data('minlength');
                }

                if($input.data('message')){
                    validateOptions.messages[fieldName] = $input.data('message');
                } else {
                    angular.forEach($input.data(), function(value, key){
                        if(key.search(/message/)== 0){
                            if(!validateOptions.messages[fieldName])
                                validateOptions.messages[fieldName] = {};

                            var messageKey = key.toLowerCase().replace(/^message/,'')
                            validateOptions.messages[fieldName][messageKey] = value;
                        }
                    });
                }
            });


            form.validate(validateOptions);

        }
    }
});

'use strict';

angular.module('SmartAdmin.Forms').directive('smartFueluxWizard', function () {
    return {
        restrict: 'A',
        scope: {
            smartWizardCallback: '&'
        },
        link: function (scope, element, attributes) {

            var wizard = element.wizard();

            var $form = element.find('form');

            wizard.on('actionclicked.fu.wizard', function(e, data){
                if ($form.data('validator')) {
                    if (!$form.valid()) {
                        $form.data('validator').focusInvalid();
                        e.preventDefault();
                    }
                }
            });

            wizard.on('finished.fu.wizard', function (e, data) {
                var formData = {};
                _.each($form.serializeArray(), function(field){
                    formData[field.name] = field.value
                });
                if(typeof scope.smartWizardCallback() === 'function'){
                    scope.smartWizardCallback()(formData)
                }
            });
        }
    }
});
'use strict';

angular.module('SmartAdmin.Forms').directive('smartWizard', function () {
    return {
        restrict: 'A',
        scope: {
            'smartWizardCallback': '&'
        },
        link: function (scope, element, attributes) {

            var stepsCount = $('[data-smart-wizard-tab]').length;

            var currentStep = 1;

            var validSteps = [];

            var $form = element.closest('form');

            var $prev = $('[data-smart-wizard-prev]', element);

            var $next = $('[data-smart-wizard-next]', element);

            function setStep(step) {
                currentStep = step;
                $('[data-smart-wizard-pane=' + step + ']', element).addClass('active').siblings('[data-smart-wizard-pane]').removeClass('active');
                $('[data-smart-wizard-tab=' + step + ']', element).addClass('active').siblings('[data-smart-wizard-tab]').removeClass('active');

                $prev.toggleClass('disabled', step == 1)
            }


            element.on('click', '[data-smart-wizard-tab]', function (e) {
                setStep(parseInt($(this).data('smartWizardTab')));
                e.preventDefault();
            });

            $next.on('click', function (e) {
                if ($form.data('validator')) {
                    if (!$form.valid()) {
                        validSteps = _.without(validSteps, currentStep);
                        $form.data('validator').focusInvalid();
                        return false;
                    } else {
                        validSteps = _.without(validSteps, currentStep);
                        validSteps.push(currentStep);
                        element.find('[data-smart-wizard-tab=' + currentStep + ']')
                            .addClass('complete')
                            .find('.step')
                            .html('<i class="fa fa-check"></i>');
                    }
                }
                if (currentStep < stepsCount) {
                    setStep(currentStep + 1);
                } else {
                    if (validSteps.length < stepsCount) {
                        var steps = _.range(1, stepsCount + 1)

                        _(steps).forEach(function (num) {
                            if (validSteps.indexOf(num) == -1) {
                                console.log(num);
                                setStep(num);
                                return false;
                            }
                        })
                    } else {
                        var data = {};
                        _.each($form.serializeArray(), function(field){
                            data[field.name] = field.value
                        });
                        if(typeof  scope.smartWizardCallback() === 'function'){
                            scope.smartWizardCallback()(data)
                        }
                    }
                }

                e.preventDefault();
            });

            $prev.on('click', function (e) {
                if (!$prev.hasClass('disabled') && currentStep > 0) {
                    setStep(currentStep - 1);
                }
                e.preventDefault();
            });


            setStep(currentStep);

        }
    }
});
'use strict';

angular.module('SmartAdmin.Layout').directive('demoStates', function ($rootScope) {
    return {
        restrict: 'EA',
        replace: true,
        templateUrl: 'app/_common/layout/directives/demo/demo-states.tpl.html',
        scope: true,
        link: function (scope, element, attributes) {
            element.parent().css({
                position: 'relative'
            });

            element.on('click', '#demo-setting', function () {
                element.toggleClass('activate')
            })
        },
        controller: function ($scope) {
            var $root = $('body');

            $scope.$watch('fixedHeader', function (fixedHeader) {
                localStorage.setItem('sm-fixed-header', fixedHeader);
                $root.toggleClass('fixed-header', fixedHeader);
                if (fixedHeader == false) {
                    $scope.fixedRibbon = false;
                    $scope.fixedNavigation = false;
                }
            });


            $scope.$watch('fixedNavigation', function (fixedNavigation) {
                localStorage.setItem('sm-fixed-navigation', fixedNavigation);
                $root.toggleClass('fixed-navigation', fixedNavigation);
                if (fixedNavigation) {
                    $scope.insideContainer = false;
                    $scope.fixedHeader = true;
                } else {
                    $scope.fixedRibbon = false;
                }
            });


            $scope.$watch('fixedRibbon', function (fixedRibbon) {
                localStorage.setItem('sm-fixed-ribbon', fixedRibbon);
                $root.toggleClass('fixed-ribbon', fixedRibbon);
                if (fixedRibbon) {
                    $scope.fixedHeader = true;
                    $scope.fixedNavigation = true;
                    $scope.insideContainer = false;
                }
            });

            $scope.$watch('fixedPageFooter', function (fixedPageFooter) {
                localStorage.setItem('sm-fixed-page-footer', fixedPageFooter);
                $root.toggleClass('fixed-page-footer', fixedPageFooter);
            });

            $scope.$watch('insideContainer', function (insideContainer) {
                localStorage.setItem('sm-inside-container', insideContainer);
                $root.toggleClass('container', insideContainer);
                if (insideContainer) {
                    $scope.fixedRibbon = false;
                    $scope.fixedNavigation = false;
                }
            });

            $scope.$watch('rtl', function (rtl) {
                localStorage.setItem('sm-rtl', rtl);
                $root.toggleClass('smart-rtl', rtl);
            });

            $scope.$watch('menuOnTop', function (menuOnTop) {
                $rootScope.$broadcast('$smartLayoutMenuOnTop', menuOnTop);
                localStorage.setItem('sm-menu-on-top', menuOnTop);
                $root.toggleClass('menu-on-top', menuOnTop);

                if(menuOnTop)$root.removeClass('minified');
            });

            $scope.$watch('colorblindFriendly', function (colorblindFriendly) {
                localStorage.setItem('sm-colorblind-friendly', colorblindFriendly);
                $root.toggleClass('colorblind-friendly', colorblindFriendly);
            });


            $scope.fixedHeader = localStorage.getItem('sm-fixed-header') == 'true';
            $scope.fixedNavigation = localStorage.getItem('sm-fixed-navigation') == 'true';
            $scope.fixedRibbon = localStorage.getItem('sm-fixed-ribbon') == 'true';
            $scope.fixedPageFooter = localStorage.getItem('sm-fixed-page-footer') == 'true';
            $scope.insideContainer = localStorage.getItem('sm-inside-container') == 'true';
            $scope.rtl = localStorage.getItem('sm-rtl') == 'true';
            $scope.menuOnTop = localStorage.getItem('sm-menu-on-top') == 'true' || $root.hasClass('menu-on-top');
            $scope.colorblindFriendly = localStorage.getItem('sm-colorblind-friendly') == 'true';


            $scope.skins = appConfig.skins;


            $scope.smartSkin = localStorage.getItem('sm-skin') ? localStorage.getItem('sm-skin') : appConfig.smartSkin;

            $scope.setSkin = function (skin) {
                $scope.smartSkin = skin.name;
                $root.removeClass(_.pluck($scope.skins, 'name').join(' '));
                $root.addClass(skin.name);
                localStorage.setItem('sm-skin', skin.name);
                $("#logo img").attr('src', skin.logo);
            };


            if($scope.smartSkin != "smart-style-0"){
                $scope.setSkin(_.find($scope.skins, {name: $scope.smartSkin}))
            }


            $scope.factoryReset = function () {
                $.SmartMessageBox({
                    title: "<i class='fa fa-refresh' style='color:green'></i> Clear Local Storage",
                    content: "Would you like to RESET all your saved widgets and clear LocalStorage?1",
                    buttons: '[No][Yes]'
                }, function (ButtonPressed) {
                    if (ButtonPressed == "Yes" && localStorage) {
                        localStorage.clear();
                        location.reload()
                    }
                });
            }
        }
    }
});
"use strict";

(function ($) {

    $.fn.smartCollapseToggle = function () {

        return this.each(function () {

            var $body = $('body');
            var $this = $(this);

            // only if not  'menu-on-top'
            if ($body.hasClass('menu-on-top')) {


            } else {

                $body.hasClass('mobile-view-activated')

                // toggle open
                $this.toggleClass('open');

                // for minified menu collapse only second level
                if ($body.hasClass('minified')) {
                    if ($this.closest('nav ul ul').length) {
                        $this.find('>a .collapse-sign .fa').toggleClass('fa-minus-square-o fa-plus-square-o');
                        $this.find('ul:first').slideToggle(appConfig.menu_speed || 200);
                    }
                } else {
                    // toggle expand item
                    $this.find('>a .collapse-sign .fa').toggleClass('fa-minus-square-o fa-plus-square-o');
                    $this.find('ul:first').slideToggle(appConfig.menu_speed || 200);
                }
            }
        });
    };
})(jQuery);

angular.module('SmartAdmin.Layout').directive('smartMenu', function ($state, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var $body = $('body');

            var $collapsible = element.find('li[data-menu-collapse]');

            var bindEvents = function(){
                $collapsible.each(function (idx, li) {
                    var $li = $(li);
                    $li
                        .on('click', '>a', function (e) {

                            // collapse all open siblings
                            $li.siblings('.open').smartCollapseToggle();

                            // toggle element
                            $li.smartCollapseToggle();

                            // add active marker to collapsed element if it has active childs
                            if (!$li.hasClass('open') && $li.find('li.active').length > 0) {
                                $li.addClass('active')
                            }

                            e.preventDefault();
                        })
                        .find('>a').append('<b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b>');

                    // initialization toggle
                    if ($li.find('li.active').length) {
                        $li.smartCollapseToggle();
                        $li.find('li.active').parents('li').addClass('active');
                    }
                });
            }
            bindEvents();


            // click on route link
            element.on('click', 'a[data-ui-sref]', function (e) {
                // collapse all siblings to element parents and remove active markers
                $(this)
                    .parents('li').addClass('active')
                    .each(function () {
                        $(this).siblings('li.open').smartCollapseToggle();
                        $(this).siblings('li').removeClass('active')
                    });

                if ($body.hasClass('mobile-view-activated')) {
                    $rootScope.$broadcast('requestToggleMenu');
                }
            });


            scope.$on('$smartLayoutMenuOnTop', function (event, menuOnTop) {
                if (menuOnTop) {
                    $collapsible.filter('.open').smartCollapseToggle();
                }
            });
        }
    }
});
(function(){
    "use strict";

    angular.module('SmartAdmin.Layout').directive('smartMenuItems', function ($http, $rootScope, $compile) {
    return {
        restrict: 'A',
        compile: function (element, attrs) {
            

            function createItem(item, parent, level){
                var li = $('<li />' ,{'ui-sref-active': "active"})
                var a = $('<a />');
                var i = $('<i />');

                li.append(a);

                if(item.sref)
                    a.attr('ui-sref', item.sref);
                if(item.href)
                    a.attr('href', item.href);
                if(item.icon){
                    i.attr('class', 'fa fa-lg fa-fw fa-'+item.icon);
                    a.append(i);
                }
                if(item.title){
                    a.attr('title', item.title);
                    if(level == 1){ 
                        a.append(' <span class="menu-item-parent">' + item.title + '</span>');
                    } else {
                        a.append(' ' + item.title);

                    }
                }

                if(item.items){
                    var ul = $('<ul />');
                    li.append(ul);
                    li.attr('data-menu-collapse', '');
                    _.forEach(item.items, function(child) {
                        createItem(child, ul, level+1);
                    })
                } 

                parent.append(li); 
            }


            $http.get(attrs.smartMenuItems).then(function(res){
                var ul = $('<ul />', {
                    'smart-menu': ''
                })
                _.forEach(res.data.items, function(item) {
                    createItem(item, ul, 1);
                })
                
                var $scope = $rootScope.$new();
                var html = $('<div>').append(ul).html(); 
                var linkingFunction = $compile(html);
                
                var _element = linkingFunction($scope);

                element.replaceWith(_element);                
            })
        }
    }
});
})();
/**
 * Jarvis Widget Directive
 *
 *    colorbutton="false"
 *    editbutton="false"
      togglebutton="false"
       deletebutton="false"
        fullscreenbutton="false"
        custombutton="false"
        collapsed="true"
          sortable="false"
 *
 *
 */
"use strict";

angular.module('SmartAdmin.Layout').directive('jarvisWidget', function($rootScope){
    return {
        restrict: "A",
        compile: function(element, attributes){
            if(element.data('widget-color'))
                element.addClass('jarviswidget-color-' + element.data('widget-color'));


            element.find('.widget-body').prepend('<div class="jarviswidget-editbox"><input class="form-control" type="text"></div>');

            element.addClass('jarviswidget');
            $rootScope.$emit('jarvisWidgetAdded', element )

        }
    }
});
 "use strict";
 
 angular.module('SmartAdmin.Layout').directive('widgetGrid', function ($rootScope, $compile, $q, $state, $timeout) {

    var jarvisWidgetsDefaults = {
        grid: 'article',
        widgets: '.jarviswidget',
        localStorage: true,
        deleteSettingsKey: '#deletesettingskey-options',
        settingsKeyLabel: 'Reset settings?',
        deletePositionKey: '#deletepositionkey-options',
        positionKeyLabel: 'Reset position?',
        sortable: true,
        buttonsHidden: false,
        // toggle button
        toggleButton: true,
        toggleClass: 'fa fa-minus | fa fa-plus',
        toggleSpeed: 200,
        onToggle: function () {
        },
        // delete btn
        deleteButton: true,
        deleteMsg: 'Warning: This action cannot be undone!',
        deleteClass: 'fa fa-times',
        deleteSpeed: 200,
        onDelete: function () {
        },
        // edit btn
        editButton: true,
        editPlaceholder: '.jarviswidget-editbox',
        editClass: 'fa fa-cog | fa fa-save',
        editSpeed: 200,
        onEdit: function () {
        },
        // color button
        colorButton: true,
        // full screen
        fullscreenButton: true,
        fullscreenClass: 'fa fa-expand | fa fa-compress',
        fullscreenDiff: 3,
        onFullscreen: function () {
        },
        // custom btn
        customButton: false,
        customClass: 'folder-10 | next-10',
        customStart: function () {
            alert('Hello you, this is a custom button...');
        },
        customEnd: function () {
            alert('bye, till next time...');
        },
        // order
        buttonOrder: '%refresh% %custom% %edit% %toggle% %fullscreen% %delete%',
        opacity: 1.0,
        dragHandle: '> header',
        placeholderClass: 'jarviswidget-placeholder',
        indicator: true,
        indicatorTime: 600,
        ajax: true,
        timestampPlaceholder: '.jarviswidget-timestamp',
        timestampFormat: 'Last update: %m%/%d%/%y% %h%:%i%:%s%',
        refreshButton: true,
        refreshButtonClass: 'fa fa-refresh',
        labelError: 'Sorry but there was a error:',
        labelUpdated: 'Last Update:',
        labelRefresh: 'Refresh',
        labelDelete: 'Delete widget:',
        afterLoad: function () {
        },
        rtl: false, // best not to toggle this!
        onChange: function () {

        },
        onSave: function () {

        },
        ajaxnav: true

    }

    var dispatchedWidgetIds = [];
    var setupWaiting = false;

    var debug = 1;

    var setupWidgets = function (element, widgetIds) {

        if (!setupWaiting) {

            if(_.intersection(widgetIds, dispatchedWidgetIds).length != widgetIds.length){

                dispatchedWidgetIds = _.union(widgetIds, dispatchedWidgetIds);

//                    console.log('setupWidgets', debug++);

                element.data('jarvisWidgets') && element.data('jarvisWidgets').destroy();
                element.jarvisWidgets(jarvisWidgetsDefaults);
                initDropdowns(widgetIds);
            }

        } else {
            if (!setupWaiting) {
                setupWaiting = true;
                $timeout(function () {
                    setupWaiting = false;
                    setupWidgets(element, widgetIds)
                }, 200);
            }
        }

    };

    var destroyWidgets = function(element, widgetIds){
        element.data('jarvisWidgets') && element.data('jarvisWidgets').destroy();
        dispatchedWidgetIds = _.xor(dispatchedWidgetIds, widgetIds);
    };

    var initDropdowns = function (widgetIds) {
        angular.forEach(widgetIds, function (wid) {
            $('#' + wid + ' [data-toggle="dropdown"]').each(function () {
                var $parent = $(this).parent();
                // $(this).removeAttr('data-toggle');
                if (!$parent.attr('dropdown')) {
                    $(this).removeAttr('href');
                    $parent.attr('dropdown', '');
                    var compiled = $compile($parent)($parent.scope())
                    $parent.replaceWith(compiled);
                }
            })
        });
    };

    var jarvisWidgetAddedOff,
        $viewContentLoadedOff,
        $stateChangeStartOff;

    return {
        restrict: 'A',
        compile: function(element){

            element.removeAttr('widget-grid data-widget-grid');

            var widgetIds = [];

            $viewContentLoadedOff = $rootScope.$on('$viewContentLoaded', function (event, data) {
                $timeout(function () {
                    setupWidgets(element, widgetIds)
                }, 100);
            });


            $stateChangeStartOff = $rootScope.$on('$stateChangeStart',
                function(event, toState, toParams, fromState, fromParams){
                    jarvisWidgetAddedOff();
                    $viewContentLoadedOff();
                    $stateChangeStartOff();
                    destroyWidgets(element, widgetIds)
                });

            jarvisWidgetAddedOff = $rootScope.$on('jarvisWidgetAdded', function (event, widget) {
                if (widgetIds.indexOf(widget.attr('id')) == -1) {
                    widgetIds.push(widget.attr('id'));
                    $timeout(function () {
                        setupWidgets(element, widgetIds)
                    }, 100);
                }
//                    console.log('jarvisWidgetAdded', widget.attr('id'));
            });

        }
    }
});

angular.module('app.proyectos')
    .controller('PoligonoAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.polygon = {
                polygonId: null,
                name: '',
                code: '',
                active: '',
                section: {
                    sectionId: null,
                }
            };

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('project.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    $scope.polygon.active = 'true';
                    $scope.polygon.section.sectionId = $stateParams.seccion;
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('project.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('project.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'polygonGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;

                        $scope.polygon.polygonId = data.polygonId;
                        $scope.polygon.name = data.name;
                        $scope.polygon.code = data.code;
                        $scope.polygon.active = '' + data.active;
                        $scope.polygon.section.sectionId = data.section.sectionId;

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            $scope.cancel = function () {
                $state.go('app.proyecto.seccion.poligono', {seccion: $scope.polygon.section.sectionId});
                return false;
            };

            // Guardar informacion, crea o edita un usuario con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                } else if (!!$scope.polygon.name && !!$scope.polygon.code) {
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('project.add')) {
                                $scope.polygon.active = ($scope.polygon.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.polygon, 'polygonPOST').then(function () {
                                    $.bigBox({
                                        title: "Nuevo polígono agregado",
                                        content: "Se guardo la información del polígono: <b>" + $scope.polygon.name + "</b> - <i>" + $scope.polygon.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del polígono';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('project.edit')) {
                                $scope.polygon.active = ($scope.polygon.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.polygon, 'polygonPUT', '/' + $scope.polygon.polygonId).then(function () {
                                    $.bigBox({
                                        title: "Polígono actualizado",
                                        content: "Se actualizo la información del polígono: <b>" + $scope.polygon.name + "</b> - <i>" + $scope.polygon.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del polígono';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);

angular.module('app.proyectos')
    .controller('PoligonoCtrl', ['$scope', '$compile', '$state', 'DTOptionsBuilder', 'DTColumnBuilder', 'permissionsService', 'restService', '$stateParams',
        function ($scope, $compile, $state, DTOptionsBuilder, DTColumnBuilder, permissionsService, restService, $stateParams) {
            $scope.pageTitle = $state.current.data.title;
            $scope.projectId;
            $scope.sectionId = $stateParams.seccion;

            /* Obtener informacion para seleccionar proyecto, seccion */
            function loadProjects() {
                restService.sendRequest(null, 'projectGET').then(function (data) {
                    $scope.listProjects = data.data;
                }, function () {
                    $.bigBox({
                        title: "Error al cargar",
                        content: "No se pudo cargar la lista de proyectos",
                        color: "#C46A69",
                        icon: "fa fa-warning shake animated",
                        timeout: 6000
                    });
                    $state.go('app.proyecto');
                });
            }

            function loadSections() {
                restService.sendRequest(null, 'sectionGET', '?projectId=' + $scope.projectId).then(function (data) {
                    $scope.listSections = data.data;
                }, function () {
                    $.bigBox({
                        title: "Error al cargar",
                        content: "No se pudo cargar la lista de secciones",
                        color: "#C46A69",
                        icon: "fa fa-warning shake animated",
                        timeout: 6000
                    });
                    $state.go('app.proyecto');
                });
            }

            loadProjects();
            if (!!$scope.sectionId) {
                restService.sendRequest(null, 'sectionGET', '/' + $scope.sectionId).then(function (data) {
                    $scope.projectId = '' + data.data.project.projectId;
                    loadSections();
                }, function () {
                    $scope.sectionId = '';
                });
            }
            /* ~ */

            $scope.updateData = function () {
                if (!!$scope.projectId && !!$scope.sectionId)
                    $state.go('app.proyecto.seccion.poligono', {seccion: $scope.sectionId});
                else if (!!$scope.projectId)
                    loadSections();
            };

            $scope.statusFilter = {
                'true': 'Activo',
                'false': 'Inactivo'
            };

            /* Datatable (DT), creacion y asignacion de eventos */
            var dataArrayResolved = [];

            $scope.standardOptions = DTOptionsBuilder
                .fromFnPromise(restService.sendRequest(null, 'polygonGET', '?sectionId=' + ($scope.sectionId || 0)))
                .withDataProp('data')
                .withDOM("t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap()
                .withOption('createdRow', createdRow);
            $scope.standardColumns = [
                DTColumnBuilder.newColumn('name').notSortable().renderWith(function (row, type, data) {
                    return '<a ui-sref="app.proyecto.seccion.poligono.circuito({poligono: ' + data.polygonId + '})">' + row + '</a>';
                }),
                DTColumnBuilder.newColumn('code').notSortable(),
                DTColumnBuilder.newColumn('active').notSortable().renderWith(function (data) {
                    return $scope.statusFilter[data];
                }),
                DTColumnBuilder.newColumn(null).notSortable().withClass('table-actions-buttons').renderWith(function (data) {
                    dataArrayResolved[data.polygonId] = data;
                    return '<a has-permission="PROJECT_VIEW" title="Ver" ui-sref="app.proyecto.seccion.poligono.accion({accion: \'ver\', id: ' + data.polygonId + '})" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>' +
                        '<a has-permission="PROJECT_EDIT" title="Editar" ui-sref="app.proyecto.seccion.poligono.accion({accion: \'editar\', id: ' + data.polygonId + '})" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>' +
                        '<a has-permission="PROJECT_EDIT" title="' + (data.active ? 'Desactivar' : 'Activar') + '" ng-click="accion(\'' + (data.active ? 'deactivate' : 'activate') + '\', ' + data.polygonId + ')" class="btn btn-default btn-xs"><i class="fa ' + (data.active ? 'fa-ban' : 'fa-check-circle-o') + '"></i></a>' +
                        '<a has-permission="PROJECT_DELETE" title="Eliminar" ng-click="accion(\'delete\', ' + data.polygonId + ')" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }

            // Callback para funcionalidad de filtros
            $scope.instanceCallback = function (dtInstance) {
                $scope.dtInstance = dtInstance;
                var table = dtInstance.DataTable;
                table.columns().eq(0).each(function (colIdx) {
                    $('input, select', table.column(colIdx).header()).on('keyup change', function () {
                        if (this.getAttribute('id') == 'table-lenght-ctrl') {
                            table
                                .page.len(this.value)
                                .draw();
                        }
                        else {
                            var isSelect = (this.nodeName == 'SELECT');
                            var searchValue = (isSelect && this.value !== '') ? '\\b' + this.value + '\\b' : this.value;
                            table
                                .column(colIdx)
                                .search(searchValue, isSelect, !isSelect)
                                .draw();
                        }
                    });
                });
            };

            $scope.accion = function (accion, id) {
                switch (accion) {
                    case 'delete':
                    case 'eliminar':
                        if (permissionsService.canAccess('project.delete')) {
                            $.SmartMessageBox({
                                title: "Borrar Polígono",
                                content: "Esta a punto de borrar la información del polígono <b>" + dataArrayResolved[id].name + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'polygonDELETE', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se borro la información del polígono <b>" + dataArrayResolved[id].name + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'polygonGET', '?sectionId=' + ($scope.sectionId || 0)));
                                        else
                                            $state.go('app.proyecto.seccion.poligono', {seccion: $scope.sectionId}, {reload: true});
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo borrar el polígono solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                    case 'activate':
                    case 'deactivate':
                        if (permissionsService.canAccess('project.edit')) {
                            var active = (accion.toLowerCase() === 'activate');
                            $.SmartMessageBox({
                                title: (active ? 'Activar' : 'Desactivar') + " Proyecto",
                                content: "Esta a punto de " + (active ? 'activar' : 'desactivar') + " el polígono <b>" + dataArrayResolved[id].name + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    dataArrayResolved[id].active = active;
                                    restService.sendRequest(dataArrayResolved[id], 'polygonPUT', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se " + (active ? 'activo' : 'desactivo') + " el polígono <b>" + dataArrayResolved[id].name + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'polygonGET', '?sectionId=' + ($scope.sectionId || 0)));
                                        else
                                            $state.go('app.proyecto.seccion.poligono', {seccion: $scope.sectionId}, {reload: true});
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo " + (active ? 'activar' : 'desactivar') + " el polígono solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                }
            };
        }
    ]);
angular.module('app.proyectos')
    .controller('CircuitoAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService', 'uiGmapGoogleMapApi',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService, uiGmapGoogleMapApi) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            var centerMapCoords = {
                latitude: 20.6322137,
                longitude: -103.2776288
            };

            $scope.device = {
                deviceId: null,
                controlNumber: '',
                macAddress: '',
                lamps: 0,
                latitude: null,
                longitude: null,
                ligthOffHour: '08:00',
                ligthOnHour: '19:00',
                timeBeforeNotify: 0,
                timeToMarkAsDisconnected: 0,
                installationStatus: 'Proyectado',
                polygonView: {
                    polygonId: null
                },
                project: {
                    projectId: null
                },
                section: {
                    sectionId: null,
                }
            };

            // Limit inputs type number to receive only numbers and some browser control keys (like tab, backspace, etc)
            $('input[type=number]').on('keydown', function (e) {
                var validKeyCodes = [107, 187, 8, 46, 37, 38, 39, 40, 116, 9];
                if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57) || validKeyCodes.indexOf(e.keyCode) != -1 )
                    return true;
                else
                    return false;
            });

            /**
             * Copy/replace values from objectData to objectTarget
             * @param objectTarget, object where to save the data
             * @param objectData, object with the data to copy
             */
            function fillObjectValues(objectTarget, objectData) {
                for (var prop in objectTarget) {
                    if (typeof objectTarget[prop] === 'object' && objectTarget[prop] !== null)
                        fillObjectValues(objectTarget[prop], objectData[prop]);
                    else
                        objectTarget[prop] = objectData[prop];
                }
            }

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('project.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    $scope.device.polygonView.polygonId = $stateParams.poligono;
                    restService.sendRequest(null, 'polygonGET', '/' + $stateParams.poligono).then(function (data) {
                        data = data.data;

                        $scope.device.project.projectId = data.section.project.projectId;
                        $scope.device.section.sectionId = data.section.sectionId;
                        $scope.device.polygonView.polygonId = data.polygonId;
                    }, function () {
                        $.bigBox({
                            title: "Error al cargar",
                            content: "No se pudo cargar la información del proyecto relacionado",
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('project.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('project.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'deviceGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;

                        fillObjectValues($scope.device, data);
                        fillObjectValues(centerMapCoords, $scope.device);

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            $scope.cancel = function () {
                $state.go('app.proyecto.seccion.poligono.circuito', {poligono: $scope.device.polygonView.polygonId});
                return false;
            };

            /* Mapa */
            $scope.map = {
                center: centerMapCoords,
                zoom: 15,
                options: {
                    scrollwheel: false
                },
                events: {
                    click: function(map, eventName, events) {
                        if (!$scope.readOnly) {
                            var event = events[0];
                            $scope.$apply(function () {
                                $scope.device.latitude = event.latLng.lat();
                                $scope.device.longitude = event.latLng.lng();
                                map.panTo(new google.maps.LatLng($scope.device.latitude, $scope.device.longitude));
                            });
                        }
                    }
                }
            };
            $scope.mapMarker = {
                id: 0,
                options: {
                    draggable: !$scope.readOnly
                },
                events: {

                    dragend: function (marker) {
                        if (!$scope.readOnly) {
                            $scope.device.latitude = marker.getPosition().lat();
                            $scope.device.longitude = marker.getPosition().lng();
                            marker.map.panTo(new google.maps.LatLng($scope.device.latitude, $scope.device.longitude));
                        }
                    }
                }
            };

            uiGmapGoogleMapApi.then(function (maps) {});
            /* ~ */

            // Guardar informacion, crea o edita un usuario con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                } else if ($scope.device.lamps > 0 && $scope.device.timeBeforeNotify > 0 && $scope.device.timeToMarkAsDisconnected > 0) {
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('project.add')) {
                                restService.sendRequest($scope.device, 'devicePOST').then(function () {
                                    $.bigBox({
                                        title: "Nuevo circuito agregado",
                                        content: "Se guardo la información del circuito: <br><b>" + $scope.device.controlNumber + "</b>, MAC <i>" + $scope.device.macAddress + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del circuito';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('project.edit')) {
                                restService.sendRequest($scope.device, 'devicePUT', '/' + $scope.device.deviceId).then(function () {
                                    $.bigBox({
                                        title: "Circuito actualizado",
                                        content: "Se actualizo la información del circuito: <br><b>" + $scope.device.controlNumber + "</b>, MAC <i>" + $scope.device.macAddress + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del circuito';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);

angular.module('app.proyectos')
    .controller('CircuitoCtrl', ['$scope', '$compile', '$state', 'DTOptionsBuilder', 'DTColumnBuilder', 'permissionsService', 'restService', '$stateParams', '$filter',
        function ($scope, $compile, $state, DTOptionsBuilder, DTColumnBuilder, permissionsService, restService, $stateParams, $filter) {
            $scope.pageTitle = $state.current.data.title;
            $scope.projectId;
            $scope.sectionId;
            $scope.polygonId = $stateParams.poligono;

            /* Obtener informacion para seleccionar proyecto, seccion */
            function loadProjects() {
                restService.sendRequest(null, 'projectGET').then(function (data) {
                    $scope.listProjects = data.data;
                }, function () {
                    $.bigBox({
                        title: "Error al cargar",
                        content: "No se pudo cargar la lista de proyectos",
                        color: "#C46A69",
                        icon: "fa fa-warning shake animated",
                        timeout: 6000
                    });
                    $state.go('app.proyecto');
                });
            }

            function loadSections() {
                restService.sendRequest(null, 'sectionGET', '?projectId=' + $scope.projectId).then(function (data) {
                    $scope.listSections = data.data;
                }, function () {
                    $.bigBox({
                        title: "Error al cargar",
                        content: "No se pudo cargar la lista de secciones",
                        color: "#C46A69",
                        icon: "fa fa-warning shake animated",
                        timeout: 6000
                    });
                    $state.go('app.proyecto');
                });
            }

            function loadPolygons() {
                restService.sendRequest(null, 'polygonGET', '?sectionId=' + $scope.sectionId).then(function (data) {
                    $scope.listPolygons = data.data;
                }, function () {
                    $.bigBox({
                        title: "Error al cargar",
                        content: "No se pudo cargar la lista de poligonos",
                        color: "#C46A69",
                        icon: "fa fa-warning shake animated",
                        timeout: 6000
                    });
                    $state.go('app.proyecto');
                });
            }

            loadProjects();
            if (!!$scope.polygonId) {
                restService.sendRequest(null, 'polygonGET', '/' + $scope.polygonId).then(function (data) {
                    $scope.sectionId = '' + data.data.section.sectionId;
                    $scope.projectId = '' + data.data.section.project.projectId;
                    loadSections();
                    loadPolygons();
                }, function () {
                    $scope.polygonId = '';
                });
            }
            /* ~ */

            $scope.updateData = function () {
                if (!!$scope.projectId && !!$scope.sectionId && !!$scope.polygonId)
                    $state.go('app.proyecto.seccion.poligono.circuito', {poligono: $scope.polygonId});
                else if (!!$scope.projectId && !!$scope.sectionId)
                    loadPolygons();
                else if (!!$scope.projectId)
                    loadSections();
            };

            $scope.statusFilter = {};

            /* Datatable (DT), creacion y asignacion de eventos */
            var dataArrayResolved = [];

            $scope.standardOptions = DTOptionsBuilder
                .fromFnPromise(restService.sendRequest(null, 'deviceGET', '?polygonId=' + ($scope.polygonId || 0)))
                .withDataProp('data')
                .withDOM("t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap()
                .withOption('createdRow', createdRow);
            $scope.standardColumns = [
                DTColumnBuilder.newColumn('code').notSortable(),
                DTColumnBuilder.newColumn('macAddress').withClass('hidden-xs').notSortable(),
                DTColumnBuilder.newColumn('lastActivity').notSortable().renderWith(function (data) {
                    return $filter('date')(data, "dd-MM-yyyy hh:mm:ssa");
                }),
                DTColumnBuilder.newColumn('installationStatus').notSortable().renderWith(function (data) {
                    return data;
                }),
                DTColumnBuilder.newColumn(null).notSortable().withClass('table-actions-buttons').renderWith(function (data) {
                    /* loadStatusFilter */
                    var objExtender = {};
                    objExtender[data.installationStatus] = data.installationStatus
                    angular.extend($scope.statusFilter, objExtender);
                    /* ~ */
                    dataArrayResolved[data.deviceId] = data;
                    return '<a has-permission="PROJECT_VIEW" title="Ver" ui-sref="app.proyecto.seccion.poligono.circuito.accion({accion: \'ver\', id: ' + data.deviceId + '})" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>' +
                        '<a has-permission="PROJECT_EDIT" title="Editar" ui-sref="app.proyecto.seccion.poligono.circuito.accion({accion: \'editar\', id: ' + data.deviceId + '})" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>' +
                        '<a has-permission="PROJECT_DELETE" title="Eliminar" ng-click="accion(\'delete\', ' + data.deviceId + ')" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }

            // Callback para funcionalidad de filtros
            $scope.instanceCallback = function (dtInstance) {
                $scope.dtInstance = dtInstance;
                var table = dtInstance.DataTable;
                table.columns().eq(0).each(function (colIdx) {
                    $('input, select', table.column(colIdx).header()).on('keyup change', function () {
                        if (this.getAttribute('id') == 'table-lenght-ctrl') {
                            table
                                .page.len(this.value)
                                .draw();
                        }
                        else {
                            var isSelect = (this.nodeName == 'SELECT');
                            var searchValue = (isSelect && this.value !== '') ? '\\b' + this.value + '\\b' : this.value;
                            table
                                .column(colIdx)
                                .search(searchValue, isSelect, !isSelect)
                                .draw();
                        }
                    });
                });
            };

            $scope.accion = function (accion, id) {
                switch (accion) {
                    case 'delete':
                    case 'eliminar':
                        if (permissionsService.canAccess('project.delete')) {
                            $.SmartMessageBox({
                                title: "Borrar Circuito",
                                content: "Esta a punto de borrar la información del circuito <b>" + dataArrayResolved[id].code + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'polygonDELETE', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se borro la información del circuito <b>" + dataArrayResolved[id].code + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'deviceGET', '?polygonId=' + ($scope.polygonId || 0)));
                                        else
                                            $state.go('app.proyecto.seccion.poligono.circuito', {poligono: $scope.polygonId}, {reload: true});
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo borrar el circuito solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                }
            };
        }
    ]);