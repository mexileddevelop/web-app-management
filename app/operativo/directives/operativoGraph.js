"use strict";
angular.module('app.operativo').value('FlotConfig', {
    "chartBorderColor": "#efefef",
    "chartGridColor": "#DDD",
    //"chartOperativo": "#7e9d3a",
    "chartOperativo": "#f99c25",
    "chartMono": "#000"

});

angular.module('app.operativo').directive('operativoGraph', ['$timeout', 'FlotConfig', function($timeout, FlotConfig) {
    return {
        restrict: 'E',
        replace: true,
        template: '<div class="chart"></div>',
        scope: {
            data: '='
        },
        link: function (scope, element) {
// For the demo we use generated data, but normally it would be coming from the server
            var dataOptions = {
                lines: {
                    lineWidth: 1,
                    show: true,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 0.2
                        }, {
                            opacity: 0.4
                        }]
                    }
                },
                splines: {
                    show: false,
                    tension: 0.6,
                    lineWidth: 1,
                    fill: 0.1
                }
            };
            // setup plot
            var options = {
                xaxis: {
                    mode: "time",
                    tickLength: 20,
                    timezone: "browser"
                },
                yaxis: {
                    min: 0
                },
                colors: [FlotConfig.chartOperativo],
                grid: {
                    color: "#999999",
                    clickable: true,
                    tickColor: "#D4D4D4",
                    borderWidth: 0,
                    hoverable: true //IMPORTANT! this is needed for tooltip to work,
                },
                legend: {
                    position: 'sw',
                    margin: 5,
                    backgroundColor: "#FFFFFF"
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s <br> %x <br> Potencia: %y",
                    dateFormat: "%y-%m-%d %H:%M:%S"
                },
                series: {
                    points: {
                        show: true,
                        lineWidth: 1,
                    }
                }
            };
            var plot = $.plot(
                element,
                [
                    $.extend({}, dataOptions, {data: scope.data[0], label: "Potencia Fase 1"}),
                    $.extend({}, dataOptions, {data: scope.data[1], label: "Potencia Fase 2"})
                ],
                options
            );

            scope.$watch('data', function (newValue) {
                plot.setData([
                    $.extend({}, dataOptions, {data: newValue[0], label: "Potencia Fase 1"}),
                    $.extend({}, dataOptions, {data: newValue[1], label: "Potencia Fase 2"})
                ]);
                plot.setupGrid();
                plot.draw();
            });
        }
    }
}]);