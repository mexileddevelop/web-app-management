'use strict';

angular.module('app.operativo')
    .controller('OperativoCtrl', ['$scope', '$compile', '$filter', '$timeout', 'DTOptionsBuilder', 'DTColumnBuilder', 'restService',
        function ($scope, $compile, $filter, $timeout, DTOptionsBuilder, DTColumnBuilder, restService) {
            var promiseListDevices = restService.sendRequest(null, 'deviceGET');
            var resolvedListDevices = [];
            var indexSelectedDevice = -1;
            var timeoutToRefreshCurrentMeasure;

            $scope.graphData = [];
            $scope.currentRecord = {
                date: null,
                fase1: null,
                fase2: null
            };

                promiseListDevices.then(function (data) {
                resolvedListDevices = data.data;
                if (!!resolvedListDevices.length) {
                    indexSelectedDevice = 0;
                    $scope.filter();
                }
            });
            /*
             Grafica
             */
            /*function getRandomData(period) {
                var currentDate = Date.now();
                var tickRate; // Minutes
                var timeToDisplay = new Date();
                switch (period) {
                    case 'D':
                    default:
                        $scope.graphFilter = period;
                        timeToDisplay.setDate(timeToDisplay.getDate() - 1);
                        tickRate = 5;
                        break;
                    case 'S':
                        $scope.graphFilter = period;
                        timeToDisplay.setDate(timeToDisplay.getDate() - 7);
                        tickRate = 60;
                        break;
                    case 'M':
                        $scope.graphFilter = period;
                        timeToDisplay.setDate(timeToDisplay.getDate() - 28);
                        tickRate = 60;
                        break;
                }
                timeToDisplay.setHours(0, 2, 0, 0);

                // zip the generated y values with the x values
                var res = [];
                var ampere;
                while (timeToDisplay.getTime() < currentDate) {
                    ampere = (timeToDisplay.getHours() < 8 || timeToDisplay.getHours() >= 20) ? 15 : 0;
                    // X & Y axis data
                    res.push([timeToDisplay.getTime(), Math.round((ampere + Math.random() * 0.05) * 100) / 100]);
                    timeToDisplay.setMinutes(timeToDisplay.getMinutes() + tickRate);
                }
                return res;
            }*/

            function getRecordsData(Records) {
                var recordsForGraph = [[], []];

                for (var i = 0; i < Records.length; i++) {
                    recordsForGraph[0].push([Records[i].createdDate, Records[i].fase1.potencia]);
                    recordsForGraph[1].push([Records[i].createdDate, Records[i].fase2.potencia]);
                }

                return recordsForGraph;
            }

            function getCurrentMeasure(lastRecord) {
                var currentMeasure = {
                    date: null,
                    fase1: null,
                    fase2: null
                };

                var timeToNotifyFromNow = new Date().getTime() - resolvedListDevices[indexSelectedDevice].timeBeforeNotify * 60000;
                if (lastRecord.createdDate >= timeToNotifyFromNow) {
                    currentMeasure.date = lastRecord.createdDate;
                    currentMeasure.fase1 = lastRecord.fase1;
                    currentMeasure.fase2 = lastRecord.fase2;
                }

                /* set timer to get a new measure every timeBeforeNotify on device (parameter was in minutes) */
                if (!!timeoutToRefreshCurrentMeasure)
                    $timeout.cancel(timeoutToRefreshCurrentMeasure);
                timeoutToRefreshCurrentMeasure = $timeout(function () {
                    restService.sendRequest(null, 'deviceRecordsGET', '?id=' + resolvedListDevices[indexSelectedDevice].deviceId).then(function (data) {
                        $scope.currentRecord = getCurrentMeasure(data.data.lastRecords);
                    });
                }, resolvedListDevices[indexSelectedDevice].timeBeforeNotify * 60000);
                /* ~ */

                return currentMeasure;
            }

            $scope.filter = function (timePeriod) {
                timePeriod = timePeriod || 'D';
                if (timePeriod != $scope.graphFilter) {
                    $scope.graphFilter = timePeriod;
                    if (indexSelectedDevice >= 0) {
                        restService.sendRequest(null, 'deviceRecordsGET', '?id=' + resolvedListDevices[indexSelectedDevice].deviceId + '&type=' + $scope.graphFilter).then(function (data) {
                            $scope.graphData = getRecordsData(data.data.records);
                            $scope.currentRecord = getCurrentMeasure(data.data.lastRecords);
                        });
                    }
                }
            };

            $scope.loadData = function (deviceId) {
                function indexInListById(device) {
                    return device.deviceId = deviceId;
                }

                if (deviceId != resolvedListDevices[indexSelectedDevice].deviceId) {
                    indexSelectedDevice = resolvedListDevices.findIndex(indexInListById);
                    $scope.filter();
                }
            };

            /*
             Tabla
             */
            $scope.dtOptions = DTOptionsBuilder
                .fromFnPromise(promiseListDevices)
                .withDataProp('data')
                //Add Bootstrap compatibility
                .withOption('responsive', true)
                .withOption('order', [[2, "desc"]])
                .withOption('createdRow', createdRow)
                .withDOM("<'dt-toolbar'<' col-sm-6 col-xs-12 hidden-xs'l><'col-sm-6 col-xs-12'f>r>" +
                    "t" +
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap();
            $scope.dtColumns = [
                DTColumnBuilder.newColumn('code').renderWith(function (data, type, full) {
                    return '<button class="btn btn-xs btn-warning" ng-click="loadData(\'' + full.deviceId + '\')"><i class="fa fa-line-chart"></i></button> ' + data;
                }),
                DTColumnBuilder.newColumn('installationStatus').renderWith(function (data) {
                    var classToAdd = 'label-default';
                    if (data.toLowerCase() == 'sin conexión' || data.toLowerCase() == 'desconectado')
                        classToAdd = 'label-danger'
                    if (data.toLowerCase() == 'apagado')
                        classToAdd = 'label-info';
                    if (data.toLowerCase() == 'en linea' && data.toLowerCase() == 'encendido')
                        classToAdd = 'label-success';

                    return '<span class="label ' + classToAdd + '">' + data + '</span>';
                }),
                DTColumnBuilder.newColumn('lastActivity').renderWith(function (data) {
                    return $filter('date')(data, "dd-MM-yyyy HH:mm:ss");
                }),
                DTColumnBuilder.newColumn('polygon.section.name'),
                DTColumnBuilder.newColumn('installationStatus').renderWith(function (data) {
                    var classToAdd = 'label-default';
                    if (data.toLowerCase() == 'sin conexión' || data.toLowerCase() == 'desconectado')
                        classToAdd = 'label-danger'
                    if (data.toLowerCase() == 'apagado')
                        classToAdd = 'label-info';
                    if (data.toLowerCase() == 'en linea' && data.toLowerCase() == 'encendido')
                        classToAdd = 'label-success';

                    return '<span class="label ' + classToAdd + '">' + data + '</span>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }
        }
    ]);