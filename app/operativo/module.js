"use strict";


angular.module('app.operativo', ['ui.router', 'datatables', 'datatables.bootstrap'])
    .config(['$stateProvider', function ($stateProvider) {

        $stateProvider
            .state('app.operativo', {
                url: '/operativo',
                data: {
                    login: true,
                    title: 'Operativo'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/operativo/views/index.html',
                        controller: 'OperativoCtrl'
                    }
                }
            })
    }]);