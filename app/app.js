'use strict';

/**
 * @ngdoc overview
 * @name app [smartadminApp]
 * @description
 * # app [smartadminApp]
 *
 * Main module of the application.
 */

angular.module('app', [
    'ngSanitize',
    'ngAnimate',
    'restangular',
    'ui.router',
    'ui.bootstrap',
    'angular-md5',

    // Smartadmin Angular Common Module
    'SmartAdmin',

    // App
    'app.auth',
    'app.layout',
    //'app.chat',
    //'app.dashboard',
    //'app.calendar',
    //'app.inbox',
    //'app.graphs',
    //'app.tables',
    //'app.forms',
    //'app.ui',
    //'app.widgets',
    //'app.appViews',
    //'app.misc',
    //'app.smartAdmin',
    //'app.eCommerce'
    'app.monitor',
    'app.operativo',
    'app.proyectos',
    'app.admin'
])
    .config(['$provide', '$httpProvider', 'RestangularProvider', '$urlRouterProvider',
        function ($provide, $httpProvider, RestangularProvider, $urlRouterProvider) {

            // Intercept http calls.
            $provide.factory('ErrorHttpInterceptor', ['$q', '$injector', 'sessionService', function ($q, $injector, sessionService) {
                var errorCounter = 0;

                function notifyError(rejection) {
                    if (rejection.status === -1)
                        rejection.data = "Servicio Web no Disponible";

                    if (rejection.status !== 401) {
                        $.bigBox({
                            title: rejection.status + ' ' + rejection.statusText,
                            content: rejection.data,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            number: ++errorCounter,
                            timeout: 6000
                        });
                    }
                }

                return {
                    // On request failure
                    requestError: function (rejection) {
                        // show notification
                        notifyError(rejection);

                        // Return the promise rejection.
                        return $q.reject(rejection);
                    },

                    // On response failure
                    responseError: function (rejection) {
                        // show notification
                        notifyError(rejection);
                        // Return the promise rejection.
                        return $q.reject(rejection);
                    }
                };
            }]);

            $urlRouterProvider.otherwise(function ($injector) {
                if ($injector.get("$state") === false)
                    return "/login";
                else
                    return "/monitoreo";
            });

            // Add the interceptor to the $httpProvider.
            $httpProvider.interceptors.push('ErrorHttpInterceptor');

            RestangularProvider.setBaseUrl(location.pathname.replace(/[^\/]+?$/, ''));

        }
    ])
    .constant('APP_CONFIG', window.appConfig)

    .run(['$rootScope', '$state', '$stateParams', 'sessionService', 'permissionsService', 'DTDefaultOptions',
        function ($rootScope, $state, $stateParams, sessionService, permissionsService, DTDefaultOptions) {
            //DTDefaultOptions es para las opcion por defecto de todas las tablas
            //Lo usamos para asignar un template del mensaje "Loading..." traducido
            DTDefaultOptions.setLoadingTemplate('<h3>Espere un momento...</h3>');
            DTDefaultOptions.setLanguageSource('api/tables/Spanish.json');
            //~
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
            // editableOptions.theme = 'bs3';

            //check if user has permissions on Routes
            $rootScope.$on('$stateChangeStart', function (event, toState) {
                $rootScope.globalShowContent = false;
                // if been login is required
                if (toState.data.login === true && !sessionService.getData()) {
                    event.preventDefault();
                    $state.go('login').then(function () {
                        $state.reload();
                    });
                } else {
                    if (!!toState.permission) {
                        if ((typeof toState.permission) === 'string') {
                            if (!permissionsService.canAccess(toState.permission)) {
                                event.preventDefault();
                                $state.go('app.monitor', {}, {reload: true});
                            }
                            $rootScope.globalShowContent = true;
                        } else
                            $state.go('app.monitor', {}, {reload: true});
                    } else
                        $rootScope.globalShowContent = true;
                }

            });

        }
    ]);