angular.module('app')
    .factory('restService', ['$http', '$q', 'tunnelRestSessionService',
        function ($http, $q, tunnelRestSessionService) {
            var endpoint = "http://209.126.99.20:8085";

            /**
             * aqui se definen todos los request que se pueden hacer en el servidor, son las pagina de las cuales van a extraer los datos
             * se define un arreglo en formato json, y a cada posicion del arreglo se le define una estructura
             * uri que es el path de la url,  type tipo de metodo, content, y si es asyncrono o no.
             */
            var configurations = {
                // Log in, log out y control de password
                "logIn": {
                    "uri": "/oauth/token",
                    "type": "POST",
                    "content": "application/x-www-form-urlencoded",
                    "async": false
                },
                "logOut": {
                    "uri": "/oauth/logout",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "chgPass": {
                    "uri": "/secure/passwordUpdate",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "frgtPass": {
                    "uri": "/users/recover_password",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "rcvrPass": {
                    "uri": "/users/recover_password",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                }
                // Usuarios
                ,
                "userGET": {
                    "uri": "/users",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "userPOST": {
                    "uri": "/users",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "userPUT": {
                    "uri": "/users",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "userDELETE": {
                    "uri": "/users",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Perfiles
                ,
                "profileGET": {
                    "uri": "/profiles",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "profileTREE": {
                    "uri": "/profiles/permissions/tree",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "profilePOST": {
                    "uri": "/profiles",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "profilePUT": {
                    "uri": "/profiles",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "profileDELETE": {
                    "uri": "/api/profiles",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Modulos
                ,
                "moduleGET": {
                    "uri": "/modules",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "modulePOST": {
                    "uri": "/modules",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "modulePUT": {
                    "uri": "/modules",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "moduleDELETE": {
                    "uri": "/modules",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Proyectos
                ,
                "projectGET": {
                    "uri": "/projects",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "projectPOST": {
                    "uri": "/projects",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "projectPUT": {
                    "uri": "/projects",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "projectDELETE": {
                    "uri": "/projects",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Secciones
                ,
                "sectionGET": {
                    "uri": "/sections",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "sectionPOST": {
                    "uri": "/sections",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "sectionPUT": {
                    "uri": "/sections",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "sectionDELETE": {
                    "uri": "/sections",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Poligonos
                ,
                "polygonGET": {
                    "uri": "/polygons",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "polygonPOST": {
                    "uri": "/polygons",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "polygonPUT": {
                    "uri": "/polygons",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "polygonDELETE": {
                    "uri": "/polygons",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                }
                // Circuitos (devices)
                ,
                "deviceGET": {
                    "uri": "/devices",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                },
                "devicePOST": {
                    "uri": "/devices",
                    "type": "POST",
                    "content": "application/json",
                    "async": false
                },
                "devicePUT": {
                    "uri": "/devices",
                    "type": "PUT",
                    "content": "application/json",
                    "async": false
                },
                "deviceDELETE": {
                    "uri": "/devices",
                    "type": "DELETE",
                    "content": "application/json",
                    "async": false
                },
                "deviceRecordsGET": {
                    "uri": "/records",
                    "type": "GET",
                    "content": "application/json",
                    "async": false
                }
            };

            return {
                sendRequest: function ($params, $code, $uriParams, $tokenRefreshed) {
                    var promiseRequest = $q.defer();

                    var service = this;
                    $tokenRefreshed = $tokenRefreshed || false;
                    var authHeader = null;
                    if (!!tunnelRestSessionService.sessionGetData()) {
                        authHeader = {"Authorization": tunnelRestSessionService.sessionGetData().tokenType + ' ' + tunnelRestSessionService.sessionGetData().token};
                    }
                    $http({
                        method: configurations[$code].type,
                        url: endpoint + configurations[$code].uri + ($uriParams || ''),
                        data: $params,
                        //content: configurations[$code].content,
                        headers: angular.extend({}, {"Content-Type": configurations[$code].content || "application/json"}, authHeader)
                    }).then(function (data) {
                        promiseRequest.resolve(data);
                    }, function (error) {
                        if (error.status === 401 || error.status === 403) {
                            if (!$tokenRefreshed) {
                                if (!!tunnelRestSessionService.sessionGetData()) {
                                    tunnelRestSessionService.sessionRefreshToken().then(function () {
                                        service.sendRequest($params, $code, $uriParams, true).then(function (data) {
                                            promiseRequest.resolve(data);
                                        }, function (error) {
                                            tunnelRestSessionService.sessionLogOut();
                                            promiseRequest.reject(error);
                                        });
                                        tunnelRestSessionService.sessionCleaningRefresh();
                                    }, function (error) {
                                        tunnelRestSessionService.sessionLogOut();
                                        tunnelRestSessionService.sessionCleaningRefresh();
                                        promiseRequest.reject(error);
                                    });
                                } else {
                                    tunnelRestSessionService.sessionLogOut();
                                    tunnelRestSessionService.sessionCleaningRefresh();
                                    promiseRequest.reject(error);
                                }
                            } else {
                                tunnelRestSessionService.sessionLogOut();
                                tunnelRestSessionService.sessionCleaningRefresh();
                                promiseRequest.reject();
                            }
                        } else
                            promiseRequest.reject(error);
                    });

                    return promiseRequest.promise;
                }
            };
        }
    ]);