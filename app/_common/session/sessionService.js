angular.module('app')
    .factory('sessionService', ['$q', '$rootScope', '$injector', 'md5', 'tunnelRestSessionService',
        function ($q, $rootScope, $injector, md5, tunnelRestSessionService) {
            var cleanData = {
                name: '',
                username: '',
                token: '',
                tokenType: '',
                tokenRefresh: '',
                permissions: []
            };
            var sessionData = null;
            var inProcess = {
                loggingOut: false,
                refreshingToken: {
                    active: false,
                    promise: null
                }
            };
            return {
                getData: function () {
                    if (!sessionData && !!window.localStorage['mexiledSessionData']) {
                        sessionData = angular.fromJson(window.localStorage['mexiledSessionData']);
                        $rootScope.$broadcast('tokenChanged');
                    }
                    return sessionData;
                },
                cleanLocalData: function () {
                    window.localStorage.removeItem('mexiledSessionData');
                },
                refreshToken: function () {
                    if (!inProcess.refreshingToken.active) {
                        inProcess.refreshingToken.active = true;
                        inProcess.refreshingToken.promise = null;

                        var refreshData = "grant_type=refresh_token&client_id=web-application-access&client_secret=secret&refresh_token=" + this.getData().tokenRefresh;
                        inProcess.refreshingToken.promise = $q.defer();

                        tunnelRestSessionService.restRequest(refreshData, 'logIn', '', true).then(function (data) {
                            data = data.data;

                            sessionData = cleanData;
                            sessionData.name = data.name;
                            sessionData.username = data.username;
                            sessionData.token = data.access_token;
                            sessionData.tokenType = data.token_type;
                            sessionData.tokenRefresh = data.refresh_token;
                            sessionData.permissions = data.roles;

                            window.localStorage['mexiledSessionData'] = JSON.stringify(sessionData);

                            inProcess.refreshingToken.promise.resolve();
                        }, function (error) {
                            inProcess.refreshingToken.promise.reject(error);
                        });
                    }

                    return inProcess.refreshingToken.promise.promise;
                },
                cleanRefreshingToken: function () {
                    inProcess.refreshingToken.active = false;
                },
                login: function (user, pass) {
                    var loginData = "grant_type=password&client_id=web-application-access&client_secret=secret&username=" + user + "&password=" + md5.createHash(pass + '{' + user + '}');
                    var promiseLogIn = $q.defer();

                    tunnelRestSessionService.restRequest(loginData, 'logIn').then(function (data) {
                        data = data.data;

                        sessionData = cleanData;
                        sessionData.name = data.name;
                        sessionData.username = data.username;
                        sessionData.token = data.access_token;
                        sessionData.tokenType = data.token_type;
                        sessionData.tokenRefresh = data.refresh_token;
                        sessionData.permissions = data.roles;

                        window.localStorage['mexiledSessionData'] = JSON.stringify(sessionData);

                        inProcess = {
                            loggingOut: false,
                            refreshingToken: {
                                active: false,
                                promise: null
                            }
                        };

                        promiseLogIn.resolve();
                    }, function (error) {
                        promiseLogIn.reject(error);
                    });

                    return promiseLogIn.promise;
                },
                logout: function () {
                    if (!inProcess.loggingOut) {
                        inProcess.loggingOut = true;
                        var service = this;
                        tunnelRestSessionService.restRequest(null, 'logOut', '', true).finally(function () {
                            service.cleanLocalData();
                            sessionData = null;

                            $injector.get('$state').go('login', {}, {reload: true});
                        });
                    }
                }
            };
        }
    ]);