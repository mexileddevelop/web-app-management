angular.module('app')
    .factory('tunnelRestSessionService', ['$injector', function ($injector) {
        var sessionService;
        var restService;
        return {
            sessionGetData: function () {
                if (!sessionService) { sessionService = $injector.get('sessionService'); }
                return sessionService.getData();
            },
            sessionRefreshToken: function () {
                if (!sessionService) { sessionService = $injector.get('sessionService'); }
                return sessionService.refreshToken();
            },
            sessionCleaningRefresh: function () {
                if (!sessionService) { sessionService = $injector.get('sessionService'); }
                return sessionService.cleanRefreshingToken();
            },
            sessionLogOut: function () {
                if (!sessionService) { sessionService = $injector.get('sessionService'); }
                return sessionService.logout();
            },
            restRequest: function ($params, $code, $uriParams, $tokenRefreshed) {
                if (!restService) { restService = $injector.get('restService'); }
                return restService.sendRequest($params, $code, $uriParams, $tokenRefreshed);
            }
        }
    }]);