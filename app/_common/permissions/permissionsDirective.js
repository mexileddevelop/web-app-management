angular.module('app').directive('hasPermission', ['permissionsService', function (permissionsService) {
    return {
        restrict: 'A',
        compile: function (element) {
            element.removeAttr('has-permission').removeAttr('data-has-permission');
            return {
                post: function (scope, element, attributes) {
                    if (!((typeof attributes.hasPermission) === 'string'))
                        throw 'hasPermission value must be a string'

                    var modules = attributes.hasPermission.trim();

                    function toggleVisibilityBasedOnPermission() {
                        if (!permissionsService.hasPermission(modules))
                            element.remove();
                    }

                    toggleVisibilityBasedOnPermission();
                    scope.$on('tokenChanged', toggleVisibilityBasedOnPermission);
                }
            };
        }
    };
}]);