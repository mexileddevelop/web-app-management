angular.module('app').factory('permissionsService', ['sessionService', function (sessionService) {
    return {
        /**
         *
         * @returns {[string,string]}
         *      string array with user permissions
         */
        getPermissions: function () {
            return sessionService.getData().permissions;
        },

        /**
         *
         * @param Modules
         *      string with requested comma separated modules
         *      @format module_submodule
         * @returns {boolean}
         *      true if the user has permission of some requested modules
         */
        hasPermission: function (modules) {
            if (!!modules) {
                modules = modules.replace(/\./g, '_').split(','); //Replace dots (.) by underscore (_) and akes an array of modules

                return modules.some(this.evaluateModulesPermission, this.getPermissions());
            }
        },

        evaluateModulesPermission: function(module) {
            var searchPattern, hasPermission; //Variables to save the search pattern and a boolean if the permission was found

            module = module.trim().toUpperCase(); //Clear white spaces and makes string uppercase

            var notPermissionFlag = (module[0] === '!'); //Check if there is a not operator in the module (true/false)
            if (notPermissionFlag)
                module = module.slice(1).trim();

            searchPattern = new RegExp('\\b' + module + '\\b');

            // this: sended by some() function in hasPermission as second parameter
            hasPermission = !!this.find(function (permission) {
                return (permission.search(searchPattern) != -1 );
            });

            return hasPermission!==notPermissionFlag;
        },

        /**
         *
         * @param permissionRequest
         *      string with requested comma separated modules
         *      @format module.submodule
         * @returns {boolean}
         *      true if the user has some requested permission
         */
        canAccess: function (permissionRequest) {
            if (!!permissionRequest) {
                permissionRequest = permissionRequest.trim();

                return this.hasPermission(permissionRequest);
            }
        }
   };
}]);