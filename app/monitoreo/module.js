"use strict";


angular.module('app.monitor', ['ui.router',
    'uiGmapgoogle-maps'
])
//.config(function(uiGmapGoogleMapApiProvider) {
//    uiGmapGoogleMapApiProvider.configure({
//        //    key: 'your api key',
//        v: '3.20', //defaults to latest 3.X anyhow
//        libraries: 'weather,geometry,visualization'
//    });
//})


angular.module('app.monitor')
    .config(['$stateProvider', 'uiGmapGoogleMapApiProvider', function ($stateProvider, uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyDk7Kg1cEqiRUEyYCXN6KWRkejGRMwHkzc',
            v: '3.20', //defaults to latest 3.X anyhow
            libraries: 'weather,geometry,visualization'
        });

        $stateProvider
            .state('app.monitor', {
                url: '/monitoreo',
                data: {
                    login: true,
                    title: 'Estatus de Infraestructura'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/monitoreo/views/index.html',
                        controller: 'MonitoreoCtrl'
                    }
                },
                resolve: {
                    scripts: function (lazyScript) {
                        return lazyScript.register([
                            'build/vendor.graphs.js'
                        ]);
                    }
                }
            })
    }]);