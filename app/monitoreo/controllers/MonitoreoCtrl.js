'use strict';

angular.module('app.monitor')
    .controller('MonitoreoCtrl', ['$scope', '$filter', '$compile', 'uiGmapGoogleMapApi', 'restService',
        function ($scope, $filter, $compile, uiGmapGoogleMapApi, restService) {
            $scope.markersCircuits = []; // Map Markers
            var centerCoords = null; // Device to center the map
            $scope.graphOperacionCircuitos = [
                {
                    name: "Proyectados",
                    data: [0],
                    color: "#9F9F9F"
                },
                {
                    name: "Desconectado",
                    data: [0],
                    color: "#E90808"
                },
                {
                    name: "Apagado",
                    data: [0],
                    color: "#49ADE8"
                },
                {
                    name: "Encendido",
                    data: [0],
                    color: "#83B925"
                }
            ]; // Graph
            $scope.graphAlarmasSistema = [
                {
                    name: "Con alerta",
                    data: [0],
                    color: "#EA0E24"
                },
                {
                    name: "Con aviso",
                    data: [0],
                    color: "#F9EA24"
                },
                {
                    name: "Sin alerta",
                    data: [0],
                    color: "#BFBFBF"
                }
            ]; // Graph

            /*
             defaultColors = {
             "OK": "#F99C25",
             "Disconnected": "#25B1C4",
             "Warning": "#F9EA24",
             "Alert": "#EA0E24",
             "StanBy": "#8E8E8E"
             };
             */
            function byLabelName(el) {
                if (el.hasOwnProperty('name'))
                    return el.name.toLowerCase() == this;
                if (el.hasOwnProperty('label'))
                    return el.label.toLowerCase() == this;
            }
            
            restService.sendRequest(null, 'deviceGET').then(function (data) {
                data = data.data;
                for (var i = 0; i < data.length; i++) {
                    if (!centerCoords && data[i].installationStatus.toLowerCase() != 'proyectado')
                        centerCoords = data[i];

                    var icon;

                    switch (data[i].installationStatus.toLowerCase()) {
                        case 'encendido':
                            icon = 'styles/img/mexiled/mapMarkerEncendido.png';
                            break;
                        case 'apagado':
                            icon = 'styles/img/mexiled/mapMarkerApagado.png';
                            break;
                        case 'proyectado':
                            icon = 'styles/img/mexiled/mapMarkerProyectado.png';
                            break;
                        default:
                            icon = 'styles/img/mexiled/mapMarkerVarios.png';
                            break;
                    }

                    /* Graficas */
                    // Operacion de circuitos
                    var index = -1;
                    index = $scope.graphOperacionCircuitos.findIndex(byLabelName, data[i].installationStatus.toLowerCase());
                    if (index >= 0) {
                        if ($scope.graphOperacionCircuitos[index].hasOwnProperty('value'))
                            $scope.graphOperacionCircuitos[index].value++;
                        if ($scope.graphOperacionCircuitos[index].hasOwnProperty('data'))
                            $scope.graphOperacionCircuitos[index].data[0]++;
                    }
                    // Alarmas de sistema
                    index = -1;
                    if (!!data[i].alert)
                        index = $scope.graphAlarmasSistema.findIndex(byLabelName, 'con alerta');
                    else if (!!data[i].warning)
                        index = $scope.graphAlarmasSistema.findIndex(byLabelName, 'con aviso');
                    else
                        index = $scope.graphAlarmasSistema.findIndex(byLabelName, 'sin alerta');
                    if (index >= 0) {
                        if ($scope.graphAlarmasSistema[index].hasOwnProperty('value'))
                            $scope.graphAlarmasSistema[index].value++;
                        if ($scope.graphAlarmasSistema[index].hasOwnProperty('data'))
                            $scope.graphAlarmasSistema[index].data[0]++;
                    }
                    /* ~ */

                    $scope.markersCircuits.push({
                        id: i + 1,
                        coords: {
                            latitude: data[i].latitude,
                            longitude: data[i].longitude
                        },
                        options: {
                            icon: {
                                url: icon,
                                scaledSize: {
                                    height: 9,
                                    width: 9
                                }
                            }
                        },
                        iwData: '<div class="info-box-wrap">' +
                            '<h3 class="title"><a>' + data[i].code + '</a></h3>' +
                            '<span><b>Número de lamparas:</b> ' + data[i].lamps + '</span><br>' +
                            '<span><b>Ultimo registro:</b> ' + $filter('date')(data[i].lastActivity, "dd-MM-yyyy HH:mm:ss") + '</span><br>' +
                            '<span><b>Estatus:</b> ' + data[i].installationStatus + '</span>' +
                            '</div>'
                    });
                }

                var graphs = angular.element(document).find('div[stackbar-chart]');
                for (var i = 0; i < graphs.length; i++) {
                    $compile(graphs[i])($scope);
                }
            });
            /* Mapa Data */
            if (!!centerCoords)
                centerCoords = {latitude: centerCoords.latitude, longitude: centerCoords.longitude};
            else
                centerCoords = {latitude: 20.6322137, longitude: -103.2776288};

            $scope.map = {
                center: centerCoords,
                zoom: 12,
                control: {},
                options: {
                    scrollwheel: false
                },
                markersEvents: {
                    click: function (marker, eventName, model) {
                        $scope.map.infoWindow.model = model;
                        $scope.map.infoWindow.show = true;
                    }
                },
                infoWindow: {
                    model: null,
                    show: false,
                    closeClock: function () {
                        this.show = false;
                    }
                }
            };

            uiGmapGoogleMapApi.then(function (maps) {});
        }
    ]);