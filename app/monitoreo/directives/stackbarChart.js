'use strict';

angular.module('app.monitor').directive('stackbarChart', function () {
    return {
        restrict: 'A',
        scope: {
            data: '='
        },
        link: function (scope, element) {
            var defaultColors = [
                "#F99C25", // Encendido
                "#25B1C4", // Apagado
                "#EA0E24", // Desconectado
                "#8E8E8E"  // Proyectados
            ];

            for (var i = 0; i < scope.data.length; i++) {
                if (!scope.data[i].color)
                    scope.data[i].color = defaultColors[i % defaultColors.length];
            }

            Highcharts.chart(element[0], {
                chart: {
                    type: 'bar'
                },
                credits: {
                    enabled: false
                },
                legend: {
                    reversed: true,
                    enabled: false
                },
                title: {
                    text: ''
                },
                xAxis: {
                    visible: false,
                },
                yAxis: {
                    visible: false
                },
                plotOptions: {
                    series: {
                        stacking: 'normal'
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<span style="color:' + this.point.color + '">\u25CF</span> ' + this.series.name + ': <b>' + this.percentage + '%</b> (' + this.point.y + ')<br/>'
                    }
                },
                series: scope.data
            });
        }
    }
});