"use strict";


angular.module('app.layout', ['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('app', {
                abstract: true,
                views: {
                    root: {
                        templateUrl: 'app/layout/layout.tpl.html'
                    }
                }
            });
        $urlRouterProvider.otherwise('/');

    }]);