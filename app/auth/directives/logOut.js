"use strict";

angular.module('app').directive('userLogout', ['sessionService', function (sessionService) {
    return {
        restrict: "EA",
        replace: false,
        link: function (scope, element) {
            element.bind('click', function () {
                sessionService.logout();
            });
        }
    }
}]);