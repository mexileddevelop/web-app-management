angular.module('app.auth')
    .controller('registerCtrl', ['$scope', '$state', 'restService', 'md5',
        function ($scope, $state, restService, md5) {
            $scope.user = {
                userId: null,
                profileId: 2,
                name: '',
                username: '',
                email: '',
                validEmail: true,
                password: '',
                passwordConfirm: '',
                active: true
            };

            $scope.registerError = false;

            // Email validator
            $('input[type=email]').on('keyup change input paste cut', function (ev) {
                var element = ev.target;
                $scope.user.validEmail = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($(element).val());
            });

            $scope.registrar = function () {
                $scope.register.submited = true;
                $scope.user.validEmail = (!!$scope.user.email && /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($scope.user.email));
                if (!!$scope.user.name && $scope.user.validEmail && !!$scope.user.password && $scope.user.password === $scope.user.passwordConfirm) {
                    $scope.registerError = false;
                    $scope.user.username = $scope.user.email;

                    var tmpPassword = $scope.user.password;
                    $scope.user.password = md5.createHash($scope.user.password + '{' + $scope.user.email + '}');
                    $scope.user.passwordConfirm = $scope.user.password;

                    restService.sendRequest($scope.user, 'userPOST').then(function () {
                        $.smallBox({
                            title: "Registro completado",
                            content: "Se realizo el registro de su cuenta exitosamente, ahora puede utilizar sus datos para ingresar.",
                            color: "#C46A69",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 5000
                        });

                        $state.go('login');
                    }, function () {
                        $scope.user.password = tmpPassword;
                        $scope.user.passwordConfirm = $scope.user.password;

                        $.smallBox({
                            title: "Error al registrar",
                            content: "Su registro no pudo completarse en estos momentos, vuelva a intentarlo mas tarde.",
                            color: "#C46A69",
                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                            timeout: 4000
                        });
                    });
                } else
                    $scope.registerError = true;
            };
        }
    ]);