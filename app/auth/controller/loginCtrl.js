angular.module('app.auth')
    .controller('loginCtrl', ['$scope', '$state', 'sessionService',
        function ($scope, $state, sessionService) {
            $scope.user = {
                value: '',
                valid: true
            };
            $scope.pass = {
                value: '',
                valid: true
            };
            $scope.loginError = false;

            sessionService.cleanLocalData();

            $scope.ingresar = function () {
                $scope.user.valid = (!!$scope.user.value && /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($scope.user.value));
                $scope.pass.valid = (!!$scope.pass.value && $scope.pass.value.length >= 6);
                if ($scope.user.valid && $scope.pass.valid) {
                    sessionService.login($scope.user.value, $scope.pass.value).then(function () {
                        $scope.loginError = false;
                        $state.go('app.monitor');
                    }, function (error) {
                        $scope.loginError = true;
                    });
                }
            };
            // Olvido contraseña
            $scope.forgotPass = function () {
                $scope.formForgotPass.submited = true;
                if ($scope.formForgotPass.$valid) {
                    if ($scope.email !== '') {
                        RestRequest.getRequestNoToken($http, null, 'frgtPass', '?username=' + $scope.email).then(
                            function () {
                                $('#modalForgotPass').dialog('close');
                                $.smallBox({
                                    title: "Solicitud enviada",
                                    content: "Se ha enviado a su correo las instrucciones para continuar con su solicitud de cambio de contraseña.",
                                    color: "#739E73",
                                    iconSmall: "fa fa-check fadeInRight animated",
                                    timeout: 4000
                                });
                            }
                        );
                    }
                }
            };

            // Limpia los modal dialog existentes en dom para su correcto funcionamiento al loggear
            $('div.ui-dialog').each(function () {
                this.parentNode.removeChild(this);
            });
        }
    ]);