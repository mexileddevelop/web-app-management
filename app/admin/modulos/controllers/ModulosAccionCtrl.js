angular.module('app.admin')
    .controller('ModulosAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.module = {
                moduleId: null,
                name: '',
                namePermission: '',
                codePermission: '',
                permissions: [] // {modulePermissionId, name, code}, {...}
            };

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('module.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('module.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('module.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'moduleGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;
                        $scope.module.moduleId = +data.moduleId;
                        $scope.module.name = data.name;
                        for (var i = 0; i < data.permissions.length; i++) {
                            $scope.module.permissions.push($.extend(data.permissions[i], {
                                updating: false,
                                error: ''
                            }));
                        }

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar el modulo",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            $scope.addPermission = function () {
                $scope.form.submited = true;
                if (!!$scope.module.namePermission && !!$scope.module.codePermission) {
                    $scope.module.codePermission = $scope.module.codePermission.toUpperCase();

                    if ($scope.module.permissions.findIndex(function (permission) {
                            return permission !== null && permission.code == $scope.module.codePermission
                        }) < 0) {
                        $scope.module.permissions.push({
                            modulePermissionId: null,
                            name: $scope.module.namePermission,
                            code: $scope.module.codePermission,
                            updating: false,
                            error: ''
                        });
                        $scope.module.namePermission = '';
                        $scope.module.codePermission = '';
                        $scope.duplexPermission = false;
                    }
                    else
                        $scope.duplexPermission = true;
                }
                else
                    $scope.duplexPermission = false;
            };
            $scope.updatePermission = function (index) {
                var indexEditing;
                var currentEditing = $scope.module.permissions.find(function (permission, index) {
                    indexEditing = index;
                    return permission !== null && permission.updating == true
                });
                if (!!currentEditing) {
                    if (currentEditing.name === '' || currentEditing.code === '') {
                        currentEditing.error = 'Ingresa la información completa del permiso';
                        return;
                    }
                    else {
                        currentEditing.code = currentEditing.code.toUpperCase();
                        var indexFound = $scope.module.permissions.find(function (permission, index) {
                            return permission !== null && (permission.code == currentEditing.code && index != indexEditing)
                        });
                        if (!indexFound) {
                            currentEditing.error = '';
                            currentEditing.updating = false;
                        }
                        else {
                            currentEditing.error = 'Permiso duplicado.';
                            return;
                        }
                    }
                }
                else
                    indexEditing = undefined;
                if (index !== undefined && index !== indexEditing)
                    $scope.module.permissions[index].updating = !$scope.module.permissions[index].updating;
            };
            $scope.deletePermission = function (index) {
                $scope.module.permissions[index] = null;
            };
            $scope.cancel = function () {
                $state.go('app.admin.modulos');
                return false;
            };
            $scope.save = function () {
                $scope.updatePermission();
                $scope.addPermission();

                $scope.module.permissions = $scope.module.permissions.filter(function (permission) {
                    return permission !== null
                });

                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                }
                else if (
                    !!$scope.module.name && !$scope.module.namePermission && !$scope.module.codePermission
                    && !!$scope.module.permissions.length
                    && $scope.module.permissions.findIndex(function (permission) {
                        return permission.error != ''
                    }) < 0
                ) {
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('module.add')) {
                                restService.sendRequest($scope.module, 'modulePOST').then(function () {
                                    $.bigBox({
                                        title: "Nuevo modulo agregado",
                                        content: "Se guardo la información del modulo: <b>" + $scope.module.name + "</b>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del modulo';

                                    $.bigBox({
                                        title: "Error al guardar modulo",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('module.edit')) {
                                restService.sendRequest($scope.module, 'modulePUT', '/' + $scope.module.moduleId).then(function () {
                                    $.bigBox({
                                        title: "Modulo actualizado",
                                        content: "Se actualizo la información del modulo: <b>" + $scope.module.name + "</b>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del modulo';

                                    $.bigBox({
                                        title: "Error al guardar modulo",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);