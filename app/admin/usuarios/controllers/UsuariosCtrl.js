angular.module('app.admin')
    .controller('UsuariosCtrl', ['$scope', '$compile', '$state', 'DTOptionsBuilder', 'DTColumnBuilder', 'permissionsService', 'restService',
        function ($scope, $compile, $state, DTOptionsBuilder, DTColumnBuilder, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.statusFilter = {
                'true': 'Activo',
                'false': 'Inactivo'
            };

            /* Datatable (DT), creacion y asignacion de eventos */
            var dataArrayResolved = [];
            $scope.standardOptions = DTOptionsBuilder
                .fromFnPromise(restService.sendRequest(null, 'userGET'))
                .withDataProp('data')
                .withDOM("t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap()
                .withOption('aaSorting', [[1, 'asc'], [0, 'asc']])
                .withOption('createdRow', createdRow);
            $scope.standardColumns = [
                DTColumnBuilder.newColumn('name').notSortable(),
                DTColumnBuilder.newColumn('email').notSortable(),
                DTColumnBuilder.newColumn('active').notSortable().renderWith(function (data) {
                    return $scope.statusFilter[data];
                }),
                DTColumnBuilder.newColumn(null).notSortable().withClass('table-actions-buttons').renderWith(function (data) {
                    dataArrayResolved[data.userId] = data.name;
                    return '<a has-permission="USER_VIEW" title="Ver" ui-sref="app.admin.usuarios.accion({accion: \'ver\', id: ' + data.userId + '})" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>' +
                        '<a has-permission="USER_EDIT" title="Editar" ui-sref="app.admin.usuarios.accion({accion: \'editar\', id: ' + data.userId + '})" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>' +
                        '<a has-permission="USER_EDIT" title="' + (data.active ? 'Desactivar' : 'Activar') + '" ng-click="accion(\'' + (data.active ? 'deactivate' : 'activate') + '\', ' + data.userId + ')" class="btn btn-default btn-xs"><i class="fa ' + (data.active ? 'fa-ban' : 'fa-check-circle-o') + '"></i></a>' +
                        '<a has-permission="USER_DELETE" title="Eliminar" ng-click="accion(\'delete\', ' + data.userId + ')" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }

            // Callback para funcionalidad de filtros
            $scope.instanceCallback = function (dtInstance) {
                $scope.dtInstance = dtInstance;
                var table = dtInstance.DataTable;
                table.columns().eq(0).each(function (colIdx) {
                    $('input, select', table.column(colIdx).header()).on('keyup change', function () {
                        if (this.getAttribute('id') == 'table-lenght-ctrl') {
                            table
                                .page.len(this.value)
                                .draw();
                        }
                        else {
                            var isSelect = (this.nodeName == 'SELECT');
                            var searchValue = (isSelect && this.value !== '') ? '\\b' + this.value + '\\b' : this.value;
                            table
                                .column(colIdx)
                                .search(searchValue, isSelect, !isSelect)
                                .draw();
                        }
                    });
                });
            };

            $scope.accion = function (accion, id) {
                switch (accion) {
                    case 'delete':
                    case 'eliminar':
                        if (permissionsService.canAccess('user.delete')) {
                            $.SmartMessageBox({
                                title: "Borrar Usuario",
                                content: "Esta a punto de borrar la información del usuario <b>" + dataArrayResolved[id] + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'userDELETE', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se borro la información del usuario <b>" + dataArrayResolved[id] + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'userGET'));
                                        else
                                            $state.reload();
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo borrar el usuario solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                    case 'activate':
                    case 'deactivate':
                        if (permissionsService.canAccess('user.edit')) {
                            var active = (accion.toLowerCase() === 'activate');
                            $.SmartMessageBox({
                                title: (active ? 'Activar' : 'Desactivar') + " Usuario",
                                content: "Esta a punto de " + (active ? 'activar' : 'desactivar') + " el usuario <b>" + dataArrayResolved[id] + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'userPUT', '/' + id + '/' + active).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se " + (active ? 'activo' : 'desactivo') + " el usuario <b>" + dataArrayResolved[id] + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'userGET'));
                                        else
                                            $state.reload();
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo " + (active ? 'activar' : 'desactivar') + " el usuario solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                }
            };
        }
    ]);