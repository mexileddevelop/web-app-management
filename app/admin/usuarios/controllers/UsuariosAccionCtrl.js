angular.module('app.admin')
    .controller('UsuariosAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'md5', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, md5, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.user = {
                userId: null,
                profileId: null,
                name: '',
                username: '',
                email: '',
                validEmail: false,
                realEmail: '',
                password: '',
                passwordConfirm: '',
                active: ''
            };

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('user.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    $scope.user.active = 'true';
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('user.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('user.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'userGET', '/findById/' + $stateParams.id).then(function (data) {
                        data = data.data;

                        $scope.user.userId = data.userId;
                        $scope.user.profileId = '' + data.profileId;
                        $scope.user.name = data.name;
                        $scope.user.email = data.email;
                        $scope.user.realEmail = data.email;
                        $scope.user.validEmail = true;
                        $scope.user.active = '' + data.active;

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar el usuario",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }
            // Number input
            $('input[type=number]').on('keydown', function (e) {
                if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57) || e.keyCode == 107 || e.keyCode == 187 || e.keyCode == 8 || e.keyCode == 46)
                    return true;
                else
                    return false;
            });

            // Email validator
            $('input[type=email]').on('keyup change input paste cut', function (ev) {
                var element = ev.target;
                $scope.user.validEmail = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test($(element).val());
            });

            $scope.listOfProfiles = [];
            restService.sendRequest(null, 'profileGET').then(function (data) {
                $scope.listOfProfiles = data.data;
            }, function () {
                $.bigBox({
                    title: "Error al cargar perfiles",
                    content: "No se pudo cargar la lista de perfiles",
                    color: "#C46A69",
                    icon: "fa fa-warning shake animated",
                    timeout: 6000
                });
                $scope.cancel();
            });

            $scope.cancel = function () {
                $state.go('app.admin.usuarios');
                return false;
            };

            // Guardar informacion, crea o edita un usuario con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                    $scope.form.passwordConfirm.$setTouched();
                } else {
                    var tmpPassword;
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('user.add')) {
                                if (!!$scope.user.password && $scope.user.passwordConfirm && $scope.user.password === $scope.user.passwordConfirm) {
                                    $scope.user.username = $scope.user.email;
                                    $scope.user.active = ($scope.user.active.toLowerCase() === 'true');

                                    tmpPassword = $scope.user.password;
                                    $scope.user.password = md5.createHash($scope.user.password + '{' + $scope.user.email + '}');
                                    $scope.user.passwordConfirm = $scope.user.password;

                                    restService.sendRequest($scope.user, 'userPOST').then(function () {
                                        $.bigBox({
                                            title: "Nuevo usuario agregado",
                                            content: "Se guardo la información del usuario: <b>" + $scope.user.name + "</b> - <i>" + $scope.user.email + "</i>",
                                            color: "#739E73",
                                            icon: "fa fa-check",
                                            timeout: 8000
                                        });
                                        $scope.cancel();
                                    }, function (error) {
                                        $scope.user.password = tmpPassword;
                                        $scope.user.passwordConfirm = $scope.user.password;

                                        var errorMsg;
                                        if (!!error.code) {
                                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                                            else errorMsg += error.message;
                                        }
                                        else errorMsg = 'No se pudo guardar la información del usuario';

                                        $.bigBox({
                                            title: "Error al guardar el usuario",
                                            content: errorMsg,
                                            color: "#C46A69",
                                            icon: "fa fa-warning shake animated",
                                            timeout: 6000
                                        });
                                    });
                                }
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('user.edit')) {
                                $scope.user.email = $scope.user.realEmail;

                                var passwordOK = true;
                                tmpPassword = $scope.user.password;
                                if (!!$scope.user.password) {
                                    if ($scope.user.password === $scope.user.passwordConfirm) {
                                        $scope.user.password = md5.createHash($scope.user.password + '{' + $scope.user.email + '}');
                                        $scope.user.passwordConfirm = $scope.user.password;
                                    } else {
                                        $scope.form.passwordConfirm.$setTouched();
                                        passwordOK = false;
                                    }
                                }

                                if (passwordOK) {
                                    $scope.user.username = $scope.user.email;
                                    $scope.user.active = ($scope.user.active.toLowerCase() === 'true');
                                    restService.sendRequest($scope.user, 'userPUT', '/' + $scope.user.userId).then(function () {
                                        $.bigBox({
                                            title: "Usuario actualizado",
                                            content: "Se actualizo la información del usuario: <b>" + $scope.user.name + "</b> - <i>" + $scope.user.email + "</i>",
                                            color: "#739E73",
                                            icon: "fa fa-check",
                                            timeout: 8000
                                        });
                                        $scope.cancel();
                                    }, function (error) {
                                        $scope.user.password = tmpPassword;
                                        $scope.user.passwordConfirm = $scope.user.password;

                                        var errorMsg;
                                        if (!!error.code) {
                                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                                            else errorMsg += error.message;
                                        }
                                        else errorMsg = 'No se pudo guardar la información del usuario';

                                        $.bigBox({
                                            title: "Error al guardar el usuario",
                                            content: errorMsg,
                                            color: "#C46A69",
                                            icon: "fa fa-warning shake animated",
                                            timeout: 6000
                                        });
                                    });
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);