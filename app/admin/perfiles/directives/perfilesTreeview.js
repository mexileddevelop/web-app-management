angular.module('app.admin')
    .directive('perfilesTreeview', ['$compile', function ($compile) {
        return {
            restrict: 'A',
            scope: {
                'items': '=',
                'readonly': '='
            },
            template: '<li ng-class="{parent_li: item.children.length}" ng-repeat="item in items" role="treeitem">' +
            '<perfiles-treeview-content></perfiles-treeview-content>' +
            '<ul ng-if="item.children.length" perfiles-treeview ng-show="item.expanded" items="item.children" role="group" class="smart-treeview-group"></ul>' +
            '</li>',
            compile: function (element, attributes) {
                // Break the recursion loop by removing the contents
                var contents = element.contents().remove();
                var compiledContents;
                return {
                    post: function (scope, element) {
                        // Compile the contents
                        if (!compiledContents) {
                            compiledContents = $compile(contents);
                        }
                        // Re-add the compiled contents to the element
                        compiledContents(scope, function (clone) {
                            element.append(clone);
                        });
                    }
                };
            }
        };
    }])
    .directive('perfilesTreeviewContent', function () {
        return {
            restrict: 'E',
            link: function (scope, element) {
                var iconOpenToggle = '', iconFolder = '', isPermission = '';
                if (scope.item.module || (scope.item.children && scope.item.children.length)) {
                    iconOpenToggle = '<i class="fa fa-lg fa-caret-right"></i>';
                    iconFolder = '<i class="fa fa-folder"></i>';
                }
                if (!scope.item.module) {
                    isPermission = 'permission';
                }
                var htmlCheckbox = '<span>' +
                    iconOpenToggle +
                    '<input type="checkbox"' +
                    'value="' + scope.item.id + '"';
                if (scope.item.active) {
                    htmlCheckbox += 'checked ';
                }
                if (scope.item.readonly) {
                    htmlCheckbox += 'disabled ';
                }
                htmlCheckbox += isPermission + '>' +
                    iconFolder +
                    scope.item.content +
                    '</span>';
                var $content = $(htmlCheckbox);

                function handleExpanded() {
                    $content.find('>i:first-child')
                        .toggleClass('fa-caret-right', !scope.item.expanded)
                        .toggleClass('fa-caret-down', !!scope.item.expanded);
                }

                if (scope.item.children && scope.item.children.length) {
                    $content.on('click', function () {
                        scope.$apply(function () {
                            scope.item.expanded = !scope.item.expanded;
                            handleExpanded();
                        });
                    });
                    $content.find('input').on('click', function (e) {
                        $(e.target.parentNode.parentNode).find('input:checkbox').each(function () {
                            this.checked = e.target.checked;
                        });
                        e.stopPropagation();
                    });

                    handleExpanded();
                }
                $content.find('input').on('click', function (e) {
                    var ulNodeClicked = e.target.parentNode.parentNode.parentNode;
                    toggleParentCheckboxOnChildrens(ulNodeClicked);
                });
                function toggleParentCheckboxOnChildrens(nodeOfCheckbox) {
                    if ($(nodeOfCheckbox).attr('role') !== 'tree') {
                        var childrensChecked = true;
                        $(nodeOfCheckbox).children().each(function () {
                            if (childrensChecked)
                                childrensChecked = this.children[0].getElementsByTagName('input')[0].checked;
                        });
                        nodeOfCheckbox.previousElementSibling.getElementsByTagName('input')[0].checked = childrensChecked;
                        toggleParentCheckboxOnChildrens(nodeOfCheckbox.parentNode.parentNode);
                    }
                }

                element.replaceWith($content);
            }
        }
    });