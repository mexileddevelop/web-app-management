angular.module('app.admin')
    .controller('PerfilesAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.profile = {
                profileId: null,
                profileName: '',
                permissions: [], // [ <int>, <int>, ... ]
                tree: []
            };

            // Revisar permisos y evaluar solo letura de los campos
            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('profile.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    restService.sendRequest(null, 'profileTREE').then(function (data) {
                        data = data.data;
                        if (data.length < 1) {
                            $.bigBox({
                                title: "No existen modulos",
                                content: "No existen modulos de permisos para seleccionar, <br>agregue algun modulo desde el menu de <b>Modulos</b>",
                                color: "#C46A69",
                                icon: "fa fa-warning shake animated",
                                timeout: 6000
                            });
                            $scope.cancel();
                        }
                        $scope.profile.tree = data;

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar el arbol de permisos",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('profile.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('profile.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'profileGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;
                        $scope.profile.profileId = +data.profileId;
                        $scope.profile.profileName = data.profileName;
                        $scope.profile.tree = data.tree;
                        $scope.profile.tree.forEach(ifAllChildrenAreSelected);
                        if ($scope.readOnly)
                            readonlyTree($scope.profile.tree);

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar perfil",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            function ifAllChildrenAreSelected(node) {
                if (!node.active) {
                    var allChildrenSelected = true;
                    if (node.children && !!node.children.length) {
                        for (var i = 0; i < node.children.length; i++) {
                            if (!!node.children[i].children.length)
                                node.children[i].children.forEach(ifAllChildrenAreSelected);
                            if (allChildrenSelected)
                                allChildrenSelected = node.children[i].active;
                        }
                    }
                    node.active = allChildrenSelected;
                }
            }

            function readonlyTree(permissionNode) {
                permissionNode.forEach(function (node) {
                    node.readonly = $scope.readOnly;
                    if (node.children && !!node.children.length)
                        readonlyTree(node.children);
                });
            }

            $scope.cancel = function () {
                $state.go('app.admin.perfiles');
                return false;
            };

            // Guardar informacion, crea o edita un perfil con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                // Revisar que el formulario no tenga errores
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                } else {
                    $scope.permissions = [];
                    $(document.form).find('input:checkbox[permission]:checked').each(function () {
                        $scope.profile.permissions.push(+this.value);
                    });

                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('profile.add')) {
                                restService.sendRequest($scope.profile, 'profilePOST').then(function () {
                                    $.bigBox({
                                        title: "Nuevo perfil agregado",
                                        content: "Se agrego el perfil: <b>" + $scope.profile.profileName + "</b>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del perfil';

                                    $.bigBox({
                                        title: "Error al guardar perfil",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('profile.edit')) {
                                $scope.profile.profileId = $stateParams.id;
                                restService.sendRequest($scope.profile, 'profilePUT', '/' + $scope.profile.profileId).then(function () {
                                    $.bigBox({
                                        title: "Perfil actualizado",
                                        content: "Se actualizo el perfil: <b>" + $scope.profile.profileName + "</b>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del perfil';

                                    $.bigBox({
                                        title: "Error al guardar perfil",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);