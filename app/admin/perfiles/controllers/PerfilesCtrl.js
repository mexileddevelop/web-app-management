angular.module('app.admin')
    .controller('PerfilesCtrl', ['$scope', '$compile', '$state', 'DTOptionsBuilder', 'DTColumnBuilder', 'permissionsService', 'restService',
        function ($scope, $compile, $state, DTOptionsBuilder, DTColumnBuilder, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;

            /* Datatable (DT), creacion y asignacion de eventos */
            var dataArrayResolved = [];
            $scope.standardOptions = DTOptionsBuilder
                .fromFnPromise(restService.sendRequest(null, 'profileGET'))
                .withDataProp('data')
                .withDOM("t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap()
                .withOption('createdRow', createdRow);
            $scope.standardColumns = [
                DTColumnBuilder.newColumn('profileName').notSortable(),
                DTColumnBuilder.newColumn(null).notSortable().withClass('table-actions-buttons').renderWith(function (data) {
                    dataArrayResolved[data.profileId] = data.profileName;
                    return '<a has-permission="PROFILE_VIEW" title="Ver" ui-sref="app.admin.perfiles.accion ({accion: \'ver\', id: ' + data.profileId + '})" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>' +
                        '<a has-permission="PROFILE_EDIT" title="Editar" ui-sref="app.admin.perfiles.accion({accion: \'editar\', id: ' + data.profileId + '})" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>' +
                        '<a has-permission="PROFILE_DELETE" title="Eliminar" ng-click="accion(\'delete\',' + data.profileId + ')" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }

            // Callback para funcionalidad de filtros
            $scope.instanceCallback = function (dtInstance) {
                $scope.dtInstance = dtInstance;
                var table = dtInstance.DataTable;
                table.columns().eq(0).each(function (colIdx) {
                    $('input, select', table.column(colIdx).header()).on('keyup change', function () {
                        if (this.getAttribute('id') == 'table-lenght-ctrl') {
                            table
                                .page.len(this.value)
                                .draw();
                        }
                        else {
                            var isSelect = (this.nodeName === 'SELECT');
                            var searchValue = (isSelect && this.value !== '') ? '\\b' + this.value + '\\b' : this.value;
                            table
                                .column(colIdx)
                                .search(searchValue, isSelect, !isSelect)
                                .draw();
                        }
                    });
                });
            };

            $scope.accion = function (accion, id) {
                switch (accion) {
                    case 'delete':
                    case 'eliminar':
                        if (permissionsService.canAccess('profile.delete')) {
                            $.SmartMessageBox({
                                title: "Borrar Perfil",
                                content: "Esta a punto de borrar la información del perfil <b>" + dataArrayResolved[id] + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'profileDELETE', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se borro la información del perfil <b>" + dataArrayResolved[id] + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'profileGET'));
                                        else
                                            $state.reload();
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo borrar el perfil solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                }
            };
        }
    ]);