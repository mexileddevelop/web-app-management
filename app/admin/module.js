"use strict";

angular.module('app.admin', ['ui.router', 'datatables', 'datatables.bootstrap'])
    .config(['$stateProvider', function ($stateProvider) {

        $stateProvider
            .state('app.admin', {
                abstract: true,
                data: {
                    login: true,
                    title: 'Administración'
                }
            })

            .state('app.admin.usuarios', {
                cache: false,
                url: '/admin/usuarios',
                permission: 'user.view, user.add, user.edit, user.delete',
                data: {
                    login: true,
                    title: 'Usuarios'
                },
                views: {
                    "content@app": {
                        controller: 'UsuariosCtrl',
                        templateUrl: "app/admin/usuarios/views/index.html"
                    }
                }
            })
            .state('app.admin.usuarios.accion', {
                cache: false,
                url: '/:accion/:id',
                permission: 'user.view, user.add, user.edit',
                data: {
                    login: true,
                    title: 'Usuario'
                },
                views: {
                    "content@app": {
                        controller: 'UsuariosAccionCtrl',
                        templateUrl: "app/admin/usuarios/views/accion.html"
                    }
                }
            })

            .state('app.admin.perfiles', {
                cache: false,
                url: '/admin/perfiles',
                permission: 'profile.view, profile.add, profile.edit, profile.delete',
                data: {
                    login: true,
                    title: 'Perfiles'
                },
                views: {
                    "content@app": {
                        controller: 'PerfilesCtrl',
                        templateUrl: "app/admin/perfiles/views/index.html"
                    }
                }
            })
            .state('app.admin.perfiles.accion', {
                cache: false,
                url: '/:accion/:id',
                permission: 'profile.view, profile.add, profile.edit',
                data: {
                    login: true,
                    title: 'Perfil'
                },
                views: {
                    "content@app": {
                        controller: 'PerfilesAccionCtrl',
                        templateUrl: "app/admin/perfiles/views/accion.html"
                    }
                }
            })

            .state('app.admin.modulos', {
                cache: false,
                url: '/admin/modulos',
                permission: 'module.view, module.add, module.edit, module.delete',
                data: {
                    login: true,
                    title: 'Modulos'
                },
                views: {
                    "content@app": {
                        controller: 'ModulosCtrl',
                        templateUrl: "app/admin/modulos/views/index.html"
                    }
                }
            })
            .state('app.admin.modulos.accion', {
                cache: false,
                url: '/:accion/:id',
                permission: 'module.view, module.add, module.edit',
                data: {
                    login: true,
                    title: 'Modulo'
                },
                views: {
                    "content@app": {
                        controller: 'ModulosAccionCtrl',
                        templateUrl: "app/admin/modulos/views/accion.html"
                    }
                }
            })

    }]);