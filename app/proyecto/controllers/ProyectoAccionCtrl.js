angular.module('app.proyectos')
    .controller('ProyectoAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.project = {
                projectId: null,
                name: '',
                code: '',
                active: ''
            };

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('project.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    $scope.project.active = 'true';
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('project.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('project.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'projectGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;

                        $scope.project.projectId = data.projectId;
                        $scope.project.name = data.name;
                        $scope.project.code = data.code;
                        $scope.project.active = '' + data.active;

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            $scope.cancel = function () {
                $state.go('app.proyecto');
                return false;
            };

            // Guardar informacion, crea o edita un usuario con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                } else if (!!$scope.project.name && !!$scope.project.code) {
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('project.add')) {
                                $scope.project.active = ($scope.project.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.project, 'projectPOST').then(function () {
                                    $.bigBox({
                                        title: "Nuevo proyecto agregado",
                                        content: "Se guardo la información del proyecto: <b>" + $scope.project.name + "</b> - <i>" + $scope.project.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del proyecto';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('project.edit')) {
                                $scope.project.active = ($scope.project.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.project, 'projectPUT', '/' + $scope.project.projectId).then(function () {
                                    $.bigBox({
                                        title: "Proyecto actualizado",
                                        content: "Se actualizo la información del proyecto: <b>" + $scope.project.name + "</b> - <i>" + $scope.project.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del proyecto';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);
