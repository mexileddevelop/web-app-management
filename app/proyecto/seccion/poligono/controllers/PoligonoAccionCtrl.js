angular.module('app.proyectos')
    .controller('PoligonoAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.polygon = {
                polygonId: null,
                name: '',
                code: '',
                active: '',
                section: {
                    sectionId: null,
                }
            };

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('project.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    $scope.polygon.active = 'true';
                    $scope.polygon.section.sectionId = $stateParams.seccion;
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('project.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('project.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'polygonGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;

                        $scope.polygon.polygonId = data.polygonId;
                        $scope.polygon.name = data.name;
                        $scope.polygon.code = data.code;
                        $scope.polygon.active = '' + data.active;
                        $scope.polygon.section.sectionId = data.section.sectionId;

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            $scope.cancel = function () {
                $state.go('app.proyecto.seccion.poligono', {seccion: $scope.polygon.section.sectionId});
                return false;
            };

            // Guardar informacion, crea o edita un usuario con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                } else if (!!$scope.polygon.name && !!$scope.polygon.code) {
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('project.add')) {
                                $scope.polygon.active = ($scope.polygon.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.polygon, 'polygonPOST').then(function () {
                                    $.bigBox({
                                        title: "Nuevo polígono agregado",
                                        content: "Se guardo la información del polígono: <b>" + $scope.polygon.name + "</b> - <i>" + $scope.polygon.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del polígono';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('project.edit')) {
                                $scope.polygon.active = ($scope.polygon.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.polygon, 'polygonPUT', '/' + $scope.polygon.polygonId).then(function () {
                                    $.bigBox({
                                        title: "Polígono actualizado",
                                        content: "Se actualizo la información del polígono: <b>" + $scope.polygon.name + "</b> - <i>" + $scope.polygon.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del polígono';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);
