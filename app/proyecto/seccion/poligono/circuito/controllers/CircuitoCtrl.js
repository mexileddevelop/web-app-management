angular.module('app.proyectos')
    .controller('CircuitoCtrl', ['$scope', '$compile', '$state', 'DTOptionsBuilder', 'DTColumnBuilder', 'permissionsService', 'restService', '$stateParams', '$filter',
        function ($scope, $compile, $state, DTOptionsBuilder, DTColumnBuilder, permissionsService, restService, $stateParams, $filter) {
            $scope.pageTitle = $state.current.data.title;
            $scope.projectId;
            $scope.sectionId;
            $scope.polygonId = $stateParams.poligono;

            /* Obtener informacion para seleccionar proyecto, seccion */
            function loadProjects() {
                restService.sendRequest(null, 'projectGET').then(function (data) {
                    $scope.listProjects = data.data;
                }, function () {
                    $.bigBox({
                        title: "Error al cargar",
                        content: "No se pudo cargar la lista de proyectos",
                        color: "#C46A69",
                        icon: "fa fa-warning shake animated",
                        timeout: 6000
                    });
                    $state.go('app.proyecto');
                });
            }

            function loadSections() {
                restService.sendRequest(null, 'sectionGET', '?projectId=' + $scope.projectId).then(function (data) {
                    $scope.listSections = data.data;
                }, function () {
                    $.bigBox({
                        title: "Error al cargar",
                        content: "No se pudo cargar la lista de secciones",
                        color: "#C46A69",
                        icon: "fa fa-warning shake animated",
                        timeout: 6000
                    });
                    $state.go('app.proyecto');
                });
            }

            function loadPolygons() {
                restService.sendRequest(null, 'polygonGET', '?sectionId=' + $scope.sectionId).then(function (data) {
                    $scope.listPolygons = data.data;
                }, function () {
                    $.bigBox({
                        title: "Error al cargar",
                        content: "No se pudo cargar la lista de poligonos",
                        color: "#C46A69",
                        icon: "fa fa-warning shake animated",
                        timeout: 6000
                    });
                    $state.go('app.proyecto');
                });
            }

            loadProjects();
            if (!!$scope.polygonId) {
                restService.sendRequest(null, 'polygonGET', '/' + $scope.polygonId).then(function (data) {
                    $scope.sectionId = '' + data.data.section.sectionId;
                    $scope.projectId = '' + data.data.section.project.projectId;
                    loadSections();
                    loadPolygons();
                }, function () {
                    $scope.polygonId = '';
                });
            }
            /* ~ */

            $scope.updateData = function () {
                if (!!$scope.projectId && !!$scope.sectionId && !!$scope.polygonId)
                    $state.go('app.proyecto.seccion.poligono.circuito', {poligono: $scope.polygonId});
                else if (!!$scope.projectId && !!$scope.sectionId)
                    loadPolygons();
                else if (!!$scope.projectId)
                    loadSections();
            };

            $scope.statusFilter = {};

            /* Datatable (DT), creacion y asignacion de eventos */
            var dataArrayResolved = [];

            $scope.standardOptions = DTOptionsBuilder
                .fromFnPromise(restService.sendRequest(null, 'deviceGET', '?polygonId=' + ($scope.polygonId || 0)))
                .withDataProp('data')
                .withDOM("t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap()
                .withOption('createdRow', createdRow);
            $scope.standardColumns = [
                DTColumnBuilder.newColumn('code').notSortable(),
                DTColumnBuilder.newColumn('macAddress').withClass('hidden-xs').notSortable(),
                DTColumnBuilder.newColumn('lastActivity').notSortable().renderWith(function (data) {
                    return $filter('date')(data, "dd-MM-yyyy hh:mm:ssa");
                }),
                DTColumnBuilder.newColumn('installationStatus').notSortable().renderWith(function (data) {
                    return data;
                }),
                DTColumnBuilder.newColumn(null).notSortable().withClass('table-actions-buttons').renderWith(function (data) {
                    /* loadStatusFilter */
                    var objExtender = {};
                    objExtender[data.installationStatus] = data.installationStatus
                    angular.extend($scope.statusFilter, objExtender);
                    /* ~ */
                    dataArrayResolved[data.deviceId] = data;
                    return '<a has-permission="PROJECT_VIEW" title="Ver" ui-sref="app.proyecto.seccion.poligono.circuito.accion({accion: \'ver\', id: ' + data.deviceId + '})" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>' +
                        '<a has-permission="PROJECT_EDIT" title="Editar" ui-sref="app.proyecto.seccion.poligono.circuito.accion({accion: \'editar\', id: ' + data.deviceId + '})" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>' +
                        '<a has-permission="PROJECT_DELETE" title="Eliminar" ng-click="accion(\'delete\', ' + data.deviceId + ')" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }

            // Callback para funcionalidad de filtros
            $scope.instanceCallback = function (dtInstance) {
                $scope.dtInstance = dtInstance;
                var table = dtInstance.DataTable;
                table.columns().eq(0).each(function (colIdx) {
                    $('input, select', table.column(colIdx).header()).on('keyup change', function () {
                        if (this.getAttribute('id') == 'table-lenght-ctrl') {
                            table
                                .page.len(this.value)
                                .draw();
                        }
                        else {
                            var isSelect = (this.nodeName == 'SELECT');
                            var searchValue = (isSelect && this.value !== '') ? '\\b' + this.value + '\\b' : this.value;
                            table
                                .column(colIdx)
                                .search(searchValue, isSelect, !isSelect)
                                .draw();
                        }
                    });
                });
            };

            $scope.accion = function (accion, id) {
                switch (accion) {
                    case 'delete':
                    case 'eliminar':
                        if (permissionsService.canAccess('project.delete')) {
                            $.SmartMessageBox({
                                title: "Borrar Circuito",
                                content: "Esta a punto de borrar la información del circuito <b>" + dataArrayResolved[id].code + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'polygonDELETE', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se borro la información del circuito <b>" + dataArrayResolved[id].code + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'deviceGET', '?polygonId=' + ($scope.polygonId || 0)));
                                        else
                                            $state.go('app.proyecto.seccion.poligono.circuito', {poligono: $scope.polygonId}, {reload: true});
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo borrar el circuito solicitado...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                }
            };
        }
    ]);