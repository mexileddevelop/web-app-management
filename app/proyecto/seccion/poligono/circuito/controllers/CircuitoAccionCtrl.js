angular.module('app.proyectos')
    .controller('CircuitoAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService', 'uiGmapGoogleMapApi',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService, uiGmapGoogleMapApi) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            var centerMapCoords = {
                latitude: 20.6322137,
                longitude: -103.2776288
            };

            $scope.device = {
                deviceId: null,
                controlNumber: '',
                macAddress: '',
                lamps: 0,
                latitude: null,
                longitude: null,
                ligthOffHour: '08:00',
                ligthOnHour: '19:00',
                timeBeforeNotify: 0,
                timeToMarkAsDisconnected: 0,
                installationStatus: 'Proyectado',
                polygonView: {
                    polygonId: null
                },
                project: {
                    projectId: null
                },
                section: {
                    sectionId: null,
                }
            };

            // Limit inputs type number to receive only numbers and some browser control keys (like tab, backspace, etc)
            $('input[type=number]').on('keydown', function (e) {
                var validKeyCodes = [107, 187, 8, 46, 37, 38, 39, 40, 116, 9];
                if ((e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 48 && e.keyCode <= 57) || validKeyCodes.indexOf(e.keyCode) != -1 )
                    return true;
                else
                    return false;
            });

            /**
             * Copy/replace values from objectData to objectTarget
             * @param objectTarget, object where to save the data
             * @param objectData, object with the data to copy
             */
            function fillObjectValues(objectTarget, objectData) {
                for (var prop in objectTarget) {
                    if (typeof objectTarget[prop] === 'object' && objectTarget[prop] !== null)
                        fillObjectValues(objectTarget[prop], objectData[prop]);
                    else
                        objectTarget[prop] = objectData[prop];
                }
            }

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('project.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    $scope.device.polygonView.polygonId = $stateParams.poligono;
                    restService.sendRequest(null, 'polygonGET', '/' + $stateParams.poligono).then(function (data) {
                        data = data.data;

                        $scope.device.project.projectId = data.section.project.projectId;
                        $scope.device.section.sectionId = data.section.sectionId;
                        $scope.device.polygonView.polygonId = data.polygonId;
                    }, function () {
                        $.bigBox({
                            title: "Error al cargar",
                            content: "No se pudo cargar la información del proyecto relacionado",
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('project.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('project.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'deviceGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;

                        fillObjectValues($scope.device, data);
                        fillObjectValues(centerMapCoords, $scope.device);

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            $scope.cancel = function () {
                $state.go('app.proyecto.seccion.poligono.circuito', {poligono: $scope.device.polygonView.polygonId});
                return false;
            };

            /* Mapa */
            $scope.map = {
                center: centerMapCoords,
                zoom: 15,
                options: {
                    scrollwheel: false
                },
                events: {
                    click: function(map, eventName, events) {
                        if (!$scope.readOnly) {
                            var event = events[0];
                            $scope.$apply(function () {
                                $scope.device.latitude = event.latLng.lat();
                                $scope.device.longitude = event.latLng.lng();
                                map.panTo(new google.maps.LatLng($scope.device.latitude, $scope.device.longitude));
                            });
                        }
                    }
                }
            };
            $scope.mapMarker = {
                id: 0,
                options: {
                    draggable: !$scope.readOnly
                },
                events: {

                    dragend: function (marker) {
                        if (!$scope.readOnly) {
                            $scope.device.latitude = marker.getPosition().lat();
                            $scope.device.longitude = marker.getPosition().lng();
                            marker.map.panTo(new google.maps.LatLng($scope.device.latitude, $scope.device.longitude));
                        }
                    }
                }
            };

            uiGmapGoogleMapApi.then(function (maps) {});
            /* ~ */

            // Guardar informacion, crea o edita un usuario con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                } else if ($scope.device.lamps > 0 && $scope.device.timeBeforeNotify > 0 && $scope.device.timeToMarkAsDisconnected > 0) {
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('project.add')) {
                                restService.sendRequest($scope.device, 'devicePOST').then(function () {
                                    $.bigBox({
                                        title: "Nuevo circuito agregado",
                                        content: "Se guardo la información del circuito: <br><b>" + $scope.device.controlNumber + "</b>, MAC <i>" + $scope.device.macAddress + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del circuito';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('project.edit')) {
                                restService.sendRequest($scope.device, 'devicePUT', '/' + $scope.device.deviceId).then(function () {
                                    $.bigBox({
                                        title: "Circuito actualizado",
                                        content: "Se actualizo la información del circuito: <br><b>" + $scope.device.controlNumber + "</b>, MAC <i>" + $scope.device.macAddress + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información del circuito';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);
