angular.module('app.proyectos')
    .controller('SeccionCtrl', ['$scope', '$compile', '$state', 'DTOptionsBuilder', 'DTColumnBuilder', 'permissionsService', 'restService', '$stateParams',
        function ($scope, $compile, $state, DTOptionsBuilder, DTColumnBuilder, permissionsService, restService, $stateParams) {
            $scope.pageTitle = $state.current.data.title;
            $scope.projectId = $stateParams.proyecto;

            /* Obtener informacion para seleccionar proyecto */
            function loadProjects() {
                restService.sendRequest(null, 'projectGET').then(function (data) {
                    $scope.listProjects = data.data;
                }, function () {
                    $.bigBox({
                        title: "Error al cargar",
                        content: "No se pudo cargar la lista de proyectos",
                        color: "#C46A69",
                        icon: "fa fa-warning shake animated",
                        timeout: 6000
                    });
                    $state.go('app.proyecto');
                });
            }

            loadProjects();
            /* ~ */

            $scope.updateData = function() {
                if (!!$scope.projectId)
                    $state.go('app.proyecto.seccion', {proyecto: $scope.projectId});
            };

            $scope.statusFilter = {
                'true': 'Activo',
                'false': 'Inactivo'
            };

            /* Datatable (DT), creacion y asignacion de eventos */
            var dataArrayResolved = [];

            $scope.standardOptions = DTOptionsBuilder
                .fromFnPromise(restService.sendRequest(null, 'sectionGET', '?projectId=' + ($scope.projectId || 0)))
                .withDataProp('data')
                .withDOM("t" + "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
                .withBootstrap()
                .withOption('createdRow', createdRow);
            $scope.standardColumns = [
                DTColumnBuilder.newColumn('name').notSortable().renderWith(function (row, type, data) {
                    return '<a ui-sref="app.proyecto.seccion.poligono({seccion: ' + data.sectionId + '})">' + row + '</a>';
                }),
                DTColumnBuilder.newColumn('code').notSortable(),
                DTColumnBuilder.newColumn('active').notSortable().renderWith(function (data) {
                    return $scope.statusFilter[data];
                }),
                DTColumnBuilder.newColumn(null).notSortable().withClass('table-actions-buttons').renderWith(function (data) {
                    dataArrayResolved[data.sectionId] = data;
                    return '<a has-permission="PROJECT_VIEW" title="Ver" ui-sref="app.proyecto.seccion.accion({accion: \'ver\', id: ' + data.sectionId + '})" class="btn btn-default btn-xs"><i class="fa fa-eye"></i></a>' +
                        '<a has-permission="PROJECT_EDIT" title="Editar" ui-sref="app.proyecto.seccion.accion({accion: \'editar\', id: ' + data.sectionId + '})" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>' +
                        '<a has-permission="PROJECT_EDIT" title="' + (data.active ? 'Desactivar' : 'Activar') + '" ng-click="accion(\'' + (data.active ? 'deactivate' : 'activate') + '\', ' + data.sectionId + ')" class="btn btn-default btn-xs"><i class="fa ' + (data.active ? 'fa-ban' : 'fa-check-circle-o') + '"></i></a>' +
                        '<a has-permission="PROJECT_DELETE" title="Eliminar" ng-click="accion(\'delete\', ' + data.sectionId + ')" class="btn btn-default btn-xs"><i class="fa fa-trash"></i></a>';
                })
            ];
            // Recompilar fila para poder asignar funciones via angular a los botones en la DT
            function createdRow(row) {
                $compile(angular.element(row).contents())($scope);
            }

            // Callback para funcionalidad de filtros
            $scope.instanceCallback = function (dtInstance) {
                $scope.dtInstance = dtInstance;
                var table = dtInstance.DataTable;
                table.columns().eq(0).each(function (colIdx) {
                    $('input, select', table.column(colIdx).header()).on('keyup change', function () {
                        if (this.getAttribute('id') == 'table-lenght-ctrl') {
                            table
                                .page.len(this.value)
                                .draw();
                        }
                        else {
                            var isSelect = (this.nodeName == 'SELECT');
                            var searchValue = (isSelect && this.value !== '') ? '\\b' + this.value + '\\b' : this.value;
                            table
                                .column(colIdx)
                                .search(searchValue, isSelect, !isSelect)
                                .draw();
                        }
                    });
                });
            };

            $scope.accion = function (accion, id) {
                switch (accion) {
                    case 'delete':
                    case 'eliminar':
                        if (permissionsService.canAccess('project.delete')) {
                            $.SmartMessageBox({
                                title: "Borrar Sección",
                                content: "Esta a punto de borrar la información de la sección <b>" + dataArrayResolved[id].name + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    restService.sendRequest(null, 'sectionDELETE', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se borro la información de la sección <b>" + dataArrayResolved[id].name + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'sectionGET', '?projectId=' + ($scope.projectId || 0)));
                                        else
                                            $state.go('app.proyecto.seccion', {proyecto: $scope.projectId}, {reload: true});
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo borrar la sección solicitada...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                    case 'activate':
                    case 'deactivate':
                        if (permissionsService.canAccess('project.edit')) {
                            var active = (accion.toLowerCase() === 'activate');
                            $.SmartMessageBox({
                                title: (active ? 'Activar' : 'Desactivar') + " Proyecto",
                                content: "Esta a punto de " + (active ? 'activar' : 'desactivar') + " la sección <b>" + dataArrayResolved[id].name + "</b>, ¿Esta usted seguro?",
                                buttons: '[No][Si]'
                            }, function (ButtonPressed) {
                                if (ButtonPressed === "Si") {
                                    dataArrayResolved[id].active = active;
                                    restService.sendRequest(dataArrayResolved[id], 'sectionPUT', '/' + id).then(function () {
                                        $.smallBox({
                                            title: "Operación correcta",
                                            content: "<i>Se " + (active ? 'activo' : 'desactivo') + " la sección <b>" + dataArrayResolved[id].name + "</b></i>",
                                            color: "#659265",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                        if ($scope.dtInstance)
                                            $scope.dtInstance.changeData(restService.sendRequest(null, 'sectionGET', '?projectId=' + ($scope.projectId || 0)));
                                        else
                                            $state.go('app.proyecto.seccion', {proyecto: $scope.projectId}, {reload: true});
                                    }, function () {
                                        $.smallBox({
                                            title: "Hubo un error",
                                            content: "<i>No se pudo " + (active ? 'activar' : 'desactivar') + " la sección solicitada...</i>",
                                            color: "#C46A69",
                                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                            timeout: 4000
                                        });
                                    });
                                }
                            });
                        }
                        break;
                }
            };
        }
    ]);