angular.module('app.proyectos')
    .controller('SeccionAccionCtrl', ['$scope', '$rootScope', '$state', '$stateParams', 'permissionsService', 'restService',
        function ($scope, $rootScope, $state, $stateParams, permissionsService, restService) {
            $scope.pageTitle = $state.current.data.title;
            $scope.action = $stateParams.accion.toLowerCase();

            $scope.section = {
                sectionId: null,
                name: '',
                code: '',
                active: '',
                project: {
                    projectId: null
                }
            };

            switch ($scope.action) {
                case 'add':
                case 'agregar':
                    if (!permissionsService.canAccess('project.add'))
                        $scope.cancel();
                    $scope.readOnly = false;
                    $scope.section.active = 'true';
                    $scope.section.project.projectId = $stateParams.proyecto;
                    break;
                case 'view':
                case 'ver':
                    if (!permissionsService.canAccess('project.view'))
                        $scope.cancel();
                    $scope.readOnly = true;
                case 'update':
                case 'edit':
                case 'editar':
                    if (!$scope.readOnly && !permissionsService.canAccess('project.edit'))
                        $scope.cancel();
                    $scope.readOnly = $scope.readOnly || false;

                    if (!$stateParams.id || isNaN($stateParams.id))
                        $scope.cancel();

                    restService.sendRequest(null, 'sectionGET', '/' + $stateParams.id).then(function (data) {
                        data = data.data;

                        $scope.section.sectionId = data.sectionId;
                        $scope.section.name = data.name;
                        $scope.section.code = data.code;
                        $scope.section.active = '' + data.active;
                        $scope.section.project.projectId = data.project.projectId;

                        $scope.dataReady = true;
                    }, function (error) {
                        var errorMsg;
                        if (!!error.code) {
                            errorMsg = '<i>Error: ' + error.code + '</i><br>';
                            if (!!error.errorList.length) errorMsg += error.errorList[0];
                            else errorMsg += error.message;
                        }
                        $.bigBox({
                            title: "Error al cargar",
                            content: errorMsg,
                            color: "#C46A69",
                            icon: "fa fa-warning shake animated",
                            timeout: 6000
                        });
                        $scope.cancel();
                    });
                    break;
                default:
                    $scope.cancel();
                    break;
            }

            $scope.cancel = function () {
                $state.go('app.proyecto.seccion', {proyecto: $scope.section.project.projectId});
                return false;
            };

            // Guardar informacion, crea o edita un usuario con la informacion
            $scope.save = function () {
                $scope.form.submited = true;
                if ($scope.form.$invalid) {
                    angular.forEach($scope.form.$error, function (field) {
                        angular.forEach(field, function (errorField) {
                            errorField.$setTouched();
                        });
                    });
                } else if (!!$scope.section.name && !!$scope.section.code) {
                    switch ($scope.action) {
                        case 'add':
                        case 'agregar':
                            if (permissionsService.canAccess('project.add')) {
                                $scope.section.active = ($scope.section.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.section, 'sectionPOST').then(function () {
                                    $.bigBox({
                                        title: "Nueva sección agregada",
                                        content: "Se guardo la información de la sección: <b>" + $scope.section.name + "</b> - <i>" + $scope.section.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información de la sección';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        case 'update':
                        case 'edit':
                        case 'editar':
                            if (permissionsService.canAccess('project.edit')) {
                                $scope.section.active = ($scope.section.active.toLowerCase() === 'true');

                                restService.sendRequest($scope.section, 'sectionPUT', '/' + $scope.section.sectionId).then(function () {
                                    $.bigBox({
                                        title: "Sección actualizada",
                                        content: "Se actualizo la información de la sección: <b>" + $scope.section.name + "</b> - <i>" + $scope.section.code + "</i>",
                                        color: "#739E73",
                                        icon: "fa fa-check",
                                        timeout: 8000
                                    });
                                    $scope.cancel();
                                }, function (error) {
                                    var errorMsg;
                                    if (!!error.code) {
                                        errorMsg = '<i>Error: ' + error.code + '</i><br>';
                                        if (!!error.errorList.length) errorMsg += error.errorList[0];
                                        else errorMsg += error.message;
                                    }
                                    else errorMsg = 'No se pudo guardar la información de la sección';

                                    $.bigBox({
                                        title: "Error al guardar",
                                        content: errorMsg,
                                        color: "#C46A69",
                                        icon: "fa fa-warning shake animated",
                                        timeout: 6000
                                    });
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
            };
        }
    ]);
