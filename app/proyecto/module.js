"use strict";

angular.module('app.proyectos', ['ui.router', 'datatables', 'datatables.bootstrap', 'uiGmapgoogle-maps'])
    .config(['$stateProvider', 'uiGmapGoogleMapApiProvider', function ($stateProvider, uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyDk7Kg1cEqiRUEyYCXN6KWRkejGRMwHkzc',
            v: '3.20', //defaults to latest 3.X anyhow
            libraries: 'weather,geometry,visualization'
        });

        $stateProvider
            .state('app.proyecto', {
                cache: false,
                url: '/proyectos',
                permission: 'project.view, project.add, project.edit, project.delete',
                data: {
                    login: true,
                    title: 'Proyectos'
                },
                views: {
                    "content@app": {
                        controller: 'ProyectoCtrl',
                        templateUrl: 'app/proyecto/views/index.html'
                    }
                }
            })
            .state('app.proyecto.accion', {
                cache: false,
                url: '/:accion?:id',
                permission: 'project.view, project.add, project.edit',
                data: {
                    login: true,
                    title: 'Proyecto'
                },
                views: {
                    "content@app": {
                        controller: 'ProyectoAccionCtrl',
                        templateUrl: 'app/proyecto/views/accion.html'
                    }
                }
            })

            .state('app.proyecto.seccion', {
                cache: false,
                url: '^/secciones/:proyecto',
                permission: 'project.view, project.add, project.edit, project.delete',
                data: {
                    login: true,
                    title: 'Secciones'
                },
                views: {
                    "content@app": {
                        controller: 'SeccionCtrl',
                        templateUrl: 'app/proyecto/seccion/views/index.html'
                    }
                }
            })
            .state('app.proyecto.seccion.accion', {
                cache: false,
                url: '/:accion?:id',
                permission: 'project.view, project.add, project.edit',
                data: {
                    login: true,
                    title: 'Seccion'
                },
                views: {
                    "content@app": {
                        controller: 'SeccionAccionCtrl',
                        templateUrl: 'app/proyecto/seccion/views/accion.html'
                    }
                }
            })

            .state('app.proyecto.seccion.poligono', {
                cache: false,
                url: '^/poligonos/:seccion',
                permission: 'project.view, project.add, project.edit, project.delete',
                data: {
                    login: true,
                    title: 'Poligonos'
                },
                views: {
                    "content@app": {
                        controller: 'PoligonoCtrl',
                        templateUrl: 'app/proyecto/seccion/poligono/views/index.html'
                    }
                }
            })
            .state('app.proyecto.seccion.poligono.accion', {
                cache: false,
                url: '/:accion?:id',
                permission: 'project.view, project.add, project.edit',
                data: {
                    login: true,
                    title: 'Polígono'
                },
                views: {
                    "content@app": {
                        controller: 'PoligonoAccionCtrl',
                        templateUrl: 'app/proyecto/seccion/poligono/views/accion.html'
                    }
                }
            })

            .state('app.proyecto.seccion.poligono.circuito', {
                cache: false,
                url: '^/circuitos/:poligono',
                permission: 'project.view, project.add, project.edit, project.delete',
                data: {
                    login: true,
                    title: 'Circuitos'
                },
                views: {
                    "content@app": {
                        controller: 'CircuitoCtrl',
                        templateUrl: 'app/proyecto/seccion/poligono/circuito/views/index.html'
                    }
                }
            })
            .state('app.proyecto.seccion.poligono.circuito.accion', {
                cache: false,
                url: '/:accion?:id',
                permission: 'project.view, project.add, project.edit',
                data: {
                    login: true,
                    title: 'Circuito'
                },
                views: {
                    "content@app": {
                        controller: 'CircuitoAccionCtrl',
                        templateUrl: 'app/proyecto/seccion/poligono/circuito/views/accion.html'
                    }
                },
                resolve: {
                    srcipts: function(lazyScript){
                        return lazyScript.register([
                            "build/vendor.ui.js"
                        ])

                    }
                }
            })

    }]);